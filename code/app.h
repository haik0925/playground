﻿#ifndef DSC_APP_H
#define DSC_APP_H

namespace dsc
{
enum class AppKeyCode
{
    Undefined = 0,
    LMouse,
    RMouse,
    ESC,
    Space,
    Num0,
    Num1,
    Num2,
    Num3,
    Num4,
    Num5,
    Num6,
    Num7,
    Num8,
    Num9,
    A,
    D,
    E,
    R,
    S,
    W,
    Y,
    Z,
    Shift,
    Control,
    ArrowUp,
    ArrowDown,
    ArrowLeft,
    ArrowRight,
    F1,
    F2,
    F3,
    F4,
    F5,
    F6,
    F7,
    F8,
    F9,
    F10,
    F11,
    F12,
    Delete,
};

enum class AppEventType
{
    Undefined = 0,
    Quit,
    Keyupdown,
};

struct AppKeyEvent
{
    AppEventType type;
    AppKeyCode code;
    bool is_down;
    bool alt_is_down;
};

union AppEvent
{
    AppEventType type;
    AppKeyEvent key;
};

}
#endif // DSC_APP_H
