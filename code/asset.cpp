﻿#include "asset.h"
#include "stb_image.h"
#include "common.h"
#include "debug.h"

namespace dsc
{
AssetStorage::AssetStorage(ArenaAllocator* asset_arena)
{
    auto register_shader = [this](ShaderType shader_type,
                                  string_view shader_name) {
        string_view root_dir = "glsl/";
        string_view vs_ext = ".vert";
        string_view fs_ext = ".frag";
        string vs_path(root_dir);
        vs_path += shader_name;
        vs_path += vs_ext;
        string fs_path(root_dir);
        fs_path += shader_name;
        fs_path += fs_ext;

        _shaders[shader_type] =
            Shader::create_from_file(vs_path.c_str(), fs_path.c_str());
    };
    register_shader(ShaderType::Default, "default");
    register_shader(ShaderType::SolidColor, "solid_color");
    register_shader(ShaderType::DebugGuiSolid, "debug_gui_solid");
    register_shader(ShaderType::DebugGuiTextured, "debug_gui_textured");
    register_shader(ShaderType::DebugDepthTest, "debug_depth_test");
    register_shader(ShaderType::DebugPhysics, "debug_physics");
    for (auto& this_shader : _shaders)
    {
        assert_(this_shader.fs != 0 && this_shader.vs != 0 &&
                this_shader.program != 0);
    }

    _meshes[MeshType::Cube] = Mesh::create_cube(asset_arena);
    _meshes[MeshType::Plane] = Mesh::create_plane(asset_arena);
    _meshes[MeshType::UIQuad] = Mesh::create_ui_quad(asset_arena);
    _meshes[MeshType::NPCTestBillboard] =
        Mesh::create_npc_test_billboard(asset_arena);
    _meshes[MeshType::Airboat] =
        Mesh::create_from_obj_file("airboat.obj", asset_arena);
    for (auto& this_mesh : _meshes)
    {
        assert_(this_mesh.vertex_count > 0);
    }

    new (&_models[ModelType::CubeTest])
        Model(&_meshes[MeshType::Cube], &_shaders[ShaderType::Default]);
    new (&_models[ModelType::LightSourceTest])
        Model(&_meshes[MeshType::Cube], &_shaders[ShaderType::SolidColor]);
    new (&_models[ModelType::NpcTestModel]) Model(
        &_meshes[MeshType::NPCTestBillboard], &_shaders[ShaderType::Default]);
    new (&_models[ModelType::Airboat])
        Model(&_meshes[MeshType::Airboat], &_shaders[ShaderType::Default]);
    for (auto& this_model : _models)
    {
        assert_(this_model.mesh != nullptr && this_model.shader != nullptr);
    }

    _textures[TextureType::Wall] =
        debug_load_texture_from_file("textures/wall.png");
    _textures[TextureType::Ground] =
        debug_load_texture_from_file("textures/ground.jpg");
    _textures[TextureType::Door] =
        debug_load_texture_from_file("textures/door.jpg");
    _textures[TextureType::Box] =
        debug_load_texture_from_file("textures/box.png");
    _textures[TextureType::Key] =
        debug_load_texture_from_file("textures/key.png");
    for (auto this_texture : _textures)
    {
        assert_(this_texture != 0);
    }

    _sfxs[SfxType::Hit1].load_from_ogg("sfx/hit1.ogg");
    _sfxs[SfxType::Hit2].load_from_ogg("sfx/hit2.ogg");
    _sfxs[SfxType::Open1].load_from_ogg("sfx/open1.ogg");
    _sfxs[SfxType::Shoot1].load_from_ogg("sfx/shoot1.ogg");
    _sfxs[SfxType::DoorOpen].load_from_ogg("sfx/door_open.ogg");
    _sfxs[SfxType::BoxHit].load_from_ogg("sfx/box_hit.ogg");
    _sfxs[SfxType::BoxBreak].load_from_ogg("sfx/box_break.ogg");
    for (auto& this_sfx : _sfxs)
    {
        // TODO(illkwon): Needs better initialization check
        assert_(this_sfx.def.loaded != nullptr);
    }

    _bgms[BgmType::Chiptronical].load_from_ogg("bgm/chiptronical.ogg");
    for (auto& this_bgm : _bgms)
    {
        // TODO(illkwon): Needs better initialization check
        assert_(this_bgm.def.loaded != nullptr);
    }
}

Texture AssetStorage::debug_load_texture_from_file(const char* filename)
{
    IntVec2 image_size = {0};
    int image_comp;
    void* image_data =
        stbi_load(filename, &image_size.x, &image_size.y, &image_comp, 4);
    assert_(image_data);
    log_("Image Loaded - Name: %s, Image Size: (%d,%d), Image Compression: %d",
         filename, image_size.x, image_size.y, image_comp);
    Texture texture =
        texture_load((u8*)image_data, image_size.width, image_size.height,
                     GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE, GL_NEAREST, GL_NEAREST,
                     GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE);
    stbi_image_free(image_data);

    return texture;
}
}
