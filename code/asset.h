﻿#ifndef DSC_ASSET_H
#define DSC_ASSET_H
#include "renderer.h"
#include "audio.h"
#include "shared.h"
#include "containers.h"

namespace dsc
{
enum class ShaderType
{
    Default = 0,
    SolidColor,
    DebugGuiSolid,
    DebugGuiTextured,
    DebugDepthTest,
    DebugPhysics,
    Count,
};

enum class MeshType
{
    Cube,
    Plane,
    UIQuad,
    NPCTestBillboard,
    Airboat, // TODO(illkwon): Just temporary model for testing obj
             // loading
    Count,
};

enum class ModelType
{
    // remember, these indexes will be saved. consider this when you are
    // altering this part.
    CubeTest = 0,
    LightSourceTest,
    NpcTestModel,
    Airboat, // TODO(illkwon): Just temporary model for testing obj
             // loading
    Count,
};

enum class TextureType
{
    // remember, these indexes will be saved. consider this when you are
    // altering this part.
    None = -1,
    Wall,
    Ground,
    Door,
    Box,
    Key,
    Count
};

enum class SfxType
{
    Hit1 = 0,
    Hit2,
    Open1,
    Shoot1,
    DoorOpen,
    BoxHit,
    BoxBreak,
    Count
};

enum class BgmType
{
    Chiptronical = 0,
    Count
};

struct AssetStorage
{
    enum_array<ShaderType, Shader> _shaders = {};
    enum_array<MeshType, Mesh> _meshes = {};
    enum_array<ModelType, Model> _models = {};
    enum_array<TextureType, Texture> _textures = {};
    enum_array<SfxType, Sound> _sfxs = {};
    enum_array<BgmType, Sound> _bgms = {};

    AssetStorage(ArenaAllocator* asset_arena);

private:
    static Texture debug_load_texture_from_file(const char* filename);
};

}
#endif // DSC_ASSET_H
