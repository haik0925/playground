﻿#include "audio.h"
#include "debug.h"
#include "common.h"

namespace dsc
{
internal_func void audio__check_cute_sound_error()
{
    if (cs_error_reason)
    {
        log_(cs_error_reason);
        assert_(cs_error_reason);
    }
}

void audio_init(AudioContext* audio, Handle64 window)
{
    const uint frequency = 44000;
    const uint latency = 15;
    const uint buffer_second = 5;
    const uint pool_count = 20;
    audio->ctx = cs_make_context((void*)window, frequency, latency,
                                 buffer_second, pool_count);
    audio__check_cute_sound_error();
}

void audio_deinit(AudioContext* audio)
{
    cs_shutdown_context(audio->ctx);
    audio__check_cute_sound_error();
}

void audio_update(AudioContext* audio)
{
    cs_mix(audio->ctx);
    audio__check_cute_sound_error();
}

void audio_stop_all_sounds(AudioContext* audio)
{
    cs_stop_all_sounds(audio->ctx);
}

void sound_init_from_ogg_file(Sound* sound, const char* filename)
{
    sound->loaded = cs_load_ogg(filename);
    sound->def = cs_make_def(&sound->loaded);
    audio__check_cute_sound_error();
}

PlayingSound sound_play(AudioContext* audio, Sound* sound)
{
    PlayingSound result = {cs_play_sound(audio->ctx, sound->def)};
    audio__check_cute_sound_error();
    return result;
}

void sound_stop(PlayingSound playing_sound)
{
    cs_stop_sound(playing_sound.data);
}

void sound_set_loop(PlayingSound playing_sound, bool loop)
{
    cs_loop_sound(playing_sound.data, loop);
}

void sound_set_pause(PlayingSound playing_sound, bool pause)
{
    cs_pause_sound(playing_sound.data, pause);
}

AudioContext::AudioContext(Handle64 window)
{
    audio_init(this, window);
}

AudioContext::~AudioContext()
{
    audio_deinit(this);
}

void AudioContext::update()
{
    audio_update(this);
}

void AudioContext::stop_all_sounds()
{
    audio_stop_all_sounds(this);
}

PlayingSound AudioContext::play_sound(Sound* sound)
{
    return sound_play(this, sound);
}

void Sound::load_from_ogg(const char* ogg_filename)
{
    sound_init_from_ogg_file(this, ogg_filename);
}

void PlayingSound::stop()
{
    sound_stop(*this);
}

void PlayingSound::set_loop(bool loop)
{
    sound_set_loop(*this, loop);
}

void PlayingSound::set_pause(bool pause)
{
    sound_set_pause(*this, pause);
}

}
