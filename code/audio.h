﻿#ifndef DSC_AUDIO_H
#define DSC_AUDIO_H
// TODO(illkwon): Define custom allocation for stb_vorbis and cute_sound
#include "stb_vorbis.h"
#include "cute_sound.h"
#include "primitive_types.h"

namespace dsc
{
struct PlayingSound
{
    cs_playing_sound_t* data;

    void stop();
    void set_loop(bool loop);
    void set_pause(bool pause);
};

struct Sound
{
    cs_loaded_sound_t loaded{};
    cs_play_sound_def_t def{};

    void load_from_ogg(const char* ogg_filename);
};

struct AudioContext
{
    cs_context_t* ctx{};

    explicit AudioContext(Handle64 window);
    ~AudioContext();
    void update();
    void stop_all_sounds();

    PlayingSound play_sound(Sound* sound);
};

}

#endif // DSC_AUDIO_H
