#ifndef DSC_BASE_SCENE_H
#define DSC_BASE_SCENE_H
#include "mathematics.h"

namespace dsc
{
struct Input;
struct AssetStorage;
struct GuiContext;
struct AudioContext;
struct ShaderConstant;

struct BaseScene
{
    BaseScene() = default;
    BaseScene(const BaseScene&) = delete;
    void operator=(const BaseScene&) = delete;
    virtual ~BaseScene() = default;

    virtual void update_and_render(Input* input,
                                   AssetStorage* asset_storage,
                                   GuiContext* gui,
                                   AudioContext* audio,
                                   ShaderConstant* view_proj_constant,
                                   ShaderConstant* light_constant,
                                   ShaderConstant* model_constant,
                                   WindowSize window_size,
                                   float dt) = 0;
};

}

#endif // DSC_BASE_SCENE_H