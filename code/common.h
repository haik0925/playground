﻿#ifndef DSC_COMMON_H
#define DSC_COMMON_H

#include "primitive_types.h"
#include <string.h>

#define global_var static
#define internal_func static

namespace dsc
{
template<typename T, ptrdiff_t N>
constexpr inline ptrdiff_t ssize(T (&arr)[N])
{
    return N;
}

template<typename T, i32 N>
constexpr inline i32 ssize32(T (&arr)[N])
{
    return N;
}

template<typename T>
inline void swap(T& a, T& b)
{
    T temp = a;
    a = b;
    b = temp;
}

constexpr inline u64 kilobytes(u64 n)
{
    return n * 1024LL;
}
constexpr inline u64 megabytes(u64 n)
{
    return kilobytes(n) * 1024LL;
}
constexpr inline u64 gigabytes(u64 n)
{
    return megabytes(n) * 1024LL;
}
constexpr inline u64 terabytes(u64 n)
{
    return gigabytes(n) * 1024LL;
}

constexpr inline u64 operator"" _kb(u64 n)
{
    return kilobytes(n);
}
constexpr inline u64 operator"" _mb(u64 n)
{
    return megabytes(n);
}
constexpr inline u64 operator"" _gb(u64 n)
{
    return gigabytes(n);
}
constexpr inline u64 operator"" _tb(u64 n)
{
    return terabytes(n);
}

}

#if 0
inline void swap_bytes_(void* value_ptr, u64 size)
{
    assert_(size > 0);

    u8* bytes = value_ptr;
    for (u64 lo = 0, hi = size - 1; hi > lo; ++lo, --hi)
    {
        u8 tmp = bytes[lo];
        bytes[lo] = bytes[hi];
        bytes[hi] = tmp;
    }
}

#define swap_bytes(x) swap_bytes_(&x, sizeof(x));

inline u16 read_u16_be(void* p)
{
    u8* b = p;
    u16 result = (b[0] << 8) | b[1];
    return result;
}

inline u32 read_u32_be(void* p)
{
    u8* b = p;
    u32 result = (b[0] << 24) | (b[1] << 16) | (b[2] << 8) | b[3];
    return result;
}

inline u64 read_u64_be(void* p)
{
    u8* b = p;
    u32 hi = read_u32_be(b); b += sizeof(u32);
    u32 lo = read_u32_be(b);
    u64 result = ((u64)hi << 32) | lo;
    return result;
}

inline i16 read_i16_be(void* p)
{
    u16 raw = read_u16_be(p);
    i16 result = *((i16*)&raw);
    return result;
}

inline i64 read_i64_be(void* p)
{
    u64 raw = read_u64_be(p);
    i64 result = *((i64*)&raw);
    return result;
}
#endif

#endif // DSC_COMMON_H
