﻿#ifndef DSC_CONTAINERS_H
#define DSC_CONTAINERS_H

#include <type_traits>
#include <array>
#include <string_view>
#include <string>
#include <new>
#include <stddef.h>
#include "primitive_types.h"
#include "memory.h"

// NOTE: Interfaces of containers follow naming convention of standard c++ for
// compatibility.

namespace dsc
{
using string_view = std::string_view;
// TODO(illkwon): Implement string
using string = std::string;

template<typename T, size_t Count>
using array = std::array<T, Count>;

template<typename T>
void fill_range(T* begin, T* end, const T& value)
{
    static_assert(std::is_copy_constructible_v<T>);
    if constexpr (std::is_trivially_copyable_v<T>)
    {
        auto dst = begin;
        while (dst != end)
        {
            *dst = value;
            ++dst;
        }
    }
    else
    {
        auto dst = begin;
        while (dst != end)
        {
            new (dst) T(value);
            ++dst;
        }
    }
}

template<typename T>
void copy(T* dst, const T& value)
{
    static_assert(std::is_copy_constructible_v<T>);
    if constexpr (std::is_trivially_copyable_v<T>)
    {
        *dst = value;
    }
    else
    {
        new (dst) T(value);
    }
}

template<typename T>
void copy_range(const T* begin, const T* end, T* dst)
{
    static_assert(std::is_copy_constructible_v<T>);
    if constexpr (std::is_trivially_copyable_v<T>)
    {
        memcpy(dst, begin, sizeof(T) * (end - begin));
    }
    else
    {
        auto src = begin;
        while (src != end)
        {
            new (dst) T(*src);
            ++src;
            ++dst;
        }
    }
}

template<typename T>
void move_range(const T* begin, const T* end, T* dst)
{
    static_assert(std::is_move_constructible_v<T>);
    if constexpr (std::is_trivially_move_constructible_v<T>)
    {
        memcpy(dst, begin, sizeof(T) * (end - begin));
    }
    else
    {
        auto src = begin;
        while (src != end)
        {
            new (dst) T(std::move(*src));
            ++src;
            ++dst;
        }
    }
}

template<typename T>
void default_construct(T* dst)
{
    static_assert(std::is_default_constructible_v<T>);
    if constexpr (std::is_trivially_default_constructible_v<T>)
    {
        memset(dst, 0, sizeof(T));
    }
    else
    {
        new (dst) T();
    }
}

template<typename T>
void default_construct_range(T* begin, T* end)
{
    static_assert(std::is_default_constructible_v<T>);
    if constexpr (std::is_trivially_default_constructible_v<T>)
    {
        memset(begin, 0, sizeof(T) * (end - begin));
    }
    else
    {
        auto dst = begin;
        while (dst != end)
        {
            new (dst) T();
            ++dst;
        }
    }
}

template<typename T>
void destruct(T* dst)
{
    if constexpr (!std::is_trivially_destructible_v<T>)
    {
        dst->~T();
    }
}

template<typename T>
void destruct_range(T* begin, T* end)
{
    if constexpr (!std::is_trivially_destructible_v<T>)
    {
        auto dst = begin;
        while (dst != end)
        {
            dst->~T();
            ++dst;
        }
    }
}

template<typename T>
struct static_vector
{
private:
    Allocator* _allocator = nullptr;
    const size_t _capacity = 0;
    size_t _size = 0;
    T* _data = nullptr;

public:
    explicit static_vector(size_t capacity,
                           Allocator* allocator = &GlobalHeapAllocator::get())
        : _allocator(allocator)
        , _capacity(capacity)
        , _data(_allocator->allocate_array<T>(capacity))
    {
    }
    static_vector(const static_vector& other) = delete;
    static_vector(static_vector&& other) = delete;
    ~static_vector()
    {
        destruct_range(begin(), end());
        _allocator->deallocate(_data);
        _allocator = nullptr;
        _size = 0;
        _data = nullptr;
    }

    static_vector& operator=(const static_vector& other) = delete;
    static_vector& operator=(static_vector&& other) = delete;

    const T* data() const
    {
        return _data;
    }
    T* data()
    {
        return _data;
    }
    size_t size() const
    {
        return _size;
    }
    ptrdiff_t ssize() const
    {
        assert_(_size <= INT64_MAX);
        return (ptrdiff_t)_size;
    }
    i32 ssize32() const
    {
        assert_(_size <= INT32_MAX);
        return (i32)_size;
    }
    size_t capacity() const
    {
        return _capacity;
    }
    bool empty() const
    {
        return _size == 0;
    }

    T& operator[](size_t idx)
    {
        assert_(idx < _size);
        return _data[idx];
    }
    const T& operator[](size_t idx) const
    {
        assert_(idx < _size);
        return _data[idx];
    }

    const T& front() const
    {
        return (*this)[0];
    }
    T& front()
    {
        return (*this)[0];
    }
    const T& back() const
    {
        return (*this)[_size - 1];
    }
    T& back()
    {
        return (*this)[_size - 1];
    }

    void clear()
    {
        destruct_range(begin(), end());
        _size = 0;
    }

    void push_back(const T& t)
    {
        assert_(_size < _capacity);
        copy(end(), t);
        ++_size;
    }

    void pop_back()
    {
        if (_size == 0)
        {
            return;
        }

        destruct(end());
        --_size;
    }

    void emplace_back()
    {
        assert_(_size < _capacity);
        default_construct(end());
        ++_size;
    }

    template<typename... Params>
    void emplace_back(Params&&... params)
    {
        assert_(_size < _capacity);
        static_assert(std::is_constructible_v<T, Params>);
        new (end()) T(std::forward<Params>(params)...);
        ++_size;
    }

    // TODO:
    /*void swap(vector& other)
    {
    }*/

    typedef T* iterator;
    iterator begin()
    {
        return _data;
    }
    iterator end()
    {
        return _data == nullptr ? nullptr : _data + _size;
    }

    typedef const T* const_iterator;
    const_iterator begin() const
    {
        return _data;
    }
    const_iterator end() const
    {
        return _data == nullptr ? nullptr : _data + _size;
    }

    bool is_valid_iterator(iterator it)
    {
        if (!_data && it)
        {
            return false;
        }
        else
        {
            return (it - begin()) >= 0 && (end() - it) >= 0;
        }
    }
};

template<typename T>
struct vector
{
private:
    Allocator* _allocator = nullptr;
    size_t _capacity = 0;
    size_t _size = 0;
    T* _data = nullptr;

    void reserve_if_required(size_t required_size)
    {
        if (_capacity < required_size)
        {
            auto newdata = _allocator->allocate_array<T>(required_size);
            move_range(begin(), end(), newdata);
            _allocator->deallocate(_data);
            _data = newdata;
            _capacity = required_size;
        }
    }

    void reserve_uninitialized_memory_if_required(size_t required_size)
    {
        if (_capacity < required_size)
        {
            _allocator->deallocate(_data);
            _data = _allocator->allocate_array<T>(required_size);
            _capacity = required_size;
        }
    }

    void stretch_if_required()
    {
        if (_capacity == _size)
        {
            reserve_if_required(_capacity == 0 ? 1 : _capacity * 3);
        }
    }

public:
    explicit vector(Allocator* allocator = &GlobalHeapAllocator::get())
        : _allocator(allocator)
    {
    }

    vector(const vector& other)
        : _allocator(other._allocator)
        , _capacity(other.capacity())
        , _size(other.size())
        , _data(_allocator->allocate_array<T>(_capacity))
    {
        copy_range(other.data(), other.end(), _data);
    }

    vector(vector&& other)
        : _allocator(other._allocator)
        , _capacity(other.capacity())
        , _size(other.size())
        , _data(other.data())
    {
        other._data = nullptr;
        other._capacity = 0;
        other._size = 0;
    }

    vector(size_t size, Allocator* allocator = &GlobalHeapAllocator::get())
        : _allocator(allocator)
        , _capacity(size)
        , _size(size)
        , _data(_allocator->allocate_array<T>(_capacity))
    {
        default_construct_range(_data, _data + _size);
    }

    vector(size_t size,
           const T& value,
           Allocator* allocator = &GlobalHeapAllocator::get())
        : _allocator(allocator)
        , _capacity(size)
        , _size(size)
        , _data(_allocator->allocate_array<T>(_capacity))
    {
        fill_range(begin(), end(), value);
    }

    ~vector()
    {
        destruct_range(begin(), end());
        _allocator->deallocate(_data);
        _allocator = nullptr;
        _capacity = 0;
        _size = 0;
        _data = nullptr;
    }

    vector& operator=(const vector& other)
    {
        destruct_range(begin(), end());
        reserve_uninitialized_memory_if_required(other.size());
        copy_range(other.data(), other.end(), _data);
        _size = other.size();

        return *this;
    }

    vector& operator=(vector&& other)
    {
        destruct_range(begin(), end());
        reserve_uninitialized_memory_if_required(other.size());
        move_range(other.data(), other.end(), _data);
        _size = other.size();

        return *this;
    }

    const T* data() const
    {
        return _data;
    }
    T* data()
    {
        return _data;
    }
    size_t size() const
    {
        return _size;
    }
    ptrdiff_t ssize() const
    {
        assert_(_size <= INT64_MAX);
        return (ptrdiff_t)_size;
    }
    i32 ssize32() const
    {
        assert_(_size <= INT32_MAX);
        return (i32)_size;
    }
    size_t capacity() const
    {
        return _capacity;
    }
    bool empty() const
    {
        return _size == 0;
    }

    T& operator[](size_t idx)
    {
        assert_(idx < _size);
        return _data[idx];
    }
    const T& operator[](size_t idx) const
    {
        assert_(idx < _size);
        return _data[idx];
    }

    const T& front() const
    {
        return (*this)[0];
    }
    T& front()
    {
        return (*this)[0];
    }
    const T& back() const
    {
        return (*this)[_size - 1];
    }
    T& back()
    {
        return (*this)[_size - 1];
    }

    void resize(size_t size)
    {
        reserve_if_required(size);
        auto offset = _size - size;
        if (offset > 0)
        {
            default_construct_range(end(), end() + offset);
        }
        else if (offset < 0)
        {
            destruct_range(end() + offset, end());
        }
        _size = size;
    }

    void resize(size_t size, const T& value)
    {
        destruct_range(begin(), end());
        reserve_uninitialized_memory_if_required(size);
        _size = size;
        fill_range(begin(), end(), value);
    }

    void clear()
    {
        destruct_range(begin(), end());
        _size = 0;
    }

    void reserve(size_t capacity)
    {
        reserve_if_required(capacity);
    }

    void push_back(const T& t)
    {
        stretch_if_required();
        copy(end(), t);
        ++_size;
    }

    void pop_back()
    {
        if (_size == 0)
        {
            return;
        }

        destruct(end());
        --_size;
    }

    void emplace_back()
    {
        stretch_if_required();
        default_construct(end());
        ++_size;
    }

    template<typename... Params>
    void emplace_back(Params&&... params)
    {
        stretch_if_required();
        static_assert(std::is_constructible_v<T, Params>);
        new (end()) T(std::forward<Params>(params)...);
        ++_size;
    }

    // TODO:
    /*void swap(vector& other)
    {
    }*/

    typedef T* iterator;
    iterator begin()
    {
        return _data;
    }
    iterator end()
    {
        return _data == nullptr ? nullptr : _data + _size;
    }

    typedef const T* const_iterator;
    const_iterator begin() const
    {
        return _data;
    }
    const_iterator end() const
    {
        return _data == nullptr ? nullptr : _data + _size;
    }

    bool is_valid_iterator(iterator it)
    {
        if (!_data && it)
        {
            return false;
        }
        else
        {
            return (it - begin()) >= 0 && (end() - it) >= 0;
        }
    }

    /*
    void insert(iterator where)
    {
        assert_(is_valid_iterator(where));
    }
    void insert(iterator where, const T& value)
    {
        assert_(is_valid_iterator(where));
    }
    void insert(iterator where, const T* first, const T* last)
    {
        assert_(is_valid_iterator(where));
    }
    */
};

template<typename E, typename T>
struct enum_array
{
    array<T, (size_t)(E::Count)> _elems = {};

    const T& operator[](E e) const
    {
        return _elems[(size_t)e];
    }

    T& operator[](E e)
    {
        return _elems[(size_t)e];
    }

    auto begin()
    {
        return _elems.begin();
    }

    auto end()
    {
        return _elems.end();
    }

    static_assert(std::is_enum_v<E>);
    static_assert((i64)(E::Count) > 0);
};
}

#endif // DSC_CONTAINERS_H
