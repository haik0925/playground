﻿#include "debug.h"
#include "common.h"
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN 1
#endif
#include <Windows.h>

namespace dsc
{
typedef struct
{
    u64 ring_buffer_size;
    char* begin;
    char* end;
    char* read_pointer;
    char* write_pointer;

    u64 output_buffer_size;
    char* output_buffer;
} DebugLogBuffer;

global_var DebugLogBuffer g_debug_log_buffer;

void debug_set_global_log_buffer(void* buffer, u64 size)
{
    assert_(size >= 4);
    g_debug_log_buffer.ring_buffer_size = size / 2;
    g_debug_log_buffer.begin = (char*)buffer;
    g_debug_log_buffer.end =
        (char*)((u8*)buffer + g_debug_log_buffer.ring_buffer_size);
    g_debug_log_buffer.read_pointer = g_debug_log_buffer.begin;
    g_debug_log_buffer.write_pointer = g_debug_log_buffer.read_pointer;
    g_debug_log_buffer.output_buffer_size =
        size - g_debug_log_buffer.ring_buffer_size;
    g_debug_log_buffer.output_buffer = g_debug_log_buffer.end;
}

char* debug_get_global_log_buffer()
{
    DebugLogBuffer* log_buffer = &g_debug_log_buffer;

    if (log_buffer->read_pointer - log_buffer->write_pointer > 0)
    {
        memcpy(log_buffer->output_buffer, log_buffer->read_pointer,
               log_buffer->end - log_buffer->read_pointer);
        memcpy(log_buffer->output_buffer +
                   (log_buffer->end - log_buffer->read_pointer),
               log_buffer->begin,
               log_buffer->write_pointer - log_buffer->begin + 1);
    }
    else
    {
        memcpy(log_buffer->output_buffer, log_buffer->read_pointer,
               log_buffer->write_pointer - log_buffer->read_pointer + 1);
    }

    return log_buffer->output_buffer;
}

internal_func u64 debug_get_log_buffer_size(DebugLogBuffer* log_buffer)
{
    u64 buffer_size = 0;
    if (log_buffer->write_pointer - log_buffer->read_pointer >= 0)
    {
        buffer_size = log_buffer->write_pointer - log_buffer->read_pointer;
    }
    else
    {
        buffer_size = (log_buffer->end - log_buffer->read_pointer) +
                      (log_buffer->write_pointer - log_buffer->begin);
    }

    return buffer_size;
}

u64 debug_get_global_log_buffer_size()
{
    u64 buffer_size = debug_get_log_buffer_size(&g_debug_log_buffer);
    return buffer_size;
}

void debug_push_buffer_internal(const char* text)
{
    DebugLogBuffer* log_buffer = &g_debug_log_buffer;

    const u64 len = strlen(text);
    assert_(len + 1 <= log_buffer->ring_buffer_size);

    u64 from_write_to_read =
        log_buffer->ring_buffer_size - debug_get_log_buffer_size(log_buffer);

    i64 overflowed = ((log_buffer->write_pointer + len + 1) - log_buffer->end);

    char* deferred_write_pointer = NULL;
    u64 deferred_write_length = 0;

    if (overflowed > 0)
    {
        memcpy(log_buffer->write_pointer, text,
               sizeof(*text) * (len + 1 - overflowed));
        text += (len + 1 - overflowed);
        deferred_write_pointer = log_buffer->begin;
        deferred_write_length = overflowed;
        log_buffer->write_pointer = log_buffer->begin + overflowed - 1;
    }
    else
    {
        deferred_write_pointer = log_buffer->write_pointer;
        deferred_write_length = len + 1;
        log_buffer->write_pointer += len;
    }

    if (len + 1 > from_write_to_read)
    {
        log_buffer->read_pointer = log_buffer->write_pointer;
        do
        {
            if (log_buffer->read_pointer == log_buffer->end)
                log_buffer->read_pointer = log_buffer->begin;

            if (*log_buffer->read_pointer++ == '\n')
                break;
        } while (log_buffer->read_pointer != log_buffer->write_pointer);
    }

    memcpy(deferred_write_pointer, text, sizeof(*text) * deferred_write_length);
}

void debug_log(const char* format, ...)
{
    char buffer[1000];
    va_list args;
    va_start(args, format);
    vsprintf(buffer, format, args);
    va_end(args);
    OutputDebugStringA(buffer);
    debug_push_buffer_internal(buffer);
}

void debug_log_newline(const char* format, ...)
{
    char buffer[1000];
    va_list args;
    va_start(args, format);
    vsprintf(buffer, format, args);
    va_end(args);
    u64 len = strlen(buffer);
    buffer[len] = '\n';
    buffer[len + 1] = '\0';
    OutputDebugStringA(buffer);
    debug_push_buffer_internal(buffer);
}

u64 debug_get_file_size(const char* filename)
{
    FILE* file = fopen(filename, "rb");
    fseek(file, 0, SEEK_END);
    int file_size = ftell(file);
    fseek(file, 0, SEEK_SET);
    assert_(file_size >= 0);
    fclose(file);

    return (u64)file_size;
}

u64 debug_read_all_binary_file(const char* filename,
                               void* out_buffer,
                               u64 buffer_size)
{
    FILE* file = fopen(filename, "rb");

    if (file == NULL)
        return 0;

    int file_size;
    {
        fseek(file, 0, SEEK_END);
        file_size = ftell(file);
        fseek(file, 0, SEEK_SET);
    }
    assert_(file_size >= 0);
    assert_((u64)file_size <= buffer_size);
    fread(out_buffer, 1, file_size, file);

    fclose(file);

    return file_size;
}

void debug_write_all_binary_file(const char* filename,
                                 void* in_buffer,
                                 u64 buffer_size)
{
    FILE* file = fopen(filename, "wb");
    assert_(file);

    u64 result = fwrite(in_buffer, sizeof(u8), buffer_size, file);
    if (result < buffer_size)
    {
        assert_(ferror(file));
        assert_(feof(file));
    }
    fclose(file);
}

TempObjInfo debug_load_temp_obj_from_raw_data(void* raw_obj_file_data,
                                              u64 raw_obj_file_data_size)
{
    TempObjInfo result = {0};
    tinyobj_parse_obj(&result.attrib, &result.shapes, &result.shape_count,
                      &result.materials, &result.material_count,
                      (const char*)raw_obj_file_data, raw_obj_file_data_size,
                      TINYOBJ_FLAG_TRIANGULATE);

    return result;
}
}
