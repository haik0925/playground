﻿#ifndef DSC_DEBUG_H
#define DSC_DEBUG_H
#include "primitive_types.h"
#include "tinyobj_loader_c.h"
#ifdef _WIN32
#include <intrin.h>
#endif

#ifdef DEBUG
#define log_append(message, ...) dsc::debug_log(message, __VA_ARGS__)
#define log_(message, ...) dsc::debug_log_newline(message, __VA_ARGS__)
#define assert_(expression)                                                    \
    do                                                                         \
    {                                                                          \
        if (!(expression))                                                     \
            __debugbreak();                                                    \
    } while (0)
#else
#define log_append(message, ...)
#define log_(message, ...)
#define assert_(expression) (expression)
#endif // DEBUG

namespace dsc
{
void debug_set_global_log_buffer(void* buffer, u64 size);
char* debug_get_global_log_buffer();
u64 debug_get_global_log_buffer_size();
void debug_log(const char* format, ...);
void debug_log_newline(const char* format, ...);
u64 debug_get_file_size(const char* filename);
u64 debug_read_all_binary_file(const char* filename,
                               void* out_buffer,
                               u64 buffer_size);
void debug_write_all_binary_file(const char* filename,
                                 void* in_buffer,
                                 u64 buffer_size);

typedef struct TempObjInfo
{
    int vertex_count;

    tinyobj_attrib_t attrib;
    tinyobj_shape_t* shapes;
    size_t shape_count;
    tinyobj_material_t* materials;
    size_t material_count;
} TempObjInfo;

TempObjInfo debug_load_temp_obj_from_raw_data(void* raw_obj_file_data,
                                              u64 raw_obj_file_data_size);
}

#endif // DSC_DEBUG_H
