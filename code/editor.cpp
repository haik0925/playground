﻿#include "editor.h"
#include "game_scene.h"
#include "debug.h"
#include "physics.h"
#include "common.h"
#include "asset.h"
#include <math.h>

namespace dsc
{
void Editor::set_mode(Input* input, EditorMode mode)
{
    switch (mode)
    {
    case EditorMode::Editing:
        input->show_cursor = true;
        input->lock_mouse_to_center = false;
        break;
    case EditorMode::Floating:
        input->show_cursor = false;
        input->lock_mouse_to_center = true;
        break;
    default: assert_(false);
    }

    editor_mode = mode;
}

void Editor::update(Camera* camera,
                    Room* rooms,
                    int room_count,
                    Input* input,
                    const WindowSize window_size,
                    float dt)
{
    // reset selected entity material
    if (editor_selected_entity_index != -1)
        deselect();

    // change editor mode
    if (input->button_toggle_editor_edit_mode.is_released())
    {
        switch (editor_mode)
        {
        case EditorMode::Editing: set_mode(input, EditorMode::Floating); break;
        case EditorMode::Floating: set_mode(input, EditorMode::Editing); break;
        default: assert_(false);
        }
    }

    if (input->button_editor_save.is_released())
    {
        log_("Doesn't work for now");
        // editor_save_all_room_templates(game);
    }

    // Calculate & apply velocity
    switch (editor_mode)
    {
    case EditorMode::Editing: break;
    case EditorMode::Floating:
        camera->look_first_person(input, 5, dt);
        camera->float_toward_look(input, 60, dt);
        break;
    default: assert_(false);
    }

    // Entity selection
    if (input->button_left_click.is_pressed())
    {
        Vec3 editor_ray;
        if (editor_mode == EditorMode::Floating)
        {
            editor_ray = camera->get_look();
        }
        else
        {
            // temporary, calculating view and proj here.
            // need better solution later.
            Mat4 view = camera->get_view_mat();
            const Mat4 proj =
                mat4_persp(camera_fov,
                           (float)window_size.width / (float)window_size.height,
                           camera_near, camera_far);

            editor_ray = screen_to_world_ray(input->mouse_pos, window_size,
                                             camera->pos, proj, view);
        }

        float ray_closest_collision_dist = float_max;
        float ray_current_collision_dist = float_max;
        editor_selected_entity_index = -1;

        for (int i = 0; i < room_count; ++i)
        {
            Room* room = &rooms[room_count];
            RoomTemplate* room_template = room->room_template;

            if (physics_collision_check_ray_aabb(
                    camera->pos, editor_ray, room->pos,
                    room_template->room_size * 0.5f,
                    &ray_current_collision_dist))
            {
                // check all props in the room

                for (int j = 0; j < room_template->static_entity_count; ++j)
                    if (physics_collision_check_ray_aabb(
                            camera->pos, editor_ray,
                            room->pos + room_template->static_entities[j].pos,
                            room_template->static_entities[j].size * 0.5f,
                            &ray_current_collision_dist))
                    {
                        if (ray_current_collision_dist < 0)
                            continue;
                        if (ray_closest_collision_dist >
                            ray_current_collision_dist)
                        {
                            editor_selected_entity_index = j;
                            editor_selected_entity_type =
                                EditorEntityType::Static;
                            editor_selected_room_template = room_template;
                            ray_closest_collision_dist =
                                ray_current_collision_dist;
                        }
                    }

                /*
                // check walls
                for (int j = 0; j < room_template->walls_size; ++j)
                    if (physics_collision_check_ray_aabb(game->camera.pos,
                editor_ray, vec3_add(game->room[i].pos,
                room_template->walls[j].pos),
                        room_template->walls[j].size * 0.5f,
                        &ray_current_collision_dist))
                    {
                        if (ray_current_collision_dist < 0) continue;
                        if (ray_closest_collision_dist >
                ray_current_collision_dist)
                        {
                            editor->editor_selected_entity_index = j;
                            editor->editor_selected_entity_type =
                EditorEntityType_Wall; editor->editor_selected_room_template =
                room_template; ray_closest_collision_dist =
                ray_current_collision_dist;
                        }
                    }

                // check grounds
                for (int j = 0; j < room_template->grounds_size; ++j)
                    if (physics_collision_check_ray_aabb(game->camera.pos,
                editor_ray, vec3_add(game->room[i].pos,
                room_template->grounds[j].pos),
                        room_template->grounds[j].size * 0.5f,
                        &ray_current_collision_dist))
                    {
                        if (ray_current_collision_dist < 0) continue;
                        if (ray_closest_collision_dist >
                ray_current_collision_dist)
                        {
                            editor->editor_selected_entity_index = j;
                            editor->editor_selected_entity_type =
                EditorEntityType_Ground; editor->editor_selected_room_template =
                room_template; ray_closest_collision_dist =
                ray_current_collision_dist;
                        }
                    }
                    */
            }
        }
    }

    // set selected entity material
    // it is temporary, be careful with material color.
    if (editor_selected_entity_index != -1)
    {
        Entity* selected_entity = NULL;

        switch (editor_selected_entity_type)
        {
        case EditorEntityType::Static:
            selected_entity =
                &editor_selected_room_template
                     ->static_entities[editor_selected_entity_index];
            break;

        default: assert_(false);
        }

        editor_previous_selected_entity_color = selected_entity->color;
        selected_entity->color = color_red().xyz();
    }

    if (input->button_editor_redo.is_pressed())
        redo();
    if (input->button_editor_undo.is_pressed())
        undo();
    if (input->button_editor_delete.is_pressed())
        remove();
}

void Editor::redo()
{
    log_(
        "Redo is currently locked. You can have this feature with only 99.99$!");
}

void Editor::undo()
{
    log_(
        "Undo is currently locked. You can have this feature with only 99.99$!");
}

void Editor::remove()
{
    if (editor_selected_entity_index != -1)
    {
        /*
        Entity* entities = NULL;
        u16* entities_size = NULL;
        switch (editor->editor_selected_entity_type)
        {
        case EditorEntityType_Wall:
            entities = editor->editor_selected_room_template->walls;
            entities_size = &editor->editor_selected_room_template->walls_size;
            break;

        case EditorEntityType_Ground:
            entities = editor->editor_selected_room_template->grounds;
            entities_size =
        &editor->editor_selected_room_template->grounds_size; break;
        }

        assert_(entities);
        assert_(*entities_size);
        */

        // add it to history
        EditorAction action{};
        action.action_type = EditorActionType::Delete;
        action.deleted_entity_type = editor_selected_entity_type;
        action.entity_backup =
            editor_selected_room_template
                ->static_entities[editor_selected_entity_index];
        action_push(action);

        // delete the selected entity
        editor_selected_room_template
            ->static_entities[editor_selected_entity_index] =
            editor_selected_room_template->static_entities
                [editor_selected_room_template->static_entity_count - 1];
        --editor_selected_room_template->static_entity_count;
        editor_selected_entity_index = -1;
    }
}

void Editor::deselect()
{
    Entity* selected_entity = NULL;

    switch (editor_selected_entity_type)
    {
    case EditorEntityType::Static:
        selected_entity = &editor_selected_room_template
                               ->static_entities[editor_selected_entity_index];
        break;

    default: assert_(false);
    }

    // it is temporary, be careful with material color.
    selected_entity->color = editor_previous_selected_entity_color;
}

void Editor::action_push(EditorAction editor_action)
{
    // add it at current
    editor_history[editor_history_current] = editor_action;

    // change curr and tail
    bool latest = editor_history_current == editor_history_tail;
    ++editor_history_current;
    if (editor_history_current >= max_editor_history_count)
        editor_history_current -= max_editor_history_count;
    if (latest)
        editor_history_tail = editor_history_current;
    if (editor_history_count == max_editor_history_count)
        editor_history_head = editor_history_tail;

    // change count
    editor_history_count =
        (editor_history_tail <= editor_history_head ? max_editor_history_count :
                                                      0) +
        editor_history_tail - editor_history_head;
}
/*
file format (in order):
int                                    entity_variables_count
int[entity_variables_count]            EntityVariableClarifiers
int                                    room_template_count

RoomTemplateType_Count amount of:
int                                    static_entity_count
Entity[static_entity_count]          static_entities
*/

#if 0
void editor_save_all_room_templates(GameScene* game)
{
    //backup the current data first
    void* backup_buffer = temp_memory_alloc(room_template_binary_file_buffer_size);
    debug_write_all_binary_file(room_template_data_backup_path, backup_buffer, debug_read_all_binary_file(room_template_data_path, backup_buffer, room_template_binary_file_buffer_size));

    u64 buffer_left_size = room_template_binary_file_buffer_size;
    void* buffer = temp_memory_alloc(room_template_binary_file_buffer_size);
    void* buffer_current_pos = buffer;

    assert_(sizeof(int) * (2 + entity_variables_count) <= buffer_left_size);
    buffer_left_size -= sizeof(int) * (2 + entity_variables_count);

    // add header to the file
    *(int*)buffer_current_pos = entity_variables_count; // entity_variables_count
    buffer_current_pos = (int*)buffer_current_pos + 1;

    for (int i = 0; i < entity_variables_count; ++i)
    {
        *(int*)buffer_current_pos = game->entity_variable_clarifier[i]; // EntityVariableClarifier
        buffer_current_pos = (int*)buffer_current_pos + 1;
    }

    *(int*)buffer_current_pos = game->room_template_count; // room_template_count
    buffer_current_pos = (int*)buffer_current_pos + 1;

    //needs room_template_count and RoomTemplateType_Count check here!

    //save room templates
    for(int i = 0; i < game->room_template_count; ++i)
        room_template_save(&game->room_templates[i], &buffer_current_pos, &buffer_left_size);

    //write to file
    debug_write_all_binary_file(room_template_data_path, buffer, room_template_binary_file_buffer_size - buffer_left_size);

    log_("Successfully saved! (Previous room template version is saved as backup)");
}
#endif

void room_template_save(RoomTemplate* room_template,
                        void** empty_binary_file,
                        u64* empty_binary_file_size)
{
    u64 total_using_size =
        sizeof(int) * 2 + sizeof(Entity) * room_template->static_entity_count;
    assert_(total_using_size <= *empty_binary_file_size);

    void* current_binary_file = *empty_binary_file;

    *(int*)current_binary_file =
        room_template->static_entity_count; // static_entity_count
    current_binary_file = (int*)current_binary_file + 1;

    for (int i = 0; i < room_template->static_entity_count; ++i)
    {
        *(Entity*)current_binary_file =
            room_template->static_entities[i]; // Entity
        current_binary_file = (Entity*)current_binary_file + 1;
    }

    *empty_binary_file_size -= total_using_size;
    *empty_binary_file = (u8*)*empty_binary_file + total_using_size;
}

internal_func void
    room_template__temp_load_1(RoomTemplate* empty_room_template);
internal_func void
    room_template__temp_load_2(RoomTemplate* empty_room_template);
internal_func void
    room_template__temp_load_3(RoomTemplate* empty_room_template);
internal_func void
    room_template__temp_load_4(RoomTemplate* empty_room_template);
internal_func void
    room_template__temp_load_5(RoomTemplate* empty_room_template);
internal_func void
    room_template__temp_load_6(RoomTemplate* empty_room_template);
internal_func void
    room_template__temp_load_7(RoomTemplate* empty_room_template);
internal_func void
    room_template__temp_load_8(RoomTemplate* empty_room_template);
internal_func void
    room_template__temp_load_9(RoomTemplate* empty_room_template);
internal_func void
    room_template__temp_load_10(RoomTemplate* empty_room_template);
internal_func void
    room_template__temp_load_11(RoomTemplate* empty_room_template);
internal_func void
    room_template__temp_load_12(RoomTemplate* empty_room_template);

#if 0
void editor_load_all_room_templates(GameScene* game)
{
    // disabled binary file I/O for now.
    /*
    void* buffer = temp_memory_alloc(room_template_binary_file_buffer_size);
    void* buffer_current_pos = buffer;
     debug_read_all_binary_file(room_template_data_path, buffer, room_template_binary_file_buffer_size);

    // load header
    int loaded_entity_variable_clarifier_count = *(int*)buffer_current_pos; // previous entity_variables_count
    buffer_current_pos = (int*)buffer_current_pos + 1;
    
    EntityVariableClarifier* loaded_entity_variable_clarifiers = temp_memory_alloc(loaded_entity_variable_clarifier_count * sizeof(int));
    for (int i = 0; i < loaded_entity_variable_clarifier_count; ++i)
    {
        loaded_entity_variable_clarifiers[i] = *(int*)buffer_current_pos;
        buffer_current_pos = (int*)buffer_current_pos + 1;
    }
    
    // Entity data loss check
    for (int i = 0; i < loaded_entity_variable_clarifier_count; ++i)
    {
        int j = 0;
        for (; j < entity_variables_count; ++j)
            if (loaded_entity_variable_clarifiers[i] == game->entity_variable_clarifier[j])
                break;
        if (j == entity_variables_count)
            log_("EntityVariableClarifier : %i is not in current entity variables. There will be data loss...", i);
    }
    
    game->room_template_count = *(int*)buffer_current_pos; // previous room_template_count
    buffer_current_pos = (int*)buffer_current_pos + 1;

    assert_(game->room_template_count <= max_room_template_count);

    for (int i = 0; i < game->room_template_count; ++i)
    {        
        room_template_load(&game->room_templates[i], loaded_entity_variable_clarifiers, loaded_entity_variable_clarifier_count, &buffer_current_pos);
        room_template_calculate_size(&game->room_templates[i]);
    }*/

    // temporary room template loaders
    room_template__temp_load_1(&game->room_templates[RoomTemplateType_1]);
    room_template_calculate_size(&game->room_templates[RoomTemplateType_1]);
    room_template__temp_load_2(&game->room_templates[RoomTemplateType_2]);
    room_template_calculate_size(&game->room_templates[RoomTemplateType_2]);
    room_template__temp_load_3(&game->room_templates[RoomTemplateType_3]);
    room_template_calculate_size(&game->room_templates[RoomTemplateType_3]);
    room_template__temp_load_4(&game->room_templates[RoomTemplateType_4]);
    room_template_calculate_size(&game->room_templates[RoomTemplateType_4]);
    room_template__temp_load_5(&game->room_templates[RoomTemplateType_5]);
    room_template_calculate_size(&game->room_templates[RoomTemplateType_5]);
    room_template__temp_load_6(&game->room_templates[RoomTemplateType_6]);
    room_template_calculate_size(&game->room_templates[RoomTemplateType_6]);
    room_template__temp_load_7(&game->room_templates[RoomTemplateType_7]);
    room_template_calculate_size(&game->room_templates[RoomTemplateType_7]);
    room_template__temp_load_8(&game->room_templates[RoomTemplateType_8]);
    room_template_calculate_size(&game->room_templates[RoomTemplateType_8]);
    room_template__temp_load_9(&game->room_templates[RoomTemplateType_9]);
    room_template_calculate_size(&game->room_templates[RoomTemplateType_9]);
    room_template__temp_load_10(&game->room_templates[RoomTemplateType_10]);
    room_template_calculate_size(&game->room_templates[RoomTemplateType_10]);
    room_template__temp_load_11(&game->room_templates[RoomTemplateType_11]);
    room_template_calculate_size(&game->room_templates[RoomTemplateType_11]);
    room_template__temp_load_12(&game->room_templates[RoomTemplateType_12]);
    room_template_calculate_size(&game->room_templates[RoomTemplateType_12]);
}
#endif

// unused function for now.
void room_template_load(
    RoomTemplate* empty_room_template,
    EntityVariableClarifier* loaded_entity_variable_clarifiers,
    int loaded_entity_variable_clarifiers_count,
    void** raw_binary_file)
{
    void* current_binary_file = *raw_binary_file;

    empty_room_template->static_entity_count = *(int*)current_binary_file;
    current_binary_file = (int*)current_binary_file + 1;

    for (int i = 0; i < empty_room_template->static_entity_count; ++i)
    {
        for (int j = 0; j < loaded_entity_variable_clarifiers_count; ++j)
            empty_room_template->static_entities[i].load(
                loaded_entity_variable_clarifiers[j], &current_binary_file);
    }

    /*
    // temporary!
    Vec3 ground_color = {116 / 255.f, 116 / 255.f, 116 / 255.f};
    Vec3 wall_color = {130 / 255.f, 130 / 255.f, 130 / 255.f};
    Vec3 door_color = {16 / 255.f, 90 / 255.f, 67 / 255.f};

    empty_room_template->static_entity_count = 14;
    //grounds
    empty_room_template->static_entities[0].pos = {0, -10.5f, 0};
    empty_room_template->static_entities[0].size = {52, 1, 52};
    empty_room_template->static_entities[0].color = ground_color;
    empty_room_template->static_entities[0].texture_index = TextureType_Ground;
    empty_room_template->static_entities[0].model_index = ModelType_CubeTest;
    empty_room_template->static_entities[1].pos = {15, -8.5f, 15};
    empty_room_template->static_entities[1].size = {20, 3, 20};
    empty_room_template->static_entities[1].color = ground_color;
    empty_room_template->static_entities[1].texture_index = TextureType_Ground;
    empty_room_template->static_entities[1].model_index = ModelType_CubeTest;
    //walls
    empty_room_template->static_entities[2].pos = {0, 6, 25.5f};
    empty_room_template->static_entities[2].size = {50, 8, 1};
    empty_room_template->static_entities[2].color = wall_color;
    empty_room_template->static_entities[2].texture_index = TextureType_Wall;
    empty_room_template->static_entities[2].model_index = ModelType_CubeTest;
    empty_room_template->static_entities[3].pos = {-15, -4, 25.5f};
    empty_room_template->static_entities[3].size = {20, 12, 1};
    empty_room_template->static_entities[3].color = wall_color;
    empty_room_template->static_entities[3].texture_index = TextureType_Wall;
    empty_room_template->static_entities[3].model_index = ModelType_CubeTest;
    empty_room_template->static_entities[4].pos = {15, -4, 25.5f};
    empty_room_template->static_entities[4].size = {20, 12, 1};
    empty_room_template->static_entities[4].color = wall_color;
    empty_room_template->static_entities[4].texture_index = TextureType_Wall;
    empty_room_template->static_entities[4].model_index = ModelType_CubeTest;
    empty_room_template->static_entities[5].pos = {25.5f, 6, 0};
    empty_room_template->static_entities[5].size = {1, 8, 50};
    empty_room_template->static_entities[5].color = wall_color;
    empty_room_template->static_entities[5].texture_index = TextureType_Wall;
    empty_room_template->static_entities[5].model_index = ModelType_CubeTest;
    empty_room_template->static_entities[6].pos = {25.5f, -4, -15};
    empty_room_template->static_entities[6].size = {1, 12, 20};
    empty_room_template->static_entities[6].color = wall_color;
    empty_room_template->static_entities[6].texture_index = TextureType_Wall;
    empty_room_template->static_entities[6].model_index = ModelType_CubeTest;
    empty_room_template->static_entities[7].pos = {25.5f, -4, 15};
    empty_room_template->static_entities[7].size = {1, 12, 20};
    empty_room_template->static_entities[7].color = wall_color;
    empty_room_template->static_entities[7].texture_index = TextureType_Wall;
    empty_room_template->static_entities[7].model_index = ModelType_CubeTest;
    empty_room_template->static_entities[8].pos = {0, 6, -25.5f};
    empty_room_template->static_entities[8].size = {50, 8, 1};
    empty_room_template->static_entities[8].color = wall_color;
    empty_room_template->static_entities[8].texture_index = TextureType_Wall;
    empty_room_template->static_entities[8].model_index = ModelType_CubeTest;
    empty_room_template->static_entities[9].pos = {-15, -4, -25.5f};
    empty_room_template->static_entities[9].size = {20, 12, 1};
    empty_room_template->static_entities[9].color = wall_color;
    empty_room_template->static_entities[9].texture_index = TextureType_Wall;
    empty_room_template->static_entities[9].model_index = ModelType_CubeTest;
    empty_room_template->static_entities[10].pos = {15, -4, -25.5f};
    empty_room_template->static_entities[10].size = {20, 12, 1};
    empty_room_template->static_entities[10].color = wall_color;
    empty_room_template->static_entities[10].texture_index = TextureType_Wall;
    empty_room_template->static_entities[10].model_index = ModelType_CubeTest;
    empty_room_template->static_entities[11].pos = {-25.5f, 6, 0};
    empty_room_template->static_entities[11].size = {1, 8, 50};
    empty_room_template->static_entities[11].color = wall_color;
    empty_room_template->static_entities[11].texture_index = TextureType_Wall;
    empty_room_template->static_entities[11].model_index = ModelType_CubeTest;
    empty_room_template->static_entities[12].pos = {-25.5f, -4, -15};
    empty_room_template->static_entities[12].size = {1, 12, 20};
    empty_room_template->static_entities[12].color = wall_color;
    empty_room_template->static_entities[12].texture_index = TextureType_Wall;
    empty_room_template->static_entities[12].model_index = ModelType_CubeTest;
    empty_room_template->static_entities[13].pos = {-25.5f, -4, 15};
    empty_room_template->static_entities[13].size = {1, 12, 20};
    empty_room_template->static_entities[13].color = wall_color;
    empty_room_template->static_entities[13].texture_index = TextureType_Wall;
    empty_room_template->static_entities[13].model_index = ModelType_CubeTest;
    */

    /*
    empty_room_template->doors_size = 4;
    empty_room_template->doors[0].pos = {0, -4, 25.5f};
    empty_room_template->doors[0].size = {10, 12, 1};
    empty_room_template->doors[0].color = door_color;

    empty_room_template->doors[1].pos = {25.5f, -4, 0};
    empty_room_template->doors[1].size = {1, 12, 10};
    empty_room_template->doors[1].color = door_color;

    empty_room_template->doors[2].pos = {0, -4, -25.5f};
    empty_room_template->doors[2].size = {10, 12, 1};
    empty_room_template->doors[2].color = door_color;

    empty_room_template->doors[3].pos = {-25.5f, -4, 0};
    empty_room_template->doors[3].size = {1, 12, 10};
    empty_room_template->doors[3].color = door_color;
    */
}

void room_template__temp_load_1(RoomTemplate* empty_room_template)
{
    Vec3 ground_color{116 / 255.f, 116 / 255.f, 116 / 255.f};
    Vec3 wall_color{130 / 255.f, 130 / 255.f, 130 / 255.f};
    Vec3 door_color{16 / 255.f, 90 / 255.f, 67 / 255.f};

    empty_room_template->static_entity_count = 7;
    // grounds
    empty_room_template->static_entities[0].pos = {0, -10.5f, 0};
    empty_room_template->static_entities[0].size = {52, 1, 52};
    empty_room_template->static_entities[0].color = ground_color;
    empty_room_template->static_entities[0].texture_index = TextureType::Ground;
    empty_room_template->static_entities[0].model_index = ModelType::CubeTest;
    // walls
    empty_room_template->static_entities[1].pos = {0, 0, 25.5f}; // up
    empty_room_template->static_entities[1].size = {50, 20, 1};
    empty_room_template->static_entities[1].color = wall_color;
    empty_room_template->static_entities[1].texture_index = TextureType::Wall;
    empty_room_template->static_entities[1].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[2].pos = {25.5f, 6, 0}; // right
    empty_room_template->static_entities[2].size = {1, 8, 50};
    empty_room_template->static_entities[2].color = wall_color;
    empty_room_template->static_entities[2].texture_index = TextureType::Wall;
    empty_room_template->static_entities[2].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[3].pos = {25.5f, -4, -15};
    empty_room_template->static_entities[3].size = {1, 12, 20};
    empty_room_template->static_entities[3].color = wall_color;
    empty_room_template->static_entities[3].texture_index = TextureType::Wall;
    empty_room_template->static_entities[3].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[4].pos = {25.5f, -4, 15};
    empty_room_template->static_entities[4].size = {1, 12, 20};
    empty_room_template->static_entities[4].color = wall_color;
    empty_room_template->static_entities[4].texture_index = TextureType::Wall;
    empty_room_template->static_entities[4].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[5].pos = {0, 0, -25.5f}; // down
    empty_room_template->static_entities[5].size = {50, 20, 1};
    empty_room_template->static_entities[5].color = wall_color;
    empty_room_template->static_entities[5].texture_index = TextureType::Wall;
    empty_room_template->static_entities[5].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[6].pos = {-25.5f, 0, 0}; // left
    empty_room_template->static_entities[6].size = {1, 20, 50};
    empty_room_template->static_entities[6].color = wall_color;
    empty_room_template->static_entities[6].texture_index = TextureType::Wall;
    empty_room_template->static_entities[6].model_index = ModelType::CubeTest;
}

void room_template__temp_load_2(RoomTemplate* empty_room_template)
{
    Vec3 ground_color = {116 / 255.f, 116 / 255.f, 116 / 255.f};
    Vec3 wall_color = {130 / 255.f, 130 / 255.f, 130 / 255.f};
    Vec3 door_color = {16 / 255.f, 90 / 255.f, 67 / 255.f};

    empty_room_template->static_entity_count = 11;
    // grounds
    empty_room_template->static_entities[0].pos = {26, -10.5f, 0};
    empty_room_template->static_entities[0].size = {104, 1, 52};
    empty_room_template->static_entities[0].color = ground_color;
    empty_room_template->static_entities[0].texture_index = TextureType::Ground;
    empty_room_template->static_entities[0].model_index = ModelType::CubeTest;
    // walls
    empty_room_template->static_entities[1].pos = {26, 0, 25.5f}; // up
    empty_room_template->static_entities[1].size = {102, 20, 1};
    empty_room_template->static_entities[1].color = wall_color;
    empty_room_template->static_entities[1].texture_index = TextureType::Wall;
    empty_room_template->static_entities[1].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[2].pos = {77.5f, 6, 0}; // right
    empty_room_template->static_entities[2].size = {1, 8, 50};
    empty_room_template->static_entities[2].color = wall_color;
    empty_room_template->static_entities[2].texture_index = TextureType::Wall;
    empty_room_template->static_entities[2].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[3].pos = {77.5f, -4, -15};
    empty_room_template->static_entities[3].size = {1, 12, 20};
    empty_room_template->static_entities[3].color = wall_color;
    empty_room_template->static_entities[3].texture_index = TextureType::Wall;
    empty_room_template->static_entities[3].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[4].pos = {77.5f, -4, 15};
    empty_room_template->static_entities[4].size = {1, 12, 20};
    empty_room_template->static_entities[4].color = wall_color;
    empty_room_template->static_entities[4].texture_index = TextureType::Wall;
    empty_room_template->static_entities[4].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[5].pos = {26, 6, -25.5f}; // down
    empty_room_template->static_entities[5].size = {102, 8, 1};
    empty_room_template->static_entities[5].color = wall_color;
    empty_room_template->static_entities[5].texture_index = TextureType::Wall;
    empty_room_template->static_entities[5].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[6].pos = {-15, -4, -25.5f};
    empty_room_template->static_entities[6].size = {20, 12, 1};
    empty_room_template->static_entities[6].color = wall_color;
    empty_room_template->static_entities[6].texture_index = TextureType::Wall;
    empty_room_template->static_entities[6].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[7].pos = {41, -4, -25.5f};
    empty_room_template->static_entities[7].size = {72, 12, 1};
    empty_room_template->static_entities[7].color = wall_color;
    empty_room_template->static_entities[7].texture_index = TextureType::Wall;
    empty_room_template->static_entities[7].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[8].pos = {-25.5f, 6, 0}; // left
    empty_room_template->static_entities[8].size = {1, 8, 50};
    empty_room_template->static_entities[8].color = wall_color;
    empty_room_template->static_entities[8].texture_index = TextureType::Wall;
    empty_room_template->static_entities[8].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[9].pos = {-25.5f, -4, -15};
    empty_room_template->static_entities[9].size = {1, 12, 20};
    empty_room_template->static_entities[9].color = wall_color;
    empty_room_template->static_entities[9].texture_index = TextureType::Wall;
    empty_room_template->static_entities[9].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[10].pos = {-25.5f, -4, 15};
    empty_room_template->static_entities[10].size = {1, 12, 20};
    empty_room_template->static_entities[10].color = wall_color;
    empty_room_template->static_entities[10].texture_index = TextureType::Wall;
    empty_room_template->static_entities[10].model_index = ModelType::CubeTest;
}

void room_template__temp_load_3(RoomTemplate* empty_room_template)
{
    Vec3 ground_color = {116 / 255.f, 116 / 255.f, 116 / 255.f};
    Vec3 wall_color = {130 / 255.f, 130 / 255.f, 130 / 255.f};
    Vec3 door_color = {16 / 255.f, 90 / 255.f, 67 / 255.f};

    empty_room_template->static_entity_count = 9;
    // grounds
    empty_room_template->static_entities[0].pos = {0, -10.5f, -26};
    empty_room_template->static_entities[0].size = {52, 1, 104};
    empty_room_template->static_entities[0].color = ground_color;
    empty_room_template->static_entities[0].texture_index = TextureType::Ground;
    empty_room_template->static_entities[0].model_index = ModelType::CubeTest;
    // walls
    empty_room_template->static_entities[1].pos = {0, 0, 25.5f}; // up
    empty_room_template->static_entities[1].size = {50, 20, 1};
    empty_room_template->static_entities[1].color = wall_color;
    empty_room_template->static_entities[1].texture_index = TextureType::Wall;
    empty_room_template->static_entities[1].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[2].pos = {25.5f, 6, -26}; // right
    empty_room_template->static_entities[2].size = {1, 8, 102};
    empty_room_template->static_entities[2].color = wall_color;
    empty_room_template->static_entities[2].texture_index = TextureType::Wall;
    empty_room_template->static_entities[2].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[3].pos = {25.5f, -4, -67};
    empty_room_template->static_entities[3].size = {1, 12, 20};
    empty_room_template->static_entities[3].color = wall_color;
    empty_room_template->static_entities[3].texture_index = TextureType::Wall;
    empty_room_template->static_entities[3].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[4].pos = {25.5f, -4, -11};
    empty_room_template->static_entities[4].size = {1, 12, 72};
    empty_room_template->static_entities[4].color = wall_color;
    empty_room_template->static_entities[4].texture_index = TextureType::Wall;
    empty_room_template->static_entities[4].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[5].pos = {0, 0, -77.5f}; // down
    empty_room_template->static_entities[5].size = {50, 20, 1};
    empty_room_template->static_entities[5].color = wall_color;
    empty_room_template->static_entities[5].texture_index = TextureType::Wall;
    empty_room_template->static_entities[5].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[6].pos = {-25.5f, 6, -26}; // left
    empty_room_template->static_entities[6].size = {1, 8, 102};
    empty_room_template->static_entities[6].color = wall_color;
    empty_room_template->static_entities[6].texture_index = TextureType::Wall;
    empty_room_template->static_entities[6].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[7].pos = {-25.5f, -4, -41};
    empty_room_template->static_entities[7].size = {1, 12, 72};
    empty_room_template->static_entities[7].color = wall_color;
    empty_room_template->static_entities[7].texture_index = TextureType::Wall;
    empty_room_template->static_entities[7].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[8].pos = {-25.5f, -4, 15};
    empty_room_template->static_entities[8].size = {1, 12, 20};
    empty_room_template->static_entities[8].color = wall_color;
    empty_room_template->static_entities[8].texture_index = TextureType::Wall;
    empty_room_template->static_entities[8].model_index = ModelType::CubeTest;
}

void room_template__temp_load_4(RoomTemplate* empty_room_template)
{
    Vec3 ground_color = {116 / 255.f, 116 / 255.f, 116 / 255.f};
    Vec3 wall_color = {130 / 255.f, 130 / 255.f, 130 / 255.f};
    Vec3 door_color = {16 / 255.f, 90 / 255.f, 67 / 255.f};

    empty_room_template->static_entity_count = 9;
    // grounds
    empty_room_template->static_entities[0].pos = {0, -10.5f, 0};
    empty_room_template->static_entities[0].size = {52, 1, 52};
    empty_room_template->static_entities[0].color = ground_color;
    empty_room_template->static_entities[0].texture_index = TextureType::Ground;
    empty_room_template->static_entities[0].model_index = ModelType::CubeTest;
    // walls
    empty_room_template->static_entities[1].pos = {0, 6, 25.5f}; // up
    empty_room_template->static_entities[1].size = {50, 8, 1};
    empty_room_template->static_entities[1].color = wall_color;
    empty_room_template->static_entities[1].texture_index = TextureType::Wall;
    empty_room_template->static_entities[1].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[2].pos = {-15, -4, 25.5f};
    empty_room_template->static_entities[2].size = {20, 12, 1};
    empty_room_template->static_entities[2].color = wall_color;
    empty_room_template->static_entities[2].texture_index = TextureType::Wall;
    empty_room_template->static_entities[2].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[3].pos = {15, -4, 25.5f};
    empty_room_template->static_entities[3].size = {20, 12, 1};
    empty_room_template->static_entities[3].color = wall_color;
    empty_room_template->static_entities[3].texture_index = TextureType::Wall;
    empty_room_template->static_entities[3].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[4].pos = {25.5f, 0, 0}; // right
    empty_room_template->static_entities[4].size = {1, 20, 50};
    empty_room_template->static_entities[4].color = wall_color;
    empty_room_template->static_entities[4].texture_index = TextureType::Wall;
    empty_room_template->static_entities[4].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[5].pos = {0, 6, -25.5f}; // down
    empty_room_template->static_entities[5].size = {50, 8, 1};
    empty_room_template->static_entities[5].color = wall_color;
    empty_room_template->static_entities[5].texture_index = TextureType::Wall;
    empty_room_template->static_entities[5].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[6].pos = {-15, -4, -25.5f};
    empty_room_template->static_entities[6].size = {20, 12, 1};
    empty_room_template->static_entities[6].color = wall_color;
    empty_room_template->static_entities[6].texture_index = TextureType::Wall;
    empty_room_template->static_entities[6].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[7].pos = {15, -4, -25.5f};
    empty_room_template->static_entities[7].size = {20, 12, 1};
    empty_room_template->static_entities[7].color = wall_color;
    empty_room_template->static_entities[7].texture_index = TextureType::Wall;
    empty_room_template->static_entities[7].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[8].pos = {-25.5f, 0, 0}; // left
    empty_room_template->static_entities[8].size = {1, 20, 50};
    empty_room_template->static_entities[8].color = wall_color;
    empty_room_template->static_entities[8].texture_index = TextureType::Wall;
    empty_room_template->static_entities[8].model_index = ModelType::CubeTest;
}

void room_template__temp_load_5(RoomTemplate* empty_room_template)
{
    Vec3 ground_color = {116 / 255.f, 116 / 255.f, 116 / 255.f};
    Vec3 wall_color = {130 / 255.f, 130 / 255.f, 130 / 255.f};
    Vec3 door_color = {16 / 255.f, 90 / 255.f, 67 / 255.f};

    empty_room_template->static_entity_count = 9;
    // grounds
    empty_room_template->static_entities[0].pos = {0, -10.5f, -26};
    empty_room_template->static_entities[0].size = {52, 1, 104};
    empty_room_template->static_entities[0].color = ground_color;
    empty_room_template->static_entities[0].texture_index = TextureType::Ground;
    empty_room_template->static_entities[0].model_index = ModelType::CubeTest;
    // walls
    empty_room_template->static_entities[1].pos = {0, 0, 25.5f}; // up
    empty_room_template->static_entities[1].size = {50, 20, 1};
    empty_room_template->static_entities[1].color = wall_color;
    empty_room_template->static_entities[1].texture_index = TextureType::Wall;
    empty_room_template->static_entities[1].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[2].pos = {25.5f, 0, -26}; // right
    empty_room_template->static_entities[2].size = {1, 20, 102};
    empty_room_template->static_entities[2].color = wall_color;
    empty_room_template->static_entities[2].texture_index = TextureType::Wall;
    empty_room_template->static_entities[2].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[3].pos = {0, 6, -77.5f}; // down
    empty_room_template->static_entities[3].size = {50, 8, 1};
    empty_room_template->static_entities[3].color = wall_color;
    empty_room_template->static_entities[3].texture_index = TextureType::Wall;
    empty_room_template->static_entities[3].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[4].pos = {-15, -4, -77.5f};
    empty_room_template->static_entities[4].size = {20, 12, 1};
    empty_room_template->static_entities[4].color = wall_color;
    empty_room_template->static_entities[4].texture_index = TextureType::Wall;
    empty_room_template->static_entities[4].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[5].pos = {15, -4, -77.5f};
    empty_room_template->static_entities[5].size = {20, 12, 1};
    empty_room_template->static_entities[5].color = wall_color;
    empty_room_template->static_entities[5].texture_index = TextureType::Wall;
    empty_room_template->static_entities[5].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[6].pos = {-25.5f, 6, -26}; // left
    empty_room_template->static_entities[6].size = {1, 8, 102};
    empty_room_template->static_entities[6].color = wall_color;
    empty_room_template->static_entities[6].texture_index = TextureType::Wall;
    empty_room_template->static_entities[6].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[7].pos = {-25.5f, -4, -41};
    empty_room_template->static_entities[7].size = {1, 12, 72};
    empty_room_template->static_entities[7].color = wall_color;
    empty_room_template->static_entities[7].texture_index = TextureType::Wall;
    empty_room_template->static_entities[7].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[8].pos = {-25.5f, -4, 15};
    empty_room_template->static_entities[8].size = {1, 12, 20};
    empty_room_template->static_entities[8].color = wall_color;
    empty_room_template->static_entities[8].texture_index = TextureType::Wall;
    empty_room_template->static_entities[8].model_index = ModelType::CubeTest;
}

void room_template__temp_load_6(RoomTemplate* empty_room_template)
{
    Vec3 ground_color = {116 / 255.f, 116 / 255.f, 116 / 255.f};
    Vec3 wall_color = {130 / 255.f, 130 / 255.f, 130 / 255.f};
    Vec3 door_color = {16 / 255.f, 90 / 255.f, 67 / 255.f};

    empty_room_template->static_entity_count = 9;
    // grounds
    empty_room_template->static_entities[0].pos = {0, -10.5f, -26};
    empty_room_template->static_entities[0].size = {52, 1, 104};
    empty_room_template->static_entities[0].color = ground_color;
    empty_room_template->static_entities[0].texture_index = TextureType::Ground;
    empty_room_template->static_entities[0].model_index = ModelType::CubeTest;
    // walls
    empty_room_template->static_entities[1].pos = {0, 0, 25.5f}; // up
    empty_room_template->static_entities[1].size = {50, 20, 1};
    empty_room_template->static_entities[1].color = wall_color;
    empty_room_template->static_entities[1].texture_index = TextureType::Wall;
    empty_room_template->static_entities[1].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[2].pos = {25.5f, 6, -26}; // right
    empty_room_template->static_entities[2].size = {1, 8, 102};
    empty_room_template->static_entities[2].color = wall_color;
    empty_room_template->static_entities[2].texture_index = TextureType::Wall;
    empty_room_template->static_entities[2].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[3].pos = {25.5f, -4, -41};
    empty_room_template->static_entities[3].size = {1, 12, 72};
    empty_room_template->static_entities[3].color = wall_color;
    empty_room_template->static_entities[3].texture_index = TextureType::Wall;
    empty_room_template->static_entities[3].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[4].pos = {25.5f, -4, 15};
    empty_room_template->static_entities[4].size = {1, 12, 20};
    empty_room_template->static_entities[4].color = wall_color;
    empty_room_template->static_entities[4].texture_index = TextureType::Wall;
    empty_room_template->static_entities[4].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[5].pos = {0, 6, -77.5f}; // down
    empty_room_template->static_entities[5].size = {50, 8, 1};
    empty_room_template->static_entities[5].color = wall_color;
    empty_room_template->static_entities[5].texture_index = TextureType::Wall;
    empty_room_template->static_entities[5].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[6].pos = {-15, -4, -77.5f};
    empty_room_template->static_entities[6].size = {20, 12, 1};
    empty_room_template->static_entities[6].color = wall_color;
    empty_room_template->static_entities[6].texture_index = TextureType::Wall;
    empty_room_template->static_entities[6].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[7].pos = {15, -4, -77.5f};
    empty_room_template->static_entities[7].size = {20, 12, 1};
    empty_room_template->static_entities[7].color = wall_color;
    empty_room_template->static_entities[7].texture_index = TextureType::Wall;
    empty_room_template->static_entities[7].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[8].pos = {-25.5f, 0, -26}; // left
    empty_room_template->static_entities[8].size = {1, 20, 102};
    empty_room_template->static_entities[8].color = wall_color;
    empty_room_template->static_entities[8].texture_index = TextureType::Wall;
    empty_room_template->static_entities[8].model_index = ModelType::CubeTest;
}

void room_template__temp_load_7(RoomTemplate* empty_room_template)
{
    Vec3 ground_color = {116 / 255.f, 116 / 255.f, 116 / 255.f};
    Vec3 wall_color = {130 / 255.f, 130 / 255.f, 130 / 255.f};
    Vec3 door_color = {16 / 255.f, 90 / 255.f, 67 / 255.f};

    empty_room_template->static_entity_count = 11;
    // grounds
    empty_room_template->static_entities[0].pos = {0, -10.5f, 0};
    empty_room_template->static_entities[0].size = {52, 1, 52};
    empty_room_template->static_entities[0].color = ground_color;
    empty_room_template->static_entities[0].texture_index = TextureType::Ground;
    empty_room_template->static_entities[0].model_index = ModelType::CubeTest;
    // walls
    empty_room_template->static_entities[1].pos = {0, 6, 25.5f}; // up
    empty_room_template->static_entities[1].size = {50, 8, 1};
    empty_room_template->static_entities[1].color = wall_color;
    empty_room_template->static_entities[1].texture_index = TextureType::Wall;
    empty_room_template->static_entities[1].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[2].pos = {-15, -4, 25.5f};
    empty_room_template->static_entities[2].size = {20, 12, 1};
    empty_room_template->static_entities[2].color = wall_color;
    empty_room_template->static_entities[2].texture_index = TextureType::Wall;
    empty_room_template->static_entities[2].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[3].pos = {15, -4, 25.5f};
    empty_room_template->static_entities[3].size = {20, 12, 1};
    empty_room_template->static_entities[3].color = wall_color;
    empty_room_template->static_entities[3].texture_index = TextureType::Wall;
    empty_room_template->static_entities[3].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[4].pos = {25.5f, 6, 0}; // right
    empty_room_template->static_entities[4].size = {1, 8, 50};
    empty_room_template->static_entities[4].color = wall_color;
    empty_room_template->static_entities[4].texture_index = TextureType::Wall;
    empty_room_template->static_entities[4].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[5].pos = {25.5f, -4, -15};
    empty_room_template->static_entities[5].size = {1, 12, 20};
    empty_room_template->static_entities[5].color = wall_color;
    empty_room_template->static_entities[5].texture_index = TextureType::Wall;
    empty_room_template->static_entities[5].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[6].pos = {25.5f, -4, 15};
    empty_room_template->static_entities[6].size = {1, 12, 20};
    empty_room_template->static_entities[6].color = wall_color;
    empty_room_template->static_entities[6].texture_index = TextureType::Wall;
    empty_room_template->static_entities[6].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[7].pos = {0, 0, -25.5f}; // down
    empty_room_template->static_entities[7].size = {50, 20, 1};
    empty_room_template->static_entities[7].color = wall_color;
    empty_room_template->static_entities[7].texture_index = TextureType::Wall;
    empty_room_template->static_entities[7].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[8].pos = {-25.5f, 6, 0}; // left
    empty_room_template->static_entities[8].size = {1, 8, 50};
    empty_room_template->static_entities[8].color = wall_color;
    empty_room_template->static_entities[8].texture_index = TextureType::Wall;
    empty_room_template->static_entities[8].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[9].pos = {-25.5f, -4, -15};
    empty_room_template->static_entities[9].size = {1, 12, 20};
    empty_room_template->static_entities[9].color = wall_color;
    empty_room_template->static_entities[9].texture_index = TextureType::Wall;
    empty_room_template->static_entities[9].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[10].pos = {-25.5f, -4, 15};
    empty_room_template->static_entities[10].size = {1, 12, 20};
    empty_room_template->static_entities[10].color = wall_color;
    empty_room_template->static_entities[10].texture_index = TextureType::Wall;
    empty_room_template->static_entities[10].model_index = ModelType::CubeTest;
}

void room_template__temp_load_8(RoomTemplate* empty_room_template)
{
    Vec3 ground_color = {116 / 255.f, 116 / 255.f, 116 / 255.f};
    Vec3 wall_color = {130 / 255.f, 130 / 255.f, 130 / 255.f};
    Vec3 door_color = {16 / 255.f, 90 / 255.f, 67 / 255.f};

    empty_room_template->static_entity_count = 9;
    // grounds
    empty_room_template->static_entities[0].pos = {0, -10.5f, 0};
    empty_room_template->static_entities[0].size = {52, 1, 52};
    empty_room_template->static_entities[0].color = ground_color;
    empty_room_template->static_entities[0].texture_index = TextureType::Ground;
    empty_room_template->static_entities[0].model_index = ModelType::CubeTest;
    // walls
    empty_room_template->static_entities[1].pos = {0, 0, 25.5f}; // up
    empty_room_template->static_entities[1].size = {50, 20, 1};
    empty_room_template->static_entities[1].color = wall_color;
    empty_room_template->static_entities[1].texture_index = TextureType::Wall;
    empty_room_template->static_entities[1].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[2].pos = {25.5f, 0, 0}; // right
    empty_room_template->static_entities[2].size = {1, 20, 50};
    empty_room_template->static_entities[2].color = wall_color;
    empty_room_template->static_entities[2].texture_index = TextureType::Wall;
    empty_room_template->static_entities[2].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[3].pos = {0, 6, -25.5f}; // down
    empty_room_template->static_entities[3].size = {50, 8, 1};
    empty_room_template->static_entities[3].color = wall_color;
    empty_room_template->static_entities[3].texture_index = TextureType::Wall;
    empty_room_template->static_entities[3].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[4].pos = {-15, -4, -25.5f};
    empty_room_template->static_entities[4].size = {20, 12, 1};
    empty_room_template->static_entities[4].color = wall_color;
    empty_room_template->static_entities[4].texture_index = TextureType::Wall;
    empty_room_template->static_entities[4].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[5].pos = {15, -4, -25.5f};
    empty_room_template->static_entities[5].size = {20, 12, 1};
    empty_room_template->static_entities[5].color = wall_color;
    empty_room_template->static_entities[5].texture_index = TextureType::Wall;
    empty_room_template->static_entities[5].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[6].pos = {-25.5f, 6, 0}; // left
    empty_room_template->static_entities[6].size = {1, 8, 50};
    empty_room_template->static_entities[6].color = wall_color;
    empty_room_template->static_entities[6].texture_index = TextureType::Wall;
    empty_room_template->static_entities[6].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[7].pos = {-25.5f, -4, -15};
    empty_room_template->static_entities[7].size = {1, 12, 20};
    empty_room_template->static_entities[7].color = wall_color;
    empty_room_template->static_entities[7].texture_index = TextureType::Wall;
    empty_room_template->static_entities[7].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[8].pos = {-25.5f, -4, 15};
    empty_room_template->static_entities[8].size = {1, 12, 20};
    empty_room_template->static_entities[8].color = wall_color;
    empty_room_template->static_entities[8].texture_index = TextureType::Wall;
    empty_room_template->static_entities[8].model_index = ModelType::CubeTest;
}

void room_template__temp_load_9(RoomTemplate* empty_room_template)
{
    Vec3 ground_color = {116 / 255.f, 116 / 255.f, 116 / 255.f};
    Vec3 wall_color = {130 / 255.f, 130 / 255.f, 130 / 255.f};
    Vec3 door_color = {16 / 255.f, 90 / 255.f, 67 / 255.f};

    empty_room_template->static_entity_count = 11;
    // grounds
    empty_room_template->static_entities[0].pos = {26, -10.5f, 0};
    empty_room_template->static_entities[0].size = {104, 1, 52};
    empty_room_template->static_entities[0].color = ground_color;
    empty_room_template->static_entities[0].texture_index = TextureType::Ground;
    empty_room_template->static_entities[0].model_index = ModelType::CubeTest;
    // walls
    empty_room_template->static_entities[1].pos = {26, 6, 25.5f}; // up
    empty_room_template->static_entities[1].size = {102, 8, 1};
    empty_room_template->static_entities[1].color = wall_color;
    empty_room_template->static_entities[1].texture_index = TextureType::Wall;
    empty_room_template->static_entities[1].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[2].pos = {-15, -4, 25.5f};
    empty_room_template->static_entities[2].size = {20, 12, 1};
    empty_room_template->static_entities[2].color = wall_color;
    empty_room_template->static_entities[2].texture_index = TextureType::Wall;
    empty_room_template->static_entities[2].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[3].pos = {41, -4, 25.5f};
    empty_room_template->static_entities[3].size = {72, 12, 1};
    empty_room_template->static_entities[3].color = wall_color;
    empty_room_template->static_entities[3].texture_index = TextureType::Wall;
    empty_room_template->static_entities[3].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[4].pos = {77.5f, 6, 0}; // right
    empty_room_template->static_entities[4].size = {1, 8, 50};
    empty_room_template->static_entities[4].color = wall_color;
    empty_room_template->static_entities[4].texture_index = TextureType::Wall;
    empty_room_template->static_entities[4].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[5].pos = {77.5f, -4, -15};
    empty_room_template->static_entities[5].size = {1, 12, 20};
    empty_room_template->static_entities[5].color = wall_color;
    empty_room_template->static_entities[5].texture_index = TextureType::Wall;
    empty_room_template->static_entities[5].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[6].pos = {77.5f, -4, 15};
    empty_room_template->static_entities[6].size = {1, 12, 20};
    empty_room_template->static_entities[6].color = wall_color;
    empty_room_template->static_entities[6].texture_index = TextureType::Wall;
    empty_room_template->static_entities[6].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[7].pos = {26, 6, -25.5f}; // down
    empty_room_template->static_entities[7].size = {102, 8, 1};
    empty_room_template->static_entities[7].color = wall_color;
    empty_room_template->static_entities[7].texture_index = TextureType::Wall;
    empty_room_template->static_entities[7].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[8].pos = {-15, -4, -25.5f};
    empty_room_template->static_entities[8].size = {20, 12, 1};
    empty_room_template->static_entities[8].color = wall_color;
    empty_room_template->static_entities[8].texture_index = TextureType::Wall;
    empty_room_template->static_entities[8].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[9].pos = {41, -4, -25.5f};
    empty_room_template->static_entities[9].size = {72, 12, 1};
    empty_room_template->static_entities[9].color = wall_color;
    empty_room_template->static_entities[9].texture_index = TextureType::Wall;
    empty_room_template->static_entities[9].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[10].pos = {-25.5f, 0, 0}; // left
    empty_room_template->static_entities[10].size = {1, 20, 50};
    empty_room_template->static_entities[10].color = wall_color;
    empty_room_template->static_entities[10].texture_index = TextureType::Wall;
    empty_room_template->static_entities[10].model_index = ModelType::CubeTest;
}

void room_template__temp_load_10(RoomTemplate* empty_room_template)
{
    Vec3 ground_color = {116 / 255.f, 116 / 255.f, 116 / 255.f};
    Vec3 wall_color = {130 / 255.f, 130 / 255.f, 130 / 255.f};
    Vec3 door_color = {16 / 255.f, 90 / 255.f, 67 / 255.f};

    empty_room_template->static_entity_count = 9;
    // grounds
    empty_room_template->static_entities[0].pos = {0, -10.5f, 0};
    empty_room_template->static_entities[0].size = {52, 1, 52};
    empty_room_template->static_entities[0].color = ground_color;
    empty_room_template->static_entities[0].texture_index = TextureType::Ground;
    // walls
    empty_room_template->static_entities[1].pos = {0, 6, 25.5f}; // up
    empty_room_template->static_entities[1].size = {50, 8, 1};
    empty_room_template->static_entities[1].color = wall_color;
    empty_room_template->static_entities[1].texture_index = TextureType::Wall;
    empty_room_template->static_entities[1].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[2].pos = {-15, -4, 25.5f};
    empty_room_template->static_entities[2].size = {20, 12, 1};
    empty_room_template->static_entities[2].color = wall_color;
    empty_room_template->static_entities[2].texture_index = TextureType::Wall;
    empty_room_template->static_entities[2].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[3].pos = {15, -4, 25.5f};
    empty_room_template->static_entities[3].size = {20, 12, 1};
    empty_room_template->static_entities[3].color = wall_color;
    empty_room_template->static_entities[3].texture_index = TextureType::Wall;
    empty_room_template->static_entities[3].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[4].pos = {25.5f, 0, 0}; // right
    empty_room_template->static_entities[4].size = {1, 20, 50};
    empty_room_template->static_entities[4].color = wall_color;
    empty_room_template->static_entities[4].texture_index = TextureType::Wall;
    empty_room_template->static_entities[4].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[5].pos = {0, 0, -25.5f}; // down
    empty_room_template->static_entities[5].size = {50, 20, 1};
    empty_room_template->static_entities[5].color = wall_color;
    empty_room_template->static_entities[5].texture_index = TextureType::Wall;
    empty_room_template->static_entities[5].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[6].pos = {-25.5f, 6, 0}; // left
    empty_room_template->static_entities[6].size = {1, 8, 50};
    empty_room_template->static_entities[6].color = wall_color;
    empty_room_template->static_entities[6].texture_index = TextureType::Wall;
    empty_room_template->static_entities[6].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[7].pos = {-25.5f, -4, -15};
    empty_room_template->static_entities[7].size = {1, 12, 20};
    empty_room_template->static_entities[7].color = wall_color;
    empty_room_template->static_entities[7].texture_index = TextureType::Wall;
    empty_room_template->static_entities[7].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[8].pos = {-25.5f, -4, 15};
    empty_room_template->static_entities[8].size = {1, 12, 20};
    empty_room_template->static_entities[8].color = wall_color;
    empty_room_template->static_entities[8].texture_index = TextureType::Wall;
    empty_room_template->static_entities[8].model_index = ModelType::CubeTest;
}

void room_template__temp_load_11(RoomTemplate* empty_room_template)
{
    Vec3 ground_color = {116 / 255.f, 116 / 255.f, 116 / 255.f};
    Vec3 wall_color = {130 / 255.f, 130 / 255.f, 130 / 255.f};
    Vec3 door_color = {16 / 255.f, 90 / 255.f, 67 / 255.f};

    empty_room_template->static_entity_count = 9;
    // grounds
    empty_room_template->static_entities[0].pos = {0, -10.5f, 0};
    empty_room_template->static_entities[0].size = {52, 1, 52};
    empty_room_template->static_entities[0].color = ground_color;
    empty_room_template->static_entities[0].texture_index = TextureType::Ground;
    // walls
    empty_room_template->static_entities[1].pos = {0, 6, 25.5f}; // up
    empty_room_template->static_entities[1].size = {50, 8, 1};
    empty_room_template->static_entities[1].color = wall_color;
    empty_room_template->static_entities[1].texture_index = TextureType::Wall;
    empty_room_template->static_entities[1].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[2].pos = {-15, -4, 25.5f};
    empty_room_template->static_entities[2].size = {20, 12, 1};
    empty_room_template->static_entities[2].color = wall_color;
    empty_room_template->static_entities[2].texture_index = TextureType::Wall;
    empty_room_template->static_entities[2].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[3].pos = {15, -4, 25.5f};
    empty_room_template->static_entities[3].size = {20, 12, 1};
    empty_room_template->static_entities[3].color = wall_color;
    empty_room_template->static_entities[3].texture_index = TextureType::Wall;
    empty_room_template->static_entities[3].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[4].pos = {25.5f, 6, 0}; // right
    empty_room_template->static_entities[4].size = {1, 8, 50};
    empty_room_template->static_entities[4].color = wall_color;
    empty_room_template->static_entities[4].texture_index = TextureType::Wall;
    empty_room_template->static_entities[4].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[5].pos = {25.5f, -4, -15};
    empty_room_template->static_entities[5].size = {1, 12, 20};
    empty_room_template->static_entities[5].color = wall_color;
    empty_room_template->static_entities[5].texture_index = TextureType::Wall;
    empty_room_template->static_entities[5].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[6].pos = {25.5f, -4, 15};
    empty_room_template->static_entities[6].size = {1, 12, 20};
    empty_room_template->static_entities[6].color = wall_color;
    empty_room_template->static_entities[6].texture_index = TextureType::Wall;
    empty_room_template->static_entities[6].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[7].pos = {0, 0, -25.5f}; // down
    empty_room_template->static_entities[7].size = {50, 20, 1};
    empty_room_template->static_entities[7].color = wall_color;
    empty_room_template->static_entities[7].texture_index = TextureType::Wall;
    empty_room_template->static_entities[7].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[8].pos = {-25.5f, 0, 0}; // left
    empty_room_template->static_entities[8].size = {1, 20, 50};
    empty_room_template->static_entities[8].color = wall_color;
    empty_room_template->static_entities[8].texture_index = TextureType::Wall;
    empty_room_template->static_entities[8].model_index = ModelType::CubeTest;
}

void room_template__temp_load_12(RoomTemplate* empty_room_template)
{
    Vec3 ground_color = {116 / 255.f, 116 / 255.f, 116 / 255.f};
    Vec3 wall_color = {130 / 255.f, 130 / 255.f, 130 / 255.f};
    Vec3 door_color = {16 / 255.f, 90 / 255.f, 67 / 255.f};

    empty_room_template->static_entity_count = 9;
    // grounds
    empty_room_template->static_entities[0].pos = {26, -10.5f, 0};
    empty_room_template->static_entities[0].size = {104, 1, 52};
    empty_room_template->static_entities[0].color = ground_color;
    empty_room_template->static_entities[0].texture_index = TextureType::Ground;
    empty_room_template->static_entities[0].model_index = ModelType::CubeTest;
    // walls
    empty_room_template->static_entities[1].pos = {26, 6, 25.5f}; // up
    empty_room_template->static_entities[1].size = {102, 8, 1};
    empty_room_template->static_entities[1].color = wall_color;
    empty_room_template->static_entities[1].texture_index = TextureType::Wall;
    empty_room_template->static_entities[1].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[2].pos = {11, -4, 25.5f};
    empty_room_template->static_entities[2].size = {72, 12, 1};
    empty_room_template->static_entities[2].color = wall_color;
    empty_room_template->static_entities[2].texture_index = TextureType::Wall;
    empty_room_template->static_entities[2].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[3].pos = {67, -4, 25.5f};
    empty_room_template->static_entities[3].size = {20, 12, 1};
    empty_room_template->static_entities[3].color = wall_color;
    empty_room_template->static_entities[3].texture_index = TextureType::Wall;
    empty_room_template->static_entities[3].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[4].pos = {77.5f, 0, 0}; // right
    empty_room_template->static_entities[4].size = {1, 20, 50};
    empty_room_template->static_entities[4].color = wall_color;
    empty_room_template->static_entities[4].texture_index = TextureType::Wall;
    empty_room_template->static_entities[4].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[5].pos = {26, 0, -25.5f}; // down
    empty_room_template->static_entities[5].size = {102, 20, 1};
    empty_room_template->static_entities[5].color = wall_color;
    empty_room_template->static_entities[5].texture_index = TextureType::Wall;
    empty_room_template->static_entities[5].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[6].pos = {-25.5f, 6, 0}; // left
    empty_room_template->static_entities[6].size = {1, 8, 50};
    empty_room_template->static_entities[6].color = wall_color;
    empty_room_template->static_entities[6].texture_index = TextureType::Wall;
    empty_room_template->static_entities[6].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[7].pos = {-25.5f, -4, -15};
    empty_room_template->static_entities[7].size = {1, 12, 20};
    empty_room_template->static_entities[7].color = wall_color;
    empty_room_template->static_entities[7].texture_index = TextureType::Wall;
    empty_room_template->static_entities[7].model_index = ModelType::CubeTest;
    empty_room_template->static_entities[8].pos = {-25.5f, -4, 15};
    empty_room_template->static_entities[8].size = {1, 12, 20};
    empty_room_template->static_entities[8].color = wall_color;
    empty_room_template->static_entities[8].texture_index = TextureType::Wall;
    empty_room_template->static_entities[8].model_index = ModelType::CubeTest;
}

void room_template_calculate_size(RoomTemplate* room_template)
{
    Vec3 room_min = {0};
    Vec3 room_max = {0};

    // grounds
    for (int i = 0; i < room_template->static_entity_count; ++i)
    {
        room_min.x = fminf(room_min.x,
                           room_template->static_entities[i].pos.x -
                               room_template->static_entities[i].size.x * 0.5f);
        room_min.y = fminf(room_min.y,
                           room_template->static_entities[i].pos.y -
                               room_template->static_entities[i].size.y * 0.5f);
        room_min.z = fminf(room_min.z,
                           room_template->static_entities[i].pos.z -
                               room_template->static_entities[i].size.z * 0.5f);

        room_max.x = fmaxf(room_max.x,
                           room_template->static_entities[i].pos.x +
                               room_template->static_entities[i].size.x * 0.5f);
        room_max.y = fmaxf(room_max.y,
                           room_template->static_entities[i].pos.y +
                               room_template->static_entities[i].size.y * 0.5f);
        room_max.z = fmaxf(room_max.z,
                           room_template->static_entities[i].pos.z +
                               room_template->static_entities[i].size.z * 0.5f);
    }

    room_template->room_size = room_max - room_min;
    room_template->room_pos = (room_min + room_max) * 0.5f;
}

void room_template_init_dummy(
    enum_array<RoomTemplateType, RoomTemplate>& room_templates)
{
    room_template__temp_load_1(&room_templates[RoomTemplateType::Num1]);
    room_template_calculate_size(&room_templates[RoomTemplateType::Num1]);
    room_template__temp_load_2(&room_templates[RoomTemplateType::Num2]);
    room_template_calculate_size(&room_templates[RoomTemplateType::Num2]);
    room_template__temp_load_3(&room_templates[RoomTemplateType::Num3]);
    room_template_calculate_size(&room_templates[RoomTemplateType::Num3]);
    room_template__temp_load_4(&room_templates[RoomTemplateType::Num4]);
    room_template_calculate_size(&room_templates[RoomTemplateType::Num4]);
    room_template__temp_load_5(&room_templates[RoomTemplateType::Num5]);
    room_template_calculate_size(&room_templates[RoomTemplateType::Num5]);
    room_template__temp_load_6(&room_templates[RoomTemplateType::Num6]);
    room_template_calculate_size(&room_templates[RoomTemplateType::Num6]);
    room_template__temp_load_7(&room_templates[RoomTemplateType::Num7]);
    room_template_calculate_size(&room_templates[RoomTemplateType::Num7]);
    room_template__temp_load_8(&room_templates[RoomTemplateType::Num8]);
    room_template_calculate_size(&room_templates[RoomTemplateType::Num8]);
    room_template__temp_load_9(&room_templates[RoomTemplateType::Num9]);
    room_template_calculate_size(&room_templates[RoomTemplateType::Num9]);
    room_template__temp_load_10(&room_templates[RoomTemplateType::Num10]);
    room_template_calculate_size(&room_templates[RoomTemplateType::Num10]);
    room_template__temp_load_11(&room_templates[RoomTemplateType::Num11]);
    room_template_calculate_size(&room_templates[RoomTemplateType::Num11]);
    room_template__temp_load_12(&room_templates[RoomTemplateType::Num12]);
    room_template_calculate_size(&room_templates[RoomTemplateType::Num12]);
}
}
