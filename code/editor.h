﻿#ifndef DSC_EDITOR_H
#define DSC_EDITOR_H

#include "mathematics.h"
#include "common.h"
#include "entity.h"
#include "shared.h"

#define max_editor_history_count 5

namespace dsc
{
struct Input;
struct Camera;
struct Room;
struct RoomTemplate;
enum class RoomTemplateType;

enum class EditorMode
{
    Editing,
    Floating
};

enum class EditorActionType
{
    New,
    Delete,
};

enum class EditorEntityType
{
    Static
};

struct EditorAction
{
    EditorActionType action_type;
    union
    {
        // new
        struct
        {
            EditorEntityType new_entity_type;
            u16 entity_index; // this needs to be updated everytime history
                              // alters.
        };
        // delete
        struct
        {
            EditorEntityType deleted_entity_type;
            Entity entity_backup;
        };
    };
};

struct Editor
{
    EditorMode editor_mode;

    // Editor stuffs
    int editor_selected_entity_index;
    EditorEntityType editor_selected_entity_type;
    RoomTemplate* editor_selected_room_template;
    Vec3 editor_previous_selected_entity_color; // temporary material change.

    // Editor history
    EditorAction editor_history[max_editor_history_count];
    int editor_history_head, editor_history_tail, editor_history_current,
        editor_history_count;

    void set_mode(Input* input, EditorMode mode);
    void update(Camera* camera,
                Room* rooms,
                int room_count,
                Input* input,
                const WindowSize window_size,
                float dt);
    void redo();
    void undo();
    void remove(); // renamed from delete because delete is a reserved keyword
    void deselect();
    void action_push(EditorAction editor_action);
};

// void editor_save_all_room_templates(GameScene* game);
void room_template_save(RoomTemplate* room_template,
                        void** empty_binary_file,
                        u64* empty_binary_file_size);
// void editor_load_all_room_templates(GameScene* game);
void room_template_load(
    RoomTemplate* empty_room_template,
    EntityVariableClarifier* loaded_entity_variable_clarifiers,
    int loaded_entity_variable_clarifiers_count,
    void** raw_binary_file);
void room_template_calculate_size(RoomTemplate* room_template);
void room_template_init_dummy(
    enum_array<RoomTemplateType, RoomTemplate>& room_templates);
}

#endif // DSC_EDITOR_H
