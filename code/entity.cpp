﻿#include "entity.h"
#include "debug.h"

namespace dsc
{
void Entity::load(EntityVariableClarifier loaded, void** raw_binary_file)
{
    switch (loaded)
    {
    case EntityVariableClarifier::Pos:
        pos = *(Vec3*)(*raw_binary_file);
        *raw_binary_file = (Vec3*)(*raw_binary_file) + 1;
        break;

    case EntityVariableClarifier::Rot:
        rot = *(Vec3*)(*raw_binary_file);
        *raw_binary_file = (Vec3*)(*raw_binary_file) + 1;
        break;

    case EntityVariableClarifier::Size:
        size = *(Vec3*)(*raw_binary_file);
        *raw_binary_file = (Vec3*)(*raw_binary_file) + 1;
        break;

    case EntityVariableClarifier::Color:
        color = *(Vec3*)(*raw_binary_file);
        *raw_binary_file = (Vec3*)(*raw_binary_file) + 1;
        break;

    case EntityVariableClarifier::ModelIndex:
        model_index = *(ModelType*)(*raw_binary_file);
        *raw_binary_file = (ModelType*)(*raw_binary_file) + 1;
        break;

    case EntityVariableClarifier::TextureIndex:
        texture_index = *(TextureType*)(*raw_binary_file);
        *raw_binary_file = (TextureType*)(*raw_binary_file) + 1;
        break;

    default: assert_(false); break;
    }
}
}
