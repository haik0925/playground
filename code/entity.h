﻿#ifndef DSC_ENTITY_H
#define DSC_ENTITY_H

#include "mathematics.h"
#include "asset.h"

namespace dsc
{
enum class EntityVariableClarifier
{
    // DO NOT change the order of this. ONLY ADD on to last enum, no
    // removing or changing order.
    Pos = 0,
    Rot,
    Size,
    Color,
    ModelIndex,
    TextureIndex,
    Count
};

struct Entity
{
    // Warning! if anything changes inside the struct Entity, you should
    // change everything below!
    Vec3 pos;
    Vec3 rot;
    Vec3 size;
    Vec3 color;
    ModelType model_index;
    TextureType texture_index;

    void load(EntityVariableClarifier loaded, void** raw_binary_file);
};

constexpr int entity_variables_count = 6;

// void clarify_entity_variables(GameScene* game);

}

#endif // DSC_ENTITY_H
