﻿#include "game_scene.h"
#include "memory.h"
#include "debug.h"
#include "common.h"
#include "gui.h"
#include "stb_easy_font.h"
#include "physics.h"

namespace dsc
{
Entities::Entities(ArenaAllocator* memory_arena, int capacity)
    : allocator(sizeof(Entity),
                capacity,
                memory_arena->push_array<Entity>(capacity))
{
}

Entity* Entities::allocate()
{
    Entity* entity = allocator.alloc<Entity>();
    return entity;
}

void Entities::free(Entity* entity)
{
    allocator.free(entity);
}

internal_func void game_scene__update_light_movement(GameScene* game, float dt);
internal_func void rooms_and_doors__create(
    Room* rooms,
    Door* doors,
    enum_array<RoomTemplateType, RoomTemplate>& room_templates);
internal_func Box box__create(float x, float z, float size);

void game_scene_init(GameScene* game,
                     Input* input,
                     ArenaAllocator* persistent_memory_arena);
void game_scene_update(GameScene* game,
                       Input* input,
                       AudioContext* audio,
                       AssetStorage* asset_storage,
                       float dt);
void game_scene_state_init(GameScene* game, Input* input, GameSceneState state);
void game_scene_state_shutdown_current_state(GameScene* game);
void game_scene_state_change(GameScene* game,
                             Input* input,
                             GameSceneState state);
void game_scene_deinit(GameScene* game);

void game_scene_init(GameScene* game, Input* input, ArenaAllocator* memory_arena)
{
    game->recoil.pos = {};
    game->recoil.vel = {};
    game->recoil.strength = 0.f;
    game->camera_rumble = 0;

    game_scene__update_light_movement(game, 0);

    game->player_can_jump = false;

    // game->entities = entities_new(memory_arena, 200);

    // generate room templates
    room_template_init_dummy(game->room_templates);

    // create rooms and doors
    rooms_and_doors__create(game->rooms, game->doors, game->room_templates);
    game->selected_door = NULL;

    game->key_pos = game->rooms[9].pos;
    game->key_pos.y = -8;
    game->key_t = 0;

    // generate new bullets
    for (int i = 0; i < max_bullet_count; ++i)
    {
        game->bullets[i].is_in_use = false;
        game->bullets[i].entity = game->entities.allocate();
        game->bullets[i].entity->color = {1, 0, 0};
        game->bullets[i].entity->size = {0.2f, 0.2f, 0.2f};
    }
    game->bullet_count = 0;
    game->bullet_current_cooltime = 0;
    game->ammo = max_ammo_count;

    // generate new eneimes
    for (int i = 0; i < max_enemy_count; ++i)
    {
        game->enemies[i].is_in_use = false;
        game->enemies[i].entity = game->entities.allocate();
        game->enemies[i].entity->color = {.2f, .2f, 1};
    }
    game->enemy_count = 0;

    game->editor = memory_arena->push<Editor>();

    game_scene_state_init(game, input, GameSceneState::Game);

    // editor init
    game->editor->editor_selected_entity_index = -1;

    game->editor->editor_history_head = 0;
    game->editor->editor_history_tail = 0;
    game->editor->editor_history_current = 0;
    game->editor->editor_history_count = 0;
}

void game_scene_update(GameScene* game,
                       Input* input,
                       AudioContext* audio,
                       AssetStorage* asset_storage,
                       float dt)
{
    Camera* camera = &game->camera;
    game->camera_rumble = 0;

    // Update first person camera
    camera->look_first_person(input, 5.f, dt);

    // Update velocity for player and enemies
    {
        const float max_speed = 20;
        const float acceleration = 250;
        const float deceleration = 200;
        const float deceleration_epsilon = 0.1f;
        const float jump_velocity = 25;
        const float gravity = 90;

        // Player
        {
            Vec2 forward =
                camera->get_look(game->recoil.pos.x, game->recoil.pos.y).xz();
            forward = forward.normalize();
            Vec2 right{-forward.y, forward.x};

            Vec2 input_acceleration{};
            bool input_horizontal = false;
            bool input_vertical = false;

            // gravity
            camera->vel.y -= gravity * dt;
            if (input->button_move_jump.is_pressed() && game->player_can_jump)
            {
                camera->vel.y = jump_velocity;
                game->player_can_jump = false;
            }

            // acceleration
            if (input->button_move_left.is_down)
                input_acceleration = input_acceleration - right;
            else if (input->button_move_right.is_down)
                input_acceleration = input_acceleration + right;
            else
                input_horizontal = true;
            if (input->button_move_back.is_down)
                input_acceleration = input_acceleration - forward;
            else if (input->button_move_forward.is_down)
                input_acceleration = input_acceleration + forward;
            else
                input_vertical = true;

            Vec2 vel = input_acceleration.normalize() * acceleration * dt +
                       camera->vel.xz();

            // deceleration
            if (input_horizontal)
            {
                float cos = vel.normalize().dot(right);
                if (cos > deceleration_epsilon)
                {
                    Vec2 new_vel = vel - right * (deceleration * dt);
                    if (new_vel.normalize().dot(right) < deceleration_epsilon)
                        new_vel = vel - right * cos * vel.length();
                    vel = new_vel;
                }
                else if (cos < -deceleration_epsilon)
                {
                    Vec2 new_vel = vel + right * deceleration * dt;
                    if (new_vel.normalize().dot(right) > -deceleration_epsilon)
                        new_vel = vel - right * cos * vel.length();
                    vel = new_vel;
                }
                else
                    vel = vel - right * cos * vel.length();
            }
            if (input_vertical)
            {
                float cos = vel.normalize().dot(forward);
                if (cos > deceleration_epsilon)
                {
                    Vec2 new_vel = vel - forward * deceleration * dt;
                    if (new_vel.normalize().dot(forward) < deceleration_epsilon)
                        new_vel = vel - forward * cos * vel.length();
                    vel = new_vel;
                }
                else if (cos < -deceleration_epsilon)
                {
                    Vec2 new_vel = vel + forward * deceleration * dt;
                    if (new_vel.normalize().dot(forward) >
                        -deceleration_epsilon)
                        new_vel = vel - forward * cos * vel.length();
                    vel = new_vel;
                }
                else
                    vel = vel - forward * cos * vel.length();
            }

            // speed limit
            float current_speed_sq = vel.lengthsq();
            if (current_speed_sq > max_speed * max_speed)
                vel = vel.normalize() * max_speed;

            // apply to velocity
            camera->vel.x = vel.x;
            camera->vel.z = vel.y;

            // apply velocity to position
            camera->pos += camera->vel * dt;
        }

        // Enemies
        {
            const int enemy_life = 5;
            const float enemy_follow_speed = 12;
            const float enemy_stop_range_sq = 49;

            // generate new enemy
            if (game->enemy_count < max_enemy_count &&
                input->button_right_click.is_pressed())
            {
                ++game->enemy_count;

                for (int i = 0; i < max_enemy_count; ++i)
                    if (!game->enemies[i].is_in_use)
                    {
                        game->enemies[i].is_in_use = true;
                        game->enemies[i].life = enemy_life;
                        game->enemies[i].entity->pos =
                            camera->pos + Vec3{0, 2, 0};
                        game->enemies[i].entity->size = {2, 4, 2};
                        break;
                    }
            }

            for (int i = 0; i < max_enemy_count; ++i)
                if (game->enemies[i].is_in_use)
                {
                    Vec2 diff =
                        (camera->pos - game->enemies[i].entity->pos).xz();

                    // simply follow player if out of range
                    if (diff.lengthsq() > enemy_stop_range_sq)
                    {
                        Vec2 applying_vel =
                            diff.normalize() * enemy_follow_speed;
                        game->enemies[i].vel.x = applying_vel.x;
                        game->enemies[i].vel.z = applying_vel.y;
                    }
                    else
                    {
                        game->enemies[i].vel.x = 0;
                        game->enemies[i].vel.z = 0;
                    }

                    // gravity
                    game->enemies[i].vel.y -= gravity * dt;

                    // apply velocity to position
                    game->enemies[i].entity->pos += game->enemies[i].vel * dt;
                }
        }
    }

    // Update Bullet
    {
        const float bullet_cooltime = 0.1f;
        const float bullet_lifetime = 5.f;
        const float bullet_speed = 60;
        const float bullet_spawn_position_offset = 2.5f;
        const float recoil_amplifier = 1.3f;
        const float recoil_y_offset = 5;
        game->bullet_current_cooltime -= dt;

        // generate new bullet
        if (input->button_left_click.is_down &&
            game->bullet_count < max_bullet_count && game->ammo &&
            game->bullet_current_cooltime <= 0)
        {
            game->bullet_current_cooltime = bullet_cooltime;
            ++game->bullet_count;
            --game->ammo;

            float random_angle_radian =
                to_radians(rand() % 10000 / 10000.f * 360.f);
            Vec2 additional_recoil_vel = {cosf(random_angle_radian),
                                          sinf(random_angle_radian) +
                                              recoil_y_offset};
            additional_recoil_vel =
                additional_recoil_vel.normalize() * game->recoil.strength;
            game->recoil.vel = game->recoil.vel + additional_recoil_vel;
            game->recoil.strength =
                fminf(game->recoil.strength * recoil_amplifier,
                      highest_recoil_strength);

            // this is inefficient, but works. Should change later.
            for (int i = 0; i < max_bullet_count; ++i)
                if (!game->bullets[i].is_in_use)
                {
                    game->bullets[i].is_in_use = true;
                    game->bullets[i].vel =
                        camera->get_look(game->recoil.pos.x,
                                         game->recoil.pos.y) *
                        bullet_speed;
                    game->bullets[i].lifetime = bullet_lifetime;
                    game->bullets[i].entity->pos =
                        camera->pos +
                        game->bullets[i].vel *
                            (bullet_spawn_position_offset / bullet_speed);
                    break;
                }
        }

        // update bullet positions
        for (int i = 0; i < max_bullet_count; ++i)
            if (game->bullets[i].is_in_use)
            {
                game->bullets[i].lifetime -= dt;
                // remove if lifetime is done
                if (game->bullets[i].lifetime <= 0)
                {
                    --game->bullet_count;
                    game->bullets[i].is_in_use = false;
                    continue;
                }

                game->bullets[i].entity->pos += game->bullets[i].vel * dt;
            }
    }

    // Update Recoil
    {
        const float recoil_break_speed = 5000;
        const float recoil_recover_speed = 5;
        const float recoil_recover_strength_speed = 5;
        const float recoil_recover_epsilon_sq = 0.1f;

        // velocity going on
        if (game->recoil.vel.lengthsq() > recoil_recover_epsilon_sq)
        {
            Vec2 recoil_recover_dir = -game->recoil.vel.normalize();
            game->recoil.vel += recoil_recover_dir * recoil_break_speed * dt;
            if (game->recoil.vel.normalize().dot(recoil_recover_dir) > 0)
                game->recoil.vel = {};
            game->recoil.pos += game->recoil.vel * dt;
        }
        // recovering from recoil
        else
        {
            game->recoil.pos =
                game->recoil.pos - game->recoil.pos * recoil_recover_speed * dt;

            game->recoil.strength +=
                (lowest_recoil_strength - game->recoil.strength) *
                recoil_recover_strength_speed * dt;
            game->recoil.strength =
                fmaxf(game->recoil.strength, lowest_recoil_strength);
        }
    }

    // Update Reload
    {
        if (input->button_reload.is_pressed())
            game->ammo = max_ammo_count;
    }

    // Update Door interactions
    {
        const float door_height = 7;
        const float door_opening_vel = 2.6f;
        const float camera_rumble = .02f;
        const float camera_rumble_max_sqrlength = 700.f;

        // Open door if interacted
        if (game->selected_door && input->button_interact.is_released())
        {
            game->selected_door->is_opened = true;
            game->rooms[game->selected_door->open_room[0]].is_opened = true;
            game->rooms[game->selected_door->open_room[1]].is_opened = true;
            audio->play_sound(&asset_storage->_sfxs[SfxType::DoorOpen]);
        }

        // Completely open the door
        for (int door_index = 0; door_index < max_door_count; ++door_index)
        {
            Door* door = &game->doors[door_index];
            if (door->is_opened && door->entity.pos.y < door_height)
            {
                float dist_multiplier =
                    1 - (door->entity.pos - game->camera.pos).lengthsq() /
                            camera_rumble_max_sqrlength;
                if (dist_multiplier < 0)
                    dist_multiplier = 0;

                door->entity.pos.y += door_opening_vel * dt;
                game->camera_rumble += camera_rumble * dist_multiplier;
            }
        }
    }

    game_scene__update_light_movement(game, dt);
}

internal_func void game_scene__update_light_movement(GameScene* game, float dt)
{
    game->light_angle += 45 * dt;
    if (game->light_angle > 360.f)
        game->light_angle -= 360.f;
    game->light_pos = {2 * cosf(to_radians(game->light_angle)), 2,
                       2 * sinf(to_radians(game->light_angle))};
}

void game_scene_deinit(GameScene* game)
{
    // game__room_template_save_all(game);
}

// value initialization (should be done every time a state loads)
// memory reservation (should be done only once)
// no shared data - instead there should be data passed between state transition

void game_scene_state_init(GameScene* game, Input* input, GameSceneState state)
{
    // Init
    switch (state)
    {
    case GameSceneState::Game:
        input->show_cursor = false;
        input->lock_mouse_to_center = true;
        break;
    case GameSceneState::Editor:
        game->editor->set_mode(input, EditorMode::Editing);
        break;
    default: assert_(false);
    }

    game->state = state;
}

void game_scene_state_shutdown_current_state(GameScene* game)
{
    // Shutdown
    switch (game->state)
    {
    case GameSceneState::Game: break;
    case GameSceneState::Editor:
        if (game->editor->editor_selected_entity_index != -1)
            game->editor->deselect();
        game->editor->editor_selected_entity_index = -1;
        break;
    default: assert_(false);
    }
}

void game_scene_state_change(GameScene* game,
                             Input* input,
                             GameSceneState state)
{
    game_scene_state_shutdown_current_state(game);
    game_scene_state_init(game, input, state);
}

void rooms_and_doors__create(
    Room* rooms,
    Door* doors,
    enum_array<RoomTemplateType, RoomTemplate>& room_templates)
{
    const float room_scale = 52;

    auto create_room = [](Vec3 pos, RoomTemplate* room_template) {
        Room result = {};
        result.pos = pos;
        result.room_template = room_template;
        return result;
    };

    // generate rooms
    rooms[0] = create_room({-room_scale * 2, 0, room_scale * 3},
                           &room_templates[RoomTemplateType::Num1]);
    rooms[1] = create_room({-room_scale, 0, room_scale * 3},
                           &room_templates[RoomTemplateType::Num2]);
    rooms[2] = create_room({room_scale, 0, room_scale * 3},
                           &room_templates[RoomTemplateType::Num3]);
    rooms[3] = create_room({-room_scale, 0, room_scale * 2},
                           &room_templates[RoomTemplateType::Num4]);
    rooms[4] = create_room({room_scale * 2, 0, room_scale * 2},
                           &room_templates[RoomTemplateType::Num5]);
    rooms[5] = create_room({-room_scale * 2, 0, room_scale},
                           &room_templates[RoomTemplateType::Num6]);
    rooms[6] = create_room({-room_scale, 0, room_scale},
                           &room_templates[RoomTemplateType::Num7]);
    rooms[7] = create_room({0, 0, room_scale},
                           &room_templates[RoomTemplateType::Num8]);
    rooms[8] = create_room({}, &room_templates[RoomTemplateType::Num9]);
    rooms[9] = create_room({room_scale * 2, 0, 0},
                           &room_templates[RoomTemplateType::Num10]);
    rooms[10] = create_room({-room_scale * 2, 0, -room_scale},
                            &room_templates[RoomTemplateType::Num11]);
    rooms[11] = create_room({-room_scale, 0, -room_scale},
                            &room_templates[RoomTemplateType::Num12]);

    // plot boxes
    rooms[8].box_count = 2;
    rooms[8].boxes[0] = box__create(13, 2, 4.5f);
    rooms[8].boxes[1] = box__create(17, -4, 6);

    auto door_init = [](Door* door, Vec3 pos, Vec3 size, int open_room1,
                        int open_room2) {
        *door = {};
        door->entity.pos = pos;
        door->entity.size = size;
        door->entity.color = {1, 1, 1};
        door->open_room[0] = open_room1;
        door->open_room[1] = open_room2;
    };

    door_init(&doors[0], {-room_scale * 1.5f, -4, room_scale * 3}, {1, 12, 10},
              0, 1);
    door_init(&doors[1], {room_scale * 0.5f, -4, room_scale * 3}, {1, 12, 10},
              1, 2);
    door_init(&doors[2], {-room_scale, -4, room_scale * 2.5f}, {10, 12, 1}, 1,
              3);
    door_init(&doors[3], {room_scale * 1.5f, -4, room_scale * 2}, {1, 12, 10},
              2, 4);
    door_init(&doors[4], {-room_scale, -4, room_scale * 1.5f}, {10, 12, 1}, 3,
              6);
    door_init(&doors[5], {-room_scale * 1.5f, -4, room_scale}, {1, 12, 10}, 5,
              6);
    door_init(&doors[6], {-room_scale * 0.5f, -4, room_scale}, {1, 12, 10}, 6,
              7);
    door_init(&doors[7], {0, -4, room_scale * 0.5f}, {10, 12, 1}, 7, 8);
    door_init(&doors[8], {room_scale * 2, -4, room_scale * 0.5f}, {10, 12, 1},
              4, 9);
    door_init(&doors[9], {room_scale * 1.5f, -4, 0}, {1, 12, 10}, 8, 9);
    door_init(&doors[10], {-room_scale * 2, -4, -room_scale * 0.5f},
              {10, 12, 1}, 5, 10);
    door_init(&doors[11], {0, -4, -room_scale * 0.5f}, {10, 12, 1}, 8, 11);
    door_init(&doors[12], {-room_scale * 1.5f, -4, -room_scale}, {1, 12, 10},
              10, 11);
#if 0
    // from left to right, up to down
    doors[0] = (Door){.entity = (Entity){.pos = (Vec3){-room_scale * 1.5f, -4,
                                                       room_scale * 3},
                                         .rot = (Vec3){0, 0, 0},
                                         .size = (Vec3){1, 12, 10},
                                         .color = (Vec3){1, 1, 1}},
                      .is_opened = false,
                      .open_room[0] = 0,
                      .open_room[1] = 1};
    doors[1] = (Door){
        .entity = (Entity){.pos = (Vec3){room_scale * 0.5f, -4, room_scale * 3},
                           .rot = (Vec3){0, 0, 0},
                           .size = (Vec3){1, 12, 10},
                           .color = (Vec3){1, 1, 1}},
        .is_opened = false,
        .open_room[0] = 1,
        .open_room[1] = 2};
    doors[2] = (Door){
        .entity = (Entity){.pos = (Vec3){-room_scale, -4, room_scale * 2.5f},
                           .rot = (Vec3){0, 0, 0},
                           .size = (Vec3){10, 12, 1},
                           .color = (Vec3){1, 1, 1}},
        .is_opened = false,
        .open_room[0] = 1,
        .open_room[1] = 3};
    doors[3] = (Door){
        .entity = (Entity){.pos = (Vec3){room_scale * 1.5f, -4, room_scale * 2},
                           .rot = (Vec3){0, 0, 0},
                           .size = (Vec3){1, 12, 10},
                           .color = (Vec3){1, 1, 1}},
        .is_opened = false,
        .open_room[0] = 2,
        .open_room[1] = 4};
    doors[4] = (Door){
        .entity = (Entity){.pos = (Vec3){-room_scale, -4, room_scale * 1.5f},
                           .rot = (Vec3){0, 0, 0},
                           .size = (Vec3){10, 12, 1},
                           .color = (Vec3){1, 1, 1}},
        .is_opened = false,
        .open_room[0] = 3,
        .open_room[1] = 6};
    doors[5] = (Door){
        .entity = (Entity){.pos = (Vec3){-room_scale * 1.5f, -4, room_scale},
                           .rot = (Vec3){0, 0, 0},
                           .size = (Vec3){1, 12, 10},
                           .color = (Vec3){1, 1, 1}},
        .is_opened = false,
        .open_room[0] = 5,
        .open_room[1] = 6};
    doors[6] = (Door){
        .entity = (Entity){.pos = (Vec3){-room_scale * 0.5f, -4, room_scale},
                           .rot = (Vec3){0, 0, 0},
                           .size = (Vec3){1, 12, 10},
                           .color = (Vec3){1, 1, 1}},
        .is_opened = false,
        .open_room[0] = 6,
        .open_room[1] = 7};
    doors[7] =
        (Door){.entity = (Entity){.pos = (Vec3){0, -4, room_scale * 0.5f},
                                  .rot = (Vec3){0, 0, 0},
                                  .size = (Vec3){10, 12, 1},
                                  .color = (Vec3){1, 1, 1}},
               .is_opened = false,
               .open_room[0] = 7,
               .open_room[1] = 8};
    doors[8] = (Door){
        .entity = (Entity){.pos = (Vec3){room_scale * 2, -4, room_scale * 0.5f},
                           .rot = (Vec3){0, 0, 0},
                           .size = (Vec3){10, 12, 1},
                           .color = (Vec3){1, 1, 1}},
        .is_opened = false,
        .open_room[0] = 4,
        .open_room[1] = 9};
    doors[9] =
        (Door){.entity = (Entity){.pos = (Vec3){room_scale * 1.5f, -4, 0},
                                  .rot = (Vec3){0, 0, 0},
                                  .size = (Vec3){1, 12, 10},
                                  .color = (Vec3){1, 1, 1}},
               .is_opened = false,
               .open_room[0] = 8,
               .open_room[1] = 9};
    doors[10] = (Door){.entity = (Entity){.pos = (Vec3){-room_scale * 2, -4,
                                                        -room_scale * 0.5f},
                                          .rot = (Vec3){0, 0, 0},
                                          .size = (Vec3){10, 12, 1},
                                          .color = (Vec3){1, 1, 1}},
                       .is_opened = false,
                       .open_room[0] = 5,
                       .open_room[1] = 10};
    doors[11] =
        (Door){.entity = (Entity){.pos = (Vec3){0, -4, -room_scale * 0.5f},
                                  .rot = (Vec3){0, 0, 0},
                                  .size = (Vec3){10, 12, 1},
                                  .color = (Vec3){1, 1, 1}},
               .is_opened = false,
               .open_room[0] = 8,
               .open_room[1] = 11};
    doors[12] = (Door){
        .entity = (Entity){.pos = (Vec3){-room_scale * 1.5f, -4, -room_scale},
                           .rot = (Vec3){0, 0, 0},
                           .size = (Vec3){1, 12, 10},
                           .color = (Vec3){1, 1, 1}},
        .is_opened = false,
        .open_room[0] = 10,
        .open_room[1] = 11};
#endif
}

Box box__create(float x, float z, float size)
{
    Box result = {};
    result.hp = 5;
    result.entity.pos = {x, -10 + size * .5f, z};
    result.entity.size = {size, size, size};
    result.entity.rot = {};
    result.entity.color = {1, 1, 1};
    return result;
}

GameScene::GameScene(Input* input, ArenaAllocator* persistent_memory_arena)
    : entities(persistent_memory_arena, 200)
{
    game_scene_init(this, input, persistent_memory_arena);
}

GameScene::~GameScene()
{
    game_scene_deinit(this);
}

void GameScene::update_and_render(Input* input,
                                  AssetStorage* asset_storage,
                                  GuiContext* gui,
                                  AudioContext* audio,
                                  ShaderConstant* view_proj_constant,
                                  ShaderConstant* light_constant,
                                  ShaderConstant* model_constant,
                                  WindowSize window_size,
                                  float dt)
{
    if (input->button_debug_1.is_released())
    {
        GameSceneState new_state = state == GameSceneState::Game ?
                                       GameSceneState::Editor :
                                       GameSceneState::Game;
        state_change(input, new_state);
    }

    switch (state)
    {
    case GameSceneState::Game:
        update(input, audio, asset_storage, dt);
        physics_update(&camera.pos, &camera.vel, &player_can_jump,
                       asset_storage, audio, rooms, doors, enemies,
                       ssize32(enemies), &enemy_count, bullets,
                       ssize32(bullets), &bullet_count);
        break;
    case GameSceneState::Editor:
        editor->update(&camera, rooms, 1, input, window_size, dt);
        break;
    default: assert_(false);
    }

    // Game
    if (state == GameSceneState::Game)
    {
        gui->push_label(10, 10, 1, 1, "Game State: %s (Press F9 to switch)",
                        "Game");
    }
    // Editor
    else
    {
        gui->push_label(10, 45, 1, 1, "Game State: %s (Press F9 to switch)",
                        "Editor");
        gui->push_label(10, 55, 1, 1, "Editor Mode: %s",
                        editor->editor_mode == EditorMode::Editing ?
                            "Editing" :
                            "Floating");
    }

    Vec3 camera_rumble = {(rand() % 1000) / 1000.f * this->camera_rumble,
                          (rand() % 1000) / 1000.f * this->camera_rumble,
                          (rand() % 1000) / 1000.f * this->camera_rumble};

    Mat4 view = camera.get_view_mat(camera_rumble, recoil.pos.x, recoil.pos.y);
    const Mat4 proj = mat4_persp(
        camera_fov, (float)window_size.width / (float)window_size.height,
        camera_near, camera_far);
    {
        ViewProjConstantData data = {};
        data.view_pos.v = camera.pos;
        data.view = view;
        data.proj = proj;
        view_proj_constant->set_data(&data);
    }
    {
        LightConstantData data = {};
        data.dir_light.direction.v = {-0.2f, -1.0f, -0.3f};
        data.dir_light.ambient.v = {0.5f, 0.5f, 0.5f};
        data.dir_light.diffuse.v = {0.8f, 0.8f, 0.8f};
        data.dir_light.specular.v = {1.f, 1.f, 1.f};
        light_constant->set_data(&data);
    }

    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glFrontFace(GL_CCW);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glDisable(GL_STENCIL_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glViewport(0, 0, window_size.width, window_size.height);
    glClearColor(0.3f, 0.3f, 0.3f, 1.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    RendererBindState renderer_bind_state = begin_render_with_initial_binds(
        view_proj_constant, light_constant, model_constant);

    // Render rooms
    {
        int max_room_sub_entity_count = 0;
        for (int i = 0; i < max_room_count; ++i)
            max_room_sub_entity_count +=
                rooms[i].room_template->static_entity_count;

        RenderDataList entity_render_data_list_map[(int)ModelType::Count][(
            int)TextureType::Count] = {0};
        for (int model_index = 0; model_index < (int)ModelType::Count;
             ++model_index)
        {
            for (int texture_index = 0; texture_index < (int)TextureType::Count;
                 ++texture_index)
            {
                entity_render_data_list_map[model_index][texture_index] =
                    RenderDataList::create_from_temp_memory(
                        max_room_sub_entity_count);
            }
        }

        for (int room_index = 0; room_index < max_room_count; ++room_index)
        {
            // render room
            Room* room = &rooms[room_index];

            // render room template
            RoomTemplate* room_template = room->room_template;
            for (int entity_index = 0;
                 entity_index < room_template->static_entity_count;
                 ++entity_index)
            {
                Entity* entity = &room_template->static_entities[entity_index];
                RenderDataList* entity_render_data_list =
                    &entity_render_data_list_map[(int)entity->model_index]
                                                [(int)entity->texture_index];
                Mat4 m = mat4_translate_vec3(room->pos + entity->pos) *
                         mat4_scale_vec3(entity->size);
                entity_render_data_list->push(m, entity->color);
            }
        }

        // for (int model_index = 0; model_index < ModelType_Count;
        // ++model_index)
        for (int model_index = 0; model_index < (int)ModelType::Count;
             ++model_index)
        {
            for (int texture_index = 0; texture_index < (int)TextureType::Count;
                 ++texture_index)
            {
                RenderDataList* entity_render_data_list =
                    &entity_render_data_list_map[model_index][texture_index];
                if (texture_index == (int)TextureType::None)
                {
                    draw_multiple_objects_with_single_model(
                        &renderer_bind_state,
                        &asset_storage->_models[(ModelType)model_index],
                        *entity_render_data_list, NULL, 0);
                }
                else
                {
                    draw_multiple_objects_with_single_model(
                        &renderer_bind_state,
                        &asset_storage->_models[(ModelType)model_index],
                        *entity_render_data_list,
                        &asset_storage->_textures[(TextureType)texture_index],
                        1);
                }
            }
        }
    }

    // Render boxes
    RenderDataList box_render_data_list =
        RenderDataList::create_from_temp_memory(max_room_box_count *
                                                max_room_count);
    for (int room_index = 0; room_index < max_room_count; ++room_index)
    {
        Room* room = &rooms[room_index];
        for (int box_index = 0; box_index < room->box_count; ++box_index)
        {
            Mat4 m = mat4_translate_vec3(room->pos +
                                         room->boxes[box_index].entity.pos) *
                     mat4_scale_vec3(room->boxes[box_index].entity.size);
            box_render_data_list.push(m, room->boxes[box_index].entity.color);
        }
    }
    draw_multiple_objects_with_single_model(
        &renderer_bind_state, &asset_storage->_models[ModelType::CubeTest],
        box_render_data_list, &asset_storage->_textures[TextureType::Box], 1);

    // Render doors
    {
        RenderDataList entity_render_data_list =
            RenderDataList::create_from_temp_memory(max_door_count);

        for (int door_index = 0; door_index < max_door_count; ++door_index)
        {
            Entity* entity = &doors[door_index].entity;
            Mat4 m = mat4_translate_vec3(entity->pos) *
                     mat4_scale_vec3(entity->size);
            entity_render_data_list.push(m, entity->color);
        }

        for (int door_index = 0; door_index < max_door_count; ++door_index)
        {
            draw_multiple_objects_with_single_model(
                &renderer_bind_state,
                &asset_storage->_models[ModelType::CubeTest],
                entity_render_data_list,
                &asset_storage->_textures[TextureType::Door], 1);
        }
    }

    // Render a key
    {
        if (input->button_debug_2.is_pressed())
        {
            static int key_room_index;
            key_pos.x = rooms[++key_room_index].pos.x;
            key_pos.z = rooms[key_room_index].pos.z;
        }

        key_t += dt;
        RenderDataList key_render_data_list =
            RenderDataList::create_from_temp_memory(1);
        Mat4 m = mat4_translate(key_pos + Vec3{0, sinf(key_t * 2) * 0.3f, 0}) *
                 mat4_rotate(key_t * 120, {0, 1, 0});
        key_render_data_list.push(m, {1, 1, 1});
        draw_multiple_objects_with_single_model(
            &renderer_bind_state, &asset_storage->_models[ModelType::CubeTest],
            key_render_data_list, &asset_storage->_textures[TextureType::Key],
            1);
    }

    // render all bullets
    {
        RenderDataList bullets_render_data_list =
            RenderDataList::create_from_temp_memory(max_bullet_count);

        for (int i = 0; i < max_bullet_count; ++i)
        {
            if (bullets[i].is_in_use)
            {
                Mat4 rotation_matrix =
                    ((mat4_rotate(bullets[i].entity->rot.x, {1, 0, 0}) *
                      mat4_rotate(bullets[i].entity->rot.y, {0, 1, 0})) *
                     mat4_rotate(bullets[i].entity->rot.z, {0, 0, 1}));
                Mat4 m = ((mat4_translate_vec3(bullets[i].entity->pos) *
                           mat4_scale_vec3(bullets[i].entity->size)) *
                          rotation_matrix);

                bullets_render_data_list.push(m, bullets[i].entity->color);
            }
        }

        draw_multiple_objects_with_single_model(
            &renderer_bind_state, &asset_storage->_models[ModelType::CubeTest],
            bullets_render_data_list, NULL, 0);
    }

    // render game UIs
    {
        const float game_ui_ammo_font_size = 4;
        const float game_ui_gun_font_size = 2;
        const float game_ui_info_font_size = 2.5f;
        const float game_ui_font_height = 7;
        const float game_ui_indent = 10;
        const float game_ui_gap = 8;

        float current_height = game_ui_indent;

        current_height += game_ui_font_height * game_ui_ammo_font_size;

        gui->push_label_color(game_ui_indent,
                              (float)window_size.height - current_height,
                              game_ui_ammo_font_size, .8f, .8f, .8f, 1, "%i/%i",
                              ammo, max_ammo_count);

        current_height += game_ui_font_height * game_ui_gun_font_size;
        current_height += game_ui_gap;

        gui->push_label_color(
            game_ui_indent, (float)window_size.height - current_height,
            game_ui_gun_font_size, .8f, .8f, .8f, 1, "Machine Gun");

        // show interact message for doors
        if (state == GameSceneState::Game)
        {
            const float show_message_dist = 12.f;
            Camera* camera = &this->camera;
            Vec3 camera_ray = camera->get_look();

            selected_door = NULL;

            for (int door_index = 0; door_index < max_door_count; ++door_index)
            {
                if (doors[door_index].is_opened)
                    continue;

                float dist = float_max;
                Entity* door = &doors[door_index].entity;
                if (physics_collision_check_ray_aabb(camera->pos, camera_ray,
                                                     door->pos,
                                                     door->size * 0.5f, &dist))
                {
                    if (dist > 0 && dist < show_message_dist)
                    {
                        const char* message = "press [E] to open doors";
                        float extent = (float)stb_easy_font_width(message) *
                                       game_ui_info_font_size * .5f;

                        gui->push_label_color(
                            (float)window_size.width * 0.5f - extent,
                            (float)window_size.height * 0.65f,
                            game_ui_info_font_size, .2f, 1, .2f, 1, message);

                        selected_door = &doors[door_index];
                        break;
                    }
                }
            }
        }

        // temp crosshair
        gui->push_label_color((float)window_size.width * 0.5f - 3.5f,
                              (float)window_size.height * 0.5f - 4.5f,
                              game_ui_gun_font_size, .2f, 1, .2f, 1, "+");
    }

    // render all enemies
    {
        RenderDataList enemies_render_data_list =
            RenderDataList::create_from_temp_memory(max_enemy_count);
        for (int i = 0; i < max_enemy_count; ++i)
        {
            if (enemies[i].is_in_use)
            {
                Mat4 rotation_matrix =
                    ((mat4_rotate(enemies[i].entity->rot.x, {1, 0, 0}) *
                      mat4_rotate(enemies[i].entity->rot.y, {0, 1, 0})) *
                     mat4_rotate(enemies[i].entity->rot.z, {0, 0, 1}));
                Mat4 m = ((mat4_translate_vec3(enemies[i].entity->pos) *
                           mat4_scale_vec3(enemies[i].entity->size)) *
                          rotation_matrix);

                enemies_render_data_list.push(m, enemies[i].entity->color);
            }
        }

        // TODO(illkwon): Replace wall texture to enemy texture.
        draw_multiple_objects_with_single_model(
            &renderer_bind_state,
            &asset_storage->_models[ModelType::NpcTestModel],
            enemies_render_data_list,
            &asset_storage->_textures[TextureType::Wall], 1);
    }

    // Render light source
    {
        RenderDataList light_render_data_list =
            RenderDataList::create_from_temp_memory(1);
        Mat4 m = mat4_translate(light_pos.x, light_pos.y, light_pos.z) *
                 mat4_scale(0.5f, 0.5f, 0.5f);
        light_render_data_list.push(m, {1, 1, 1});
        draw_multiple_objects_with_single_model(
            &renderer_bind_state,
            &asset_storage->_models[ModelType::LightSourceTest],
            light_render_data_list, NULL, 0);
    }

    // Editor UI
    if (state == GameSceneState::Editor)
    {
        float editor_ui_alpha =
            editor->editor_mode == EditorMode::Editing ? 1.f : 0.5f;

        const float gui_font_size = 2;
        const char* gui_text_save = "Save (ctrl+s)";
        const char* gui_text_undo = "Undo (ctrl+z)";
        const char* gui_text_redo = "Redo (ctrl+y)";
        Vec2 gui_position_stack{};
        float gui_width =
            (float)stb_easy_font_width(gui_text_save) * gui_font_size + 10;

        if (gui->push_button(gui_position_stack, {gui_width, 25},
                             {.8f, .8f, .8f, 1}, gui_font_size,
                             {.2f, .2f, .2f, 1}, gui_text_save))
        {
            log_("Doesn't work for now!");
            // editor_save_all_room_templates(game);
        }
        gui_position_stack.x += gui_width + 10;

        gui_width =
            (float)stb_easy_font_width(gui_text_undo) * gui_font_size + 10;
        if (gui->push_button(gui_position_stack, {gui_width, 25},
                             {.8f, .8f, .8f, 1}, gui_font_size,
                             {.2f, .2f, .2f, 1}, gui_text_undo))
            editor->undo();
        gui_position_stack.x += gui_width + 10;

        gui_width =
            (float)stb_easy_font_width(gui_text_redo) * gui_font_size + 10;
        if (gui->push_button(gui_position_stack, {gui_width, 25},
                             {.8f, .8f, .8f, 1}, gui_font_size,
                             {.2f, .2f, .2f, 1}, gui_text_redo))
            editor->redo();
        gui_position_stack.x += gui_width + 10;

        gui->push_console_window(window_size, editor_ui_alpha);
    }

    // Gizmo ui
    {
        const float gizmo_length = 50;
        Mat4 axis_mat = mat4(gizmo_length, 0, 0, 0, 0, gizmo_length, 0, 0, 0, 0,
                             gizmo_length, 0, 0, 0, 0, 0);
        axis_mat = view * axis_mat;
        struct
        {
            Vec3 v;
            Vec3 c;
        } axis[3] = {
            {{axis_mat.e[0][0], axis_mat.e[0][1], axis_mat.e[0][2]}, {1, 0, 0}},
            {{axis_mat.e[1][0], axis_mat.e[1][1], axis_mat.e[1][2]}, {0, 1, 0}},
            {{axis_mat.e[2][0], axis_mat.e[2][1], axis_mat.e[2][2]}, {0, 0, 1}},
        };

        if (axis[0].v.z > axis[1].v.z)
            swap(axis[0], axis[1]);
        if (axis[0].v.z > axis[2].v.z)
            swap(axis[0], axis[2]);
        if (axis[1].v.z > axis[2].v.z)
            swap(axis[1], axis[2]);

        const Vec2 gizmo_pos = {window_size.width - gizmo_length - 20,
                                gizmo_length + 20};
        for (int i = 0; i < ssize(axis); ++i)
        {
            gui->push_line_color(gizmo_pos.x, gizmo_pos.y,
                                 gizmo_pos.x + axis[i].v.x,
                                 gizmo_pos.y - axis[i].v.y, axis[i].c.x,
                                 axis[i].c.y, axis[i].c.z, 0.8f);
        }
    }
}

void GameScene::update(Input* input,
                       AudioContext* audio,
                       AssetStorage* asset_storage,
                       float dt)
{
    game_scene_update(this, input, audio, asset_storage, dt);
}

void GameScene::state_init(Input* input, GameSceneState state)
{
    game_scene_state_init(this, input, state);
}

void GameScene::state_shutdown_current_state()
{
    game_scene_state_shutdown_current_state(this);
}

void GameScene::state_change(Input* input, GameSceneState state)
{
    game_scene_state_change(this, input, state);
}
}
