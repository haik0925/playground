﻿#ifndef DSC_GAME_SCENE_H
#define DSC_GAME_SCENE_H
#include "mathematics.h"
#include "memory.h"
#include "entity.h"
#include "editor.h"
#include "asset.h"
#include "audio.h"
#include "shared.h"
#include "common.h"
#include "base_scene.h"

namespace dsc
{
constexpr float camera_fov = 60;
constexpr float camera_near = 0.1f;
constexpr float camera_far = 1000.f;

constexpr int max_bullet_count = 30;
constexpr int max_enemy_count = 20;
constexpr int max_ammo_count = 30;
constexpr float lowest_recoil_strength = 100.f;
constexpr float highest_recoil_strength = 300.f;

constexpr int max_room_count = 12;
constexpr int max_door_count = 13;
constexpr int max_room_box_count = 15;
//#define max_room_template_count 12
constexpr int max_room_template_static_entity_count = 30;

constexpr const char* room_template_data_path = "room_templates.data";
constexpr const char* room_template_data_backup_path = "room_templates.backup";
constexpr u64 room_template_binary_file_buffer_size = 1_mb;

constexpr int save_header_count = 3;

struct Editor;

struct Entities
{
    PoolAllocator allocator;
    Entities(ArenaAllocator* memory_arena, int capacity);

    Entity* allocate();
    void free(Entity* entity);
};

struct Bullet
{
    bool is_in_use;
    float lifetime;
    Vec3 vel;
    Entity* entity;
};

struct Enemy
{
    bool is_in_use;
    int life;
    Vec3 vel;

    Entity* entity;
};

/*
typedef struct WayPoint WayPoint;
typedef struct WayPoint
{
    Vec3 pos;
    Vec3 size;

    WayPoint** linked_waypoints;
    int linked_waypoints_count;

} WayPoint;
*/

struct RoomTemplate
{
    Entity static_entities[max_room_template_static_entity_count];
    int static_entity_count;

    Vec3 room_size;
    Vec3 room_pos;
};

enum class RoomTemplateType
{
    Num1,
    Num2,
    Num3,
    Num4,
    Num5,
    Num6,
    Num7,
    Num8,
    Num9,
    Num10,
    Num11,
    Num12,
    Count
};

struct Box
{
    Entity entity;
    int hp;
};

struct Room
{
    Vec3 pos;
    RoomTemplate* room_template;
    bool is_opened;

    // dynamic objects bound to room
    Box boxes[max_room_box_count];
    int box_count;
};

struct Door
{
    Entity entity;
    bool is_opened;
    int open_room[2];
};

enum class GameSceneState
{
    Game = 1,
    Editor
};

struct GameScene : BaseScene
{
    Camera camera;
    Entities entities;

    float light_angle;
    Vec3 light_pos;

    struct
    {
        Vec2 pos;
        Vec2 vel;
        float strength;
    } recoil;
    float camera_rumble;

    bool player_can_jump;

    // room templates
    enum_array<RoomTemplateType, RoomTemplate> room_templates;

    // room
    Room rooms[max_room_count];
    Door doors[max_door_count];
    Door* selected_door;

    Vec3 key_pos;
    float key_t;

    // Bullets
    Bullet bullets[max_bullet_count];
    int bullet_count;
    float bullet_current_cooltime;
    int ammo;

    // Enemies
    Enemy enemies[max_enemy_count];
    int enemy_count;

    GameSceneState state;

    Editor* editor;

    GameScene(Input* input, ArenaAllocator* persistent_memory_arena);
    ~GameScene() override;
    void update_and_render(Input* input,
                           AssetStorage* asset_storage,
                           GuiContext* gui,
                           AudioContext* audio,
                           ShaderConstant* view_proj_constant,
                           ShaderConstant* light_constant,
                           ShaderConstant* model_constant,
                           WindowSize window_size,
                           float dt) override;
    void update(Input* input,
                AudioContext* audio,
                AssetStorage* asset_storage,
                float dt);
    void state_init(Input* input, GameSceneState state);
    void state_shutdown_current_state();
    void state_change(Input* input, GameSceneState state);
};
}

#endif // DSC_GAME_SCENE_H
