#version 450

layout (location = 0) in vec2 m_uv;
layout (location = 1) in vec3 m_normal;
layout (location = 2) in vec3 m_world_pos;

layout (location = 0) out vec4 out_color;

// INCLUDE_FILE "ubo.glsl"
uniform sampler2D u_tex;

void main()
{
    vec4 view_pos = view * vec4(m_world_pos, 1);
    vec4 proj_pos = proj * view_pos;

    //out_color = vec4(vec3(gl_FragCoord.z), 1.0);
    //out_color = vec4(vec3(view_pos.z), 1.0);
    //out_color = vec4(vec3(m_world_pos.z * 0.05), 1.0);
    //out_color = vec4(vec3(-view_pos.z * 0.01), 1.0);
    out_color = vec4(vec3(proj_pos.w * 0.01), 1.0);
    //out_color = vec4(vec3(0.5f), 1.0);
}
