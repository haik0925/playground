#version 450

out gl_PerVertex { vec4 gl_Position; };

layout (location = 0) in vec2 in_pos;
//layout (location = 1) in vec2 in_uv;
layout (location = 2) in vec4 in_color;

layout (location = 0) out vec4 m_color;

// INCLUDE_FILE "ubo.glsl"

void main()
{
    m_color = in_color;
    gl_Position = proj * view * vec4(in_pos.xy, -0.5, 1.0);
}
