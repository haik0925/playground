#version 450

layout (location = 0) in vec2 m_uv;
layout (location = 1) in vec4 m_color;

layout (location = 0) out vec4 out_color;

uniform sampler2D u_tex;

void main()
{
    out_color = texture(u_tex, m_uv) * m_color;
}
