#version 450

layout (location = 0) in vec2 m_uv;
layout (location = 1) in vec3 m_normal;
layout (location = 2) in vec3 m_world_pos;

layout (location = 0) out vec4 out_color;

// INCLUDE_FILE "ubo.glsl"
uniform sampler2D u_tex;

void main()
{
    vec3 norm = normalize(m_normal);

    vec3 final_light_value = vec3(0.0);

    // Process a directional light
    {
        vec3 ambient = dir_light.ambient;
        vec3 light_dir = normalize(-dir_light.direction);
        float diff = max(dot(norm, light_dir), 0.0);
        vec3 diffuse = diff * dir_light.diffuse;
        vec3 view_dir = normalize(view_pos - m_world_pos);
        vec3 reflect_dir = reflect(-light_dir, norm);
        float spec = pow(max(dot(view_dir, reflect_dir), 0.0), 32);
        vec3 specular = spec * dir_light.specular;
        vec3 dir_light_value = ambient + diffuse + specular;
        final_light_value += dir_light_value;
    }

    // Process point lights
    for (int i = 0; i < point_light_count; ++i)
    {
        PointLight light = point_lights[i];
        float distance = length(light.position - m_world_pos);
        float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
        vec3 ambient = light.ambient * attenuation;
        vec3 diffuse = light.diffuse * attenuation;
        vec3 specular = light.specular * attenuation;
        vec3 point_light_value = ambient + diffuse + specular;
        final_light_value += point_light_value;
    }

    /*float ambient_strength = 0.1;
    vec3 ambient = ambient_strength * light_color;

    vec3 light_dir = normalize(light_pos - m_world_pos);
    float diff = max(dot(norm, light_dir), 0.0);
    vec3 diffuse = diff * light_color;

    float specular_strength = 0.2;
    vec3 view_dir = normalize(view_pos - m_world_pos);
    vec3 reflect_dir = reflect(-light_dir, norm);
    float spec = pow(max(dot(view_dir, reflect_dir), 0.0), 32);*/

    out_color = texture(u_tex, vec2(m_uv.x, 1.0 - m_uv.y)) * vec4(model_color * final_light_value, 1);
}
