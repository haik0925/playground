#version 450

out gl_PerVertex { vec4 gl_Position; };

layout (location = 0) in vec3 in_pos;
layout (location = 1) in vec2 in_uv;
layout (location = 2) in vec3 in_normal;

layout (location = 0) out vec2 m_uv;
layout (location = 1) out vec3 m_normal;
layout (location = 2) out vec3 m_world_pos;

// INCLUDE_FILE "ubo.glsl"

void main()
{
    m_uv = in_uv;
    m_normal = mat3(transpose(model_inv)) * in_normal;
    m_world_pos = vec3(model * vec4(in_pos, 1.0));
    gl_Position = proj * view * model * vec4(in_pos, 1.0);
}
