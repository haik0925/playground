layout (binding = 0, std140) uniform ViewProj
{
    vec3 view_pos;
    mat4 view;
    mat4 proj;
};

struct DirLight
{
    vec3 direction;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

struct PointLight
{
    vec3 position;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float constant;
    float linear;
    float quadratic;
};

layout (binding = 1, std140) uniform LightInfos
{
    DirLight dir_light;
    PointLight point_lights[10];
    int point_light_count;
};

layout (binding = 2, std140) uniform ModelData
{
    mat4 model;
    mat4 model_inv;
    vec3 model_color;
};
