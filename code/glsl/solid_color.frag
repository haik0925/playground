#version 450

layout (location = 0) out vec4 out_color;

// INCLUDE_FILE "ubo.glsl"

void main()
{
    out_color = vec4(model_color, 1);
}
