#version 450

out gl_PerVertex { vec4 gl_Position; };

layout (location = 0) in vec3 in_pos;

// INCLUDE_FILE "ubo.glsl"

void main()
{
    gl_Position = proj * view * model * vec4(in_pos, 1.0);
}
