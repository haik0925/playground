﻿#include "gui.h"
#define STB_EASY_FONT_IMPLEMENTATION
#include "stb_easy_font.h"
#include "mathematics.h"
#include "common.h"
#include "debug.h"
#include "asset.h"
#include "game_scene.h"
#include <stdio.h>
#include <stdarg.h>
#include "glad.h"

namespace dsc
{
GuiContext::GuiContext(AssetStorage* asset_storage)
    : vertex_object(nullptr,
                    sizeof(vertices),
                    sizeof(*vertices),
                    s_attribs,
                    ssize32(s_attribs))
{
    assert_(!inited);

    solid_color_ui_shader = asset_storage->_shaders[ShaderType::DebugGuiSolid];
    texture_ui_shader = asset_storage->_shaders[ShaderType::DebugGuiTextured];

    default_text_color = {0, 1, 0.62f};
    default_button_color = {0.6f, 0.6f, 0.6f};

    button_texture_size = {48, 48};
    button_texture_border = 8;

    default_texture = get_default_texture();

    inited = true;
}

float GuiContext::get_font_height() const
{
    return 12; // copied from stb_easy_font source code
}

void GuiContext::push_vertex(GuiType type,
                             float x,
                             float y,
                             float u,
                             float v,
                             float r,
                             float g,
                             float b,
                             float a,
                             Texture texture)
{
    assert_(vertex_count < gui_max_vertex_count);
    if (render_commmand_count > 0 &&
        render_commmands[render_commmand_count - 1].type == type &&
        render_commmands[render_commmand_count - 1].texture == texture)
    {
        ++render_commmands[render_commmand_count - 1].vertex_count;
    }
    else
    {
        assert_(render_commmand_count <= gui_max_render_command_count);
        render_commmands[render_commmand_count++] = GuiRenderCommand{
            type,
            1,
            texture,
        };
    }

    GuiVertex* vertex = &vertices[vertex_count];
    *vertex = GuiVertex{
        {x, y},
        {u, v},
        {r, g, b, a},
    };
    ++vertex_count;
}

void GuiContext::push_quad(GuiType type,
                           float x,
                           float y,
                           float w,
                           float h,
                           float umin,
                           float vmin,
                           float umax,
                           float vmax,
                           float r,
                           float g,
                           float b,
                           float a,
                           Texture texture)
{
    push_vertex(type, x, y, umin, vmin, r, g, b, a, texture);
    push_vertex(type, x, y + h, umin, vmax, r, g, b, a, texture);
    push_vertex(type, x + w, y + h, umax, vmax, r, g, b, a, texture);
    push_vertex(type, x + w, y + h, umax, vmax, r, g, b, a, texture);
    push_vertex(type, x + w, y, umax, vmin, r, g, b, a, texture);
    push_vertex(type, x, y, umin, vmin, r, g, b, a, texture);
}

void GuiContext::push_circle(GuiType type,
                             float center_x,
                             float center_y,
                             float radius,
                             float umin,
                             float vmin,
                             float umax,
                             float vmax,
                             float r,
                             float g,
                             float b,
                             float a,
                             Texture texture)
{
    const int triangle_count = 36;
    float triangle_unit_angle = to_radians(360.f / (float)triangle_count);
    float ucenter = (umin + umax) * 0.5f;
    float vcenter = (vmin + vmax) * 0.5f;
    float uvhalfwidth = (umax - umin) * 0.5f;
    float uvhalfheight = (vmax - vmin) * 0.5f;

    for (int i = 0; i < triangle_count; ++i)
    {
        float angles[2] = {(float)(i + 1) * triangle_unit_angle,
                           (float)i * triangle_unit_angle};
        float cos0 = cosf(angles[0]);
        float sin0 = sinf(angles[0]);
        float cos1 = cosf(angles[1]);
        float sin1 = sinf(angles[1]);
        float x0 = center_x + radius * cos0;
        float y0 = center_y + radius * sin0;
        float x1 = center_x + radius * cos1;
        float y1 = center_y + radius * sin1;
        float u0 = ucenter + uvhalfwidth * cos0;
        float v0 = vcenter + uvhalfheight * sin0;
        float u1 = ucenter + uvhalfwidth * cos1;
        float v1 = vcenter + uvhalfheight * sin1;

        push_vertex(type, center_x, center_y, ucenter, vcenter, r, g, b, a,
                    texture);
        push_vertex(type, x0, y0, u0, v0, r, g, b, a, texture);
        push_vertex(type, x1, y1, u1, v1, r, g, b, a, texture);
    }
}

void GuiContext::push_stb_easy_font_quads(float x,
                                          float y,
                                          float scale,
                                          float r,
                                          float g,
                                          float b,
                                          float a,
                                          void* stb_easy_font_vertices,
                                          int num_quads)
{
    for (int i = 0; i < num_quads; ++i)
    {
        StbEasyFontTextVertex* v =
            (StbEasyFontTextVertex*)stb_easy_font_vertices + i * 4;
        float qx = x + (v[0].x - x) * scale;
        float qy = y + (v[0].y - y) * scale;
        float qw = (v[2].x - v[0].x) * scale;
        float qh = (v[2].y - v[0].y) * scale;
        push_quad(GuiType::Text, qx, qy, qw, qh, 0, 0, 1, 1, r, g, b, a,
                  default_texture);
    }
}

void GuiContext::push_label_color(float x,
                                  float y,
                                  float scale,
                                  float r,
                                  float g,
                                  float b,
                                  float a,
                                  const char* format,
                                  ...)
{
    global_var char buffer[500];
    va_list args;
    va_start(args, format);
    vsprintf(buffer, format, args);
    va_end(args);

    global_var StbEasyFontTextVertex vertex_buffer[7000]; // ~500 chars
    int num_quads = stb_easy_font_print(x, y, buffer, NULL, vertex_buffer,
                                        sizeof(vertex_buffer));

    push_stb_easy_font_quads(x, y, scale, r, g, b, a, vertex_buffer, num_quads);
}

void GuiContext::push_label(float x,
                            float y,
                            float scale,
                            float a,
                            const char* format,
                            ...)
{
    global_var char buffer[500];
    va_list args;
    va_start(args, format);
    vsprintf(buffer, format, args);
    va_end(args);

    push_label_color(x, y, scale, default_text_color.x, default_text_color.y,
                     default_text_color.z, a, buffer);
}

void GuiContext::push_label_wrap(float x, float y, const char* text)
{
    int text_width = stb_easy_font_width(text);
}

void GuiContext::push_panel_color(float x,
                                  float y,
                                  float w,
                                  float h,
                                  float r,
                                  float g,
                                  float b,
                                  float a)
{
    push_quad(GuiType::Panel, x, y, w, h, 0, 0, 1, 1, r, g, b, a,
              default_texture);
}

bool GuiContext::push_button(Vec2 pos,
                             Vec2 size,
                             Color button_color,
                             float font_scale,
                             Color label_color,
                             const char* label)
{
    bool result = push_button_texture(pos, size, button_color, font_scale,
                                      label_color, default_texture, label);
    return result;
}

bool GuiContext::push_button_circle(Vec2 center,
                                    float radius,
                                    Color button_color,
                                    float font_scale,
                                    Color label_color,
                                    const char* label)
{
    bool result =
        push_button_circle_texture(center, radius, button_color, font_scale,
                                   label_color, default_texture, label);
    return result;
}

bool GuiContext::push_button_texture(Vec2 pos,
                                     Vec2 size,
                                     Color button_color,
                                     float font_scale,
                                     Color label_color,
                                     Texture texture,
                                     const char* label)
{
    // TODO: we should maybe let user to choose hover and pressed color.
    const Color button_hover_color = button_color * 1.2f;
    const Color button_pressed_color = button_color * 1.5f;

    bool result = false;

    struct
    {
        Vec2 pos;
        Vec2 size;
        Color color;
    } button = {
        pos,
        size,
        button_color,
    };

    float mx = mouse_pos.x;
    float my = mouse_pos.y;
    if (mx >= button.pos.x && mx < button.pos.x + button.size.x &&
        my >= button.pos.y && my < button.pos.y + button.size.y)
    {
        if (mouse_is_down)
            button.color = button_pressed_color;
        else if (mouse_is_released)
        {
            button.color = button_pressed_color;
            result = true;
        }
        else
            button.color = button_hover_color;
    }

    if (button_texture_border <= 0)
    {
        push_quad(GuiType::Button, pos.x, pos.y, size.x, size.y, 0, 0, 1, 1,
                  button.color.x, button.color.y, button.color.z,
                  button.color.w, texture);
    }
    else
    {
        Vec2 q = pos;

        Vec2 uv_border{
            (float)button_texture_border / (float)button_texture_size.x,
            (float)button_texture_border / (float)button_texture_size.y};

        // top side
        push_quad(GuiType::Button, q.x, q.y, (float)button_texture_border,
                  (float)button_texture_border, 0, 0, uv_border.x, uv_border.y,
                  button.color.x, button.color.y, button.color.z,
                  button.color.w, texture);
        q.x += button_texture_border;
        push_quad(GuiType::Button, q.x, q.y, size.x - button_texture_border * 2,
                  (float)button_texture_border, uv_border.x, 0, 1 - uv_border.x,
                  uv_border.y, button.color.x, button.color.y, button.color.z,
                  button.color.w, texture);
        q.x += (size.x - button_texture_border * 2);
        push_quad(GuiType::Button, q.x, q.y, (float)button_texture_border,
                  (float)button_texture_border, 1 - uv_border.x, 0, 1,
                  uv_border.y, button.color.x, button.color.y, button.color.z,
                  button.color.w, texture);

        // center side
        q.x = pos.x;
        q.y += button_texture_border;
        push_quad(GuiType::Button, q.x, q.y, (float)button_texture_border,
                  size.y - button_texture_border * 2, 0, uv_border.y,
                  uv_border.x, 1 - uv_border.y, button.color.x, button.color.y,
                  button.color.z, button.color.w, texture);
        q.x += button_texture_border;
        push_quad(GuiType::Button, q.x, q.y, size.x - button_texture_border * 2,
                  size.y - button_texture_border * 2, uv_border.x, uv_border.y,
                  1 - uv_border.x, 1 - uv_border.y, button.color.x,
                  button.color.y, button.color.z, button.color.w, texture);
        q.x += (size.x - button_texture_border * 2);
        push_quad(GuiType::Button, q.x, q.y, (float)button_texture_border,
                  size.y - button_texture_border * 2, 1 - uv_border.x,
                  uv_border.y, 1, 1 - uv_border.y, button.color.x,
                  button.color.y, button.color.z, button.color.w, texture);

        // bottom side
        q.x = pos.x;
        q.y += (size.y - button_texture_border * 2);
        push_quad(GuiType::Button, q.x, q.y, (float)button_texture_border,
                  (float)button_texture_border, 0, 1 - uv_border.y, uv_border.x,
                  1, button.color.x, button.color.y, button.color.z,
                  button.color.w, texture);
        q.x += button_texture_border;
        push_quad(GuiType::Button, q.x, q.y, size.x - button_texture_border * 2,
                  (float)button_texture_border, uv_border.x, 1 - uv_border.y,
                  1 - uv_border.x, 1, button.color.x, button.color.y,
                  button.color.z, button.color.w, texture);
        q.x += (size.x - button_texture_border * 2);
        push_quad(GuiType::Button, q.x, q.y, (float)button_texture_border,
                  (float)button_texture_border, 1 - uv_border.x,
                  1 - uv_border.y, 1, 1, button.color.x, button.color.y,
                  button.color.z, button.color.w, texture);
    }

    // TODO: Code duplication with gui_push_button_circle
    float text_width = stb_easy_font_width(label) * font_scale;
    float text_height = stb_easy_font_height(label) * font_scale;
    float tx = pos.x + size.x * 0.5f - text_width * 0.5f;
    float ty = pos.y + size.y * 0.5f - text_height * 0.5f +
               3; // TODO: temporary magic number for adjusting text height
    push_label_color(tx, ty, font_scale, label_color.x, label_color.y,
                     label_color.z, label_color.w, label);
    return result;
}

bool GuiContext::push_button_circle_texture(Vec2 center,
                                            float radius,
                                            Color button_color,
                                            float font_scale,
                                            Color label_color,
                                            Texture texture,
                                            const char* label)
{
    // TODO: we should maybe let user to choose hover and pressed color.
    const Color button_hover_color = button_color * 1.2f;
    const Color button_pressed_color = button_color * 1.5f;

    bool result = false;

    Color final_button_color = button_color;

    float mrx = mouse_pos.x - center.x;
    float mry = mouse_pos.y - center.y;
    if ((radius * radius) >= (mrx * mrx + mry * mry))
    {
        if (mouse_is_down)
            final_button_color = button_pressed_color;
        else if (mouse_is_released)
        {
            final_button_color = button_pressed_color;
            result = true;
        }
        else
            final_button_color = button_hover_color;
    }

    push_circle(GuiType::Button, center.x, center.y, radius, 0, 0, 1, 1,
                final_button_color.r, final_button_color.g,
                final_button_color.b, final_button_color.a, texture);

    // TODO: Code duplication with gui_push_button
    float text_width = stb_easy_font_width(label) * font_scale;
    float text_height = stb_easy_font_height(label) * font_scale;
    float tx = center.x - text_width * 0.5f;
    float ty = center.y - text_height * 0.5f +
               3; // TODO: temporary magic number for adjusting text height
    push_label_color(tx, ty, font_scale, label_color.x, label_color.y,
                     label_color.z, label_color.w, label);

    return result;
}

void GuiContext::push_line_color(float x0,
                                 float y0,
                                 float x1,
                                 float y1,
                                 float r,
                                 float g,
                                 float b,
                                 float a)
{
    push_vertex(GuiType::Line, x0, y0, 0, 0, r, g, b, a, default_texture);
    push_vertex(GuiType::Line, x1, y1, 1, 1, r, g, b, a, default_texture);
}

void GuiContext::push_console_window(IntVec2 window_size, float transparency)
{
    Vec2 console_pos{0, 500};
    Vec2 console_size{(float)window_size.width,
                      (float)(window_size.height - console_pos.y)};
    push_panel_color(console_pos.x, console_pos.y, console_size.x,
                     console_size.y, 0, 0, 0, transparency);
    char* console_buffer = debug_get_global_log_buffer();
    u64 console_buffer_len = debug_get_global_log_buffer_size();
    int max_line_length = 0;
    int line_count = 0;
    {
        int line_length = 0;
        for (u64 i = 0; i < console_buffer_len; ++i)
        {
            char* ch = console_buffer + i;
            if (*ch == '\n')
            {
                if (max_line_length < line_length)
                    max_line_length = line_length;
                line_length = 0;
                ++line_count;
            }
            else
                ++line_length;
        }
    }

    int font_width = stb_easy_font_width(console_buffer);

    char* console_buffer_to_print = NULL;
    u64 console_buffer_len_to_print = 0;
    if (font_width <= console_size.x)
    {
        console_buffer_to_print = console_buffer;
        console_buffer_len_to_print = console_buffer_len;
    }
    else
    {
        int char_width = font_width / max_line_length;

        int wrap_count = 0;
        {
            int line_width = 0;
            for (u64 i = 0; i < console_buffer_len; ++i)
            {
                char* ch = console_buffer + i;
                char null_terminated[] = {*ch, '\0'};
                line_width += stb_easy_font_width(null_terminated);

                if (line_width > console_size.x)
                {
                    line_width = 0;
                    ++wrap_count;
                }
                else if (*ch == '\n')
                    line_width = 0;
            }
        }

        u64 wrapped_console_buffer_len = console_buffer_len + wrap_count;
        char* wrapped_console_buffer =
            (char*)temp_memory_alloc(wrapped_console_buffer_len + 1);
        char* dst = wrapped_console_buffer;
        char* src = console_buffer;

        int line_width = 0;
        for (u64 i = 0; i < wrapped_console_buffer_len; ++i)
        {
            char null_terminated[] = {*src, '\0'};
            line_width += stb_easy_font_width(null_terminated);
            if (line_width > console_size.x)
            {
                *dst++ = '\n';
                line_width = 0;
            }
            else
            {
                *dst++ = *src++;
                if (*src == '\n')
                    line_width = 0;
            }
        }
        *dst = '\0';

        console_buffer_to_print = wrapped_console_buffer;
        console_buffer_len_to_print = wrapped_console_buffer_len;
    }

    int buffer_size =
        (int)console_buffer_len_to_print * 270; // 270 bytes per character
    if (buffer_size > 0)
    {
        void* buffer = temp_memory_alloc(buffer_size);
        int offset_y =
            stb_easy_font_height(console_buffer_to_print) - (int)console_size.y;
        int num_quads = stb_easy_font_print(
            console_pos.x, console_pos.y - (float)offset_y,
            console_buffer_to_print, NULL, buffer, buffer_size);
        StbEasyFontTextVertex* raw = (StbEasyFontTextVertex*)buffer;
        StbEasyFontTextVertex* clipped =
            (StbEasyFontTextVertex*)temp_memory_alloc(buffer_size);
        StbEasyFontTextVertex* v = clipped;

        int clipped_num_quads = 0;
        for (int i = 0; i < num_quads; ++i)
        {
            bool is_inside_console = true;
            for (int j = 0; j < 4; ++j)
            {
                if (raw[j].y < (console_pos.y + 6))
                {
                    is_inside_console = false;
                    break;
                }
            }
            if (is_inside_console)
            {
                memcpy(v, raw, sizeof(*raw) * 4);

                ++clipped_num_quads;
                v += 4;
            }

            raw += 4;
        }
        push_stb_easy_font_quads(console_pos.x, console_pos.y, 1, 0, 1, 0.62f,
                                 transparency, clipped, clipped_num_quads);
    }
}

void GuiContext::render()
{
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_STENCIL_TEST);
    vertex_object.update_buffer(vertices, sizeof(*vertices) * vertex_count);
    vertex_object.bind();

    int vertex_offset = 0;
    for (int i = 0; i < render_commmand_count; ++i)
    {
        GuiRenderCommand* render_command = &render_commmands[i];
        glBindTextureUnit(0, render_command->texture);

        switch (render_command->type)
        {
        case GuiType::Button:
            glBindProgramPipeline(texture_ui_shader.program);
            break;
        case GuiType::Panel:
        case GuiType::Text:
        case GuiType::Line:
            glBindProgramPipeline(solid_color_ui_shader.program);
            break;
        default: assert_(false);
        }

        switch (render_command->type)
        {
        case GuiType::Button:
        case GuiType::Panel:
        case GuiType::Text:
            glDrawArrays(GL_TRIANGLES, vertex_offset,
                         render_command->vertex_count);
            break;
        case GuiType::Line:
            glDrawArrays(GL_LINES, vertex_offset, render_command->vertex_count);
            break;
        default: assert_(false);
        }
        vertex_offset += render_command->vertex_count;
    }
}

void GuiContext::flush()
{
    vertex_count = 0;
    render_commmand_count = 0;
}
}
