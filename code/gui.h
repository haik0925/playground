﻿#ifndef DSC_GUI_H
#define DSC_GUI_H

#include "primitive_types.h"
#include "mathematics.h"
#include "renderer.h"
#include "shared.h"

constexpr int gui_max_vertex_count = 300000;
constexpr int gui_max_render_command_count = 1000;

namespace dsc
{
struct AssetStorage;

struct GuiVertex
{
    Vec2 pos;
    Vec2 uv;
    Vec4 color;
};

enum class GuiType
{
    Panel,
    Button,
    Text,
    Line,
};

struct GuiRenderCommand
{
    GuiType type;
    int vertex_count;
    Texture texture;
};

struct GuiContext
{
    bool inited = false;

    GuiVertex vertices[gui_max_vertex_count] = {};
    int vertex_count = 0;
    VertexObject vertex_object;
    Shader solid_color_ui_shader = {};
    Shader texture_ui_shader = {};

    GuiRenderCommand render_commmands[gui_max_render_command_count] = {};
    int render_commmand_count = 0;

    Vec2 mouse_pos = {};
    bool mouse_is_down = false;
    bool mouse_is_released = false;

    Vec3 default_text_color = {};
    Vec3 default_button_color = {};
    IntVec2 button_texture_size = {};
    int button_texture_border = 0;
    Texture default_texture = 0;

    static inline VertexLayoutAttrib s_attribs[] = {
        {VertexUsage::Position, VertexComponentType::Float, 2,
         offsetof(GuiVertex, pos)},
        {VertexUsage::TexCoord, VertexComponentType::Float, 2,
         offsetof(GuiVertex, uv)},
        {VertexUsage::Color, VertexComponentType::Float, 4,
         offsetof(GuiVertex, color)},
    };

    GuiContext(AssetStorage* asset_storage);

    float get_font_height() const;

    void push_vertex(GuiType type,
                     float x,
                     float y,
                     float u,
                     float v,
                     float r,
                     float g,
                     float b,
                     float a,
                     Texture texture);
    void push_quad(GuiType type,
                   float x,
                   float y,
                   float w,
                   float h,
                   float umin,
                   float vmin,
                   float umax,
                   float vmax,
                   float r,
                   float g,
                   float b,
                   float a,
                   Texture texture);
    void push_circle(GuiType type,
                     float center_x,
                     float center_y,
                     float radius,
                     float umin,
                     float vmin,
                     float umax,
                     float vmax,
                     float r,
                     float g,
                     float b,
                     float a,
                     Texture texture);
    void push_stb_easy_font_quads(float x,
                                  float y,
                                  float scale,
                                  float r,
                                  float g,
                                  float b,
                                  float a,
                                  void* stb_easy_font_vertices,
                                  int num_quads);
    void push_label_color(float x,
                          float y,
                          float scale,
                          float r,
                          float g,
                          float b,
                          float a,
                          const char* format,
                          ...);
    void push_label(float x,
                    float y,
                    float scale,
                    float a,
                    const char* format,
                    ...);
    void push_label_wrap(float x, float y, const char* text);
    void push_panel_color(float x,
                          float y,
                          float w,
                          float h,
                          float r,
                          float g,
                          float b,
                          float a);
    bool push_button(Vec2 pos,
                     Vec2 size,
                     Color button_color,
                     float font_scale,
                     Color label_color,
                     const char* label);
    bool push_button_circle(Vec2 center,
                            float radius,
                            Color button_color,
                            float font_scale,
                            Color label_color,
                            const char* label);
    bool push_button_texture(Vec2 pos,
                             Vec2 size,
                             Color button_color,
                             float font_scale,
                             Color label_color,
                             Texture texture,
                             const char* label);
    bool push_button_circle_texture(Vec2 center,
                                    float radius,
                                    Color button_color,
                                    float font_scale,
                                    Color label_color,
                                    Texture texture,
                                    const char* label);
    void push_line_color(float x0,
                         float y0,
                         float x1,
                         float y1,
                         float r,
                         float g,
                         float b,
                         float a);
    void push_console_window(IntVec2 window_size, float transparency);
    void render();
    void flush();
};

struct StbEasyFontTextVertex
{
    float x, y, z;
    u8 padding[4];
};

}

#endif // DSC_GUI_H
