﻿#include "common.h"
#include "mathematics.h"
#include "win32.h"
#include "audio.h"
#include "game_scene.h"
#include "test_scene.h"
#include "editor.h"
#include "renderer.h"
#include "physics.h"
#include "asset.h"
#include "debug.h"
#include "memory.h"
#include "app.h"
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN 1
#endif
#include <Windows.h>
#include <timeapi.h>
#include <time.h>
#include <stdlib.h>
#include "glad.h"

using namespace dsc;

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
    srand((unsigned)time(0));

#if DEBUG
    static_assert(sizeof(LPVOID) == 8, "Must be 64bit address");
    LPVOID base_address = (LPVOID)terabytes(2);
#else
    LPVOID base_address = NULL;
#endif

    // This is the only part where heap allocation happens
    ArenaAllocator persistent_memory_arena = {};
    {
        constexpr u64 persistent_memory_size = 200_mb;
        constexpr u64 temp_memory_size = 100_mb;
        u64 total_memory_size = persistent_memory_size + temp_memory_size;
        void* total_memory =
            VirtualAlloc(base_address, total_memory_size,
                         MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
        assert_(total_memory != NULL);
        void* persistent_memory = total_memory;
        persistent_memory_arena.memory = persistent_memory;
        persistent_memory_arena.size = persistent_memory_size;

        void* temp_memory = (u8*)persistent_memory + persistent_memory_size;
        temp_memory_init(temp_memory, temp_memory_size);
    }

    {
        u64 log_buffer_size = 20_kb;
        void* log_buffer = persistent_memory_arena.push(log_buffer_size);
        debug_set_global_log_buffer(log_buffer, log_buffer_size);
    }

    // Set win32 schedular granularity
    {
        TIMECAPS time_caps;
        timeGetDevCaps(&time_caps, sizeof(time_caps));
        log_("Minimum time resolution: %d milliseconds", time_caps.wPeriodMin);
        timeBeginPeriod(time_caps.wPeriodMin);
    }

    const int initial_window_width = 1280;
    const int initial_window_height = 720;
    Win32Context win32 =
        win32_init(initial_window_width, initial_window_height);
    log_("Window Size: %d %d", win32_window_size(&win32).width,
         win32_window_size(&win32).height);
    win32_init_wgl(&win32);

    AudioContext audio(win32.window);

    const float target_dt = 1.f / 60.f;
    u64 last_counter = win32_current_clock();
    Input input = {0};

    auto asset_storage =
        std::make_unique<AssetStorage>(&persistent_memory_arena);

    /*GuiContext* gui = (GuiContext*)memory_arena_push(&persistent_memory_arena,
                                                     sizeof(GuiContext));*/
    auto gui = std::make_unique<GuiContext>(asset_storage.get());
    // gui_context_init(gui, graphics, asset_storage.get());

    ArenaAllocator game_scene_memory_arena =
        persistent_memory_arena.create_sub_arena(1_mb);
    std::unique_ptr<BaseScene> scene =
#if 0
        std::make_unique<GameScene>(&input, &persistent_memory_arena);
#else
        std::make_unique<TestScene>(asset_storage.get());
    input.lock_mouse_to_center = true;
#endif

        bool running = true;

    ShaderConstant view_proj_constant(sizeof(ViewProjConstantData));
    ShaderConstant light_constant(sizeof(LightConstantData));
    ShaderConstant model_constant(sizeof(ModelConstantData));

    while (running)
    {
        for (int i = 0; i < ssize(input.buttons); ++i)
            input.buttons[i].was_down = input.buttons[i].is_down;

        AppEvent event = {};
        while (win32_poll_event(&event))
        {
            switch (event.type)
            {
            case AppEventType::Quit: running = false; break;

            case AppEventType::Keyupdown:
                switch (event.key.code)
                {
                case AppKeyCode::W:
                    input.button_move_forward.process_event(&event.key);
                    break;
                case AppKeyCode::A:
                    input.button_move_left.process_event(&event.key);
                    break;
                case AppKeyCode::S:
                    input.button_move_back.process_event(&event.key);
                    if (input.button_control.is_down)
                        input.button_editor_save.process_event(&event.key);
                    break;
                case AppKeyCode::D:
                    input.button_move_right.process_event(&event.key);
                    break;
                case AppKeyCode::ArrowUp:
                    input.button_action_up.process_event(&event.key);
                    break;
                case AppKeyCode::ArrowDown:
                    input.button_action_down.process_event(&event.key);
                    break;
                case AppKeyCode::ArrowLeft:
                    input.button_action_left.process_event(&event.key);
                    break;
                case AppKeyCode::ArrowRight:
                    input.button_action_right.process_event(&event.key);
                    break;
                case AppKeyCode::E:
                    input.button_interact.process_event(&event.key);
                    break;
                case AppKeyCode::R:
                    input.button_reload.process_event(&event.key);
                    break;
                case AppKeyCode::Y:
                    if (input.button_control.is_down)
                        input.button_editor_redo.process_event(&event.key);
                    break;
                case AppKeyCode::Z:
                    if (input.button_control.is_down)
                        input.button_editor_undo.process_event(&event.key);
                    break;
                case AppKeyCode::Num1:
                    input.button_select_weapon_1.process_event(&event.key);
                    break;
                case AppKeyCode::LMouse:
                    input.button_left_click.process_event(&event.key);
                    break;
                case AppKeyCode::RMouse:
                    input.button_right_click.process_event(&event.key);
                    break;
                case AppKeyCode::Space:
                    input.button_toggle_editor_edit_mode.process_event(
                        &event.key);
                    input.button_move_jump.process_event(&event.key);
                    break;
                case AppKeyCode::F9:
                    input.button_debug_1.process_event(&event.key);
                    break;
                case AppKeyCode::F10:
                    input.button_debug_2.process_event(&event.key);
                    break;
                case AppKeyCode::F11:
                    input.button_debug_3.process_event(&event.key);
                    break;
                case AppKeyCode::F8:
                    input.button_debug_4.process_event(&event.key);
                    break;
                case AppKeyCode::Control:
                    input.button_control.process_event(&event.key);
                    break;
                case AppKeyCode::ESC: running = false; break;
                case AppKeyCode::Delete:
                    input.button_editor_delete.process_event(&event.key);
                    break;
                }
                break;
            }
        }

        const WindowSize window_size = win32_window_size(&win32);

        // Compute mouse input
        IntVec2 last_mouse_pos = input.mouse_pos;
        {
            input.mouse_pos = win32_cursor_pos(&win32);
            input.mouse_delta = input.mouse_pos - last_mouse_pos;
            if (win32_is_window_focused() && input.lock_mouse_to_center)
            {
                input.mouse_pos = {window_size.width / 2,
                                   window_size.height / 2};
                win32_set_cursor_pos(&win32, input.mouse_pos.x,
                                     input.mouse_pos.y);
            }
        }
        win32_show_cursor(input.show_cursor);

        // Update gui input state
        const Mat4 ui_view = mat4_identity();
        const Mat4 ui_proj =
            mat4_ortho(0, (float)window_size.width, (float)window_size.height,
                       0, 0.1f, 10.f);
        gui->mouse_pos =
            screen_to_world(input.mouse_pos,
                            {window_size.width, window_size.height}, ui_proj,
                            mat4_identity())
                .xy();
        gui->mouse_is_down = input.button_left_click.is_down;
        gui->mouse_is_released = input.button_left_click.is_released();

        scene->update_and_render(&input, asset_storage.get(), gui.get(), &audio,
                                 &view_proj_constant, &light_constant,
                                 &model_constant, window_size, target_dt);

        {
            ViewProjConstantData data = {};
            data.view = ui_view;
            data.proj = ui_proj;
            view_proj_constant.set_data(&data);
        }
        gui->render();
        gui->flush();

        audio.update();

        temp_memory_flush();

        Win32Clock counter = win32_current_clock();
        float dt = win32_elapsed_time(last_counter, counter);
        int ms_to_sleep = (int)(target_dt * 1000.f) - (int)(dt * 1000.f + 2.f);
        if (ms_to_sleep > 0 && dt < target_dt)
            Sleep(ms_to_sleep);
        counter = win32_current_clock();
        dt = win32_elapsed_time(last_counter, counter);
        while (dt < target_dt)
        {
            counter = win32_current_clock();
            dt = win32_elapsed_time(last_counter, counter);
        }
        last_counter = counter;

        SwapBuffers((HDC)win32.dc);
    }

    // game_scene_deinit(game_scene);

    return 0;
}
