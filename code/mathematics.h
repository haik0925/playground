﻿#ifndef DSC_MATHEMATICS_H
#define DSC_MATHEMATICS_H

namespace dsc
{
constexpr float pi32 = 3.14159265359f;

inline float to_radians(float degrees)
{
    float result = degrees * (pi32 / 180.f);
    return result;
}

inline float to_degrees(float radians)
{
    float result = radians * (180.f / pi32);
    return result;
}

union Vec2
{
    struct
    {
        float x, y;
    };
    struct
    {
        float width, height;
    };

    float dot(Vec2 other) const;
    float lengthsq() const;
    float length() const;
    Vec2 normalize() const;
};

#if 0
Vec2 vec2_negate(Vec2 v);
Vec2 vec2_add(Vec2 a, Vec2 b);
Vec2 vec2_sub(Vec2 a, Vec2 b);
Vec2 vec2_mult(Vec2 a, Vec2 b);
Vec2 vec2_multf(Vec2 a, float b);
Vec2 vec2_div(Vec2 a, Vec2 b);
Vec2 vec2_divf(Vec2 a, float b);
bool vec2_equals(Vec2 a, Vec2 b);
float vec2_dot(Vec2 a, Vec2 b);
float vec2_lengthsq(Vec2 v);
float vec2_length(Vec2 v);
Vec2 vec2_normalize(Vec2 v);
#endif
Vec2 operator-(Vec2 v);
Vec2 operator+(Vec2 a, Vec2 b);
Vec2 operator-(Vec2 a, Vec2 b);
Vec2 operator*(Vec2 a, Vec2 b);
Vec2 operator*(Vec2 a, float b);
Vec2 operator/(Vec2 a, Vec2 b);
Vec2 operator/(Vec2 a, float b);
Vec2& operator+=(Vec2& a, Vec2 b);
Vec2& operator-=(Vec2& a, Vec2 b);
Vec2& operator*=(Vec2& a, Vec2 b);
Vec2& operator*=(Vec2& a, float b);
Vec2& operator/=(Vec2& a, Vec2 b);
Vec2& operator/=(Vec2& a, float b);
bool operator==(Vec2 a, Vec2 b);
bool operator!=(Vec2 a, Vec2 b);

struct Vec3
{
    float x, y, z;

    Vec2 xy() const;
    Vec2 xz() const;
    Vec2 yz() const;

    float dot(Vec3 other) const;
    Vec3 cross(Vec3 other) const;
    float lengthsq() const;
    float length() const;
    Vec3 normalize() const;
};

#if 0
Vec2 vec3_xy(Vec3 v);
Vec2 vec3_xz(Vec3 v);
Vec2 vec3_yz(Vec3 v);
Vec3 vec3_negate(Vec3 v);
Vec3 vec3_add(Vec3 a, Vec3 b);
Vec3 vec3_sub(Vec3 a, Vec3 b);
Vec3 vec3_mult(Vec3 a, Vec3 b);
Vec3 vec3_multf(Vec3 a, float b);
Vec3 vec3_div(Vec3 a, Vec3 b);
Vec3 vec3_divf(Vec3 a, float b);
bool vec3_equals(Vec3 a, Vec3 b);
float vec3_lengthsq(Vec3 v);
float vec3_length(Vec3 v);
Vec3 vec3_normalize(Vec3 v);
float vec3_dot(Vec3 a, Vec3 b);
Vec3 vec3_cross(Vec3 a, Vec3 b);
#endif
Vec3 operator-(Vec3 v);
Vec3 operator+(Vec3 a, Vec3 b);
Vec3 operator-(Vec3 a, Vec3 b);
Vec3 operator*(Vec3 a, Vec3 b);
Vec3 operator*(Vec3 a, float b);
Vec3 operator/(Vec3 a, Vec3 b);
Vec3 operator/(Vec3 a, float b);
Vec3& operator+=(Vec3& a, Vec3 b);
Vec3& operator-=(Vec3& a, Vec3 b);
Vec3& operator*=(Vec3& a, Vec3 b);
Vec3& operator*=(Vec3& a, float b);
Vec3& operator/=(Vec3& a, Vec3 b);
Vec3& operator/=(Vec3& a, float b);
bool operator==(Vec3 a, Vec3 b);
bool operator!=(Vec3 a, Vec3 b);

union Vec4
{
    struct
    {
        float x, y, z, w;
    };
    struct
    {
        float r, g, b, a;
    };

    Vec3 xyz() const;
};

#if 0
Vec3 vec4_xyz(Vec4 v);
Vec4 vec4_multf(Vec4 a, float b);
#endif
Vec4 operator*(Vec4 a, float b);

using Color = Vec4;

Color color_red();
Color color_blue();
Color color_green();
Color color_white();
Color color_black();

union IntVec2
{
    struct
    {
        int x, y;
    };
    struct
    {
        int width, height;
    };
};

using WindowSize = IntVec2;

#if 0
IntVec2 intvec2_sub(IntVec2 a, IntVec2 b);
#endif
IntVec2 operator-(IntVec2 a, IntVec2 b);

union IntVec3
{
    struct
    {
        int x, y, z;
    };
    struct
    {
        int width, height, length;
    };
};

// Row major
struct Mat4
{
    float e[4][4];

    Mat4 transpose() const;
};

Mat4 mat4(float e00,
          float e01,
          float e02,
          float e03,
          float e10,
          float e11,
          float e12,
          float e13,
          float e20,
          float e21,
          float e22,
          float e23,
          float e30,
          float e31,
          float e32,
          float e33);
Mat4 mat4_diagonal(float v);
Mat4 mat4_identity();
Mat4 mat4_transpose(Mat4 m);
#if 0
Mat4 mat4_negate(Mat4 m);
Mat4 mat4_add(Mat4 a, Mat4 b);
Mat4 mat4_sub(Mat4 a, Mat4 b);
Mat4 mat4_mult(Mat4 a, Mat4 b);
Mat4 mat4_multf(Mat4 a, float b);
Vec4 mat4_mult_vec4(Mat4 a, Vec4 b);
#endif
Mat4 operator-(Mat4 m);
Mat4 operator+(Mat4 a, Mat4 b);
Mat4 operator-(Mat4 a, Mat4 b);
Mat4 operator*(Mat4 a, Mat4 b);
Mat4 operator*(Mat4 a, float b);
Vec4 operator*(Mat4 a, Vec4 b);

Mat4 mat4_ortho(float left,
                float right,
                float bottom,
                float top,
                float nearZ,
                float farZ);
Mat4 mat4_persp(float fov, float aspect_ratio, float near_z, float far_z);
Mat4 mat4_translate(Vec3 v);
Mat4 mat4_translate(float x, float y, float z);
Mat4 mat4_translate_vec3(Vec3 t);
Mat4 mat4_scale(float x, float y, float z);
Mat4 mat4_scale_vec3(Vec3 s);
bool mat4_inverse(Mat4 in, Mat4* out);
Mat4 mat4_rotate(float angle, Vec3 axis);
Mat4 mat4_lookat(Vec3 eye, Vec3 target, Vec3 up);

struct Quaternion
{
    union
    {
        Vec3 xyz;
        struct
        {
            float x, y, z;
        };
    };
    float w;
};

Quaternion quaternion_vec4(Vec4 v);
#if 0
Quaternion quaternion_add(Quaternion a, Quaternion b);
Quaternion quaternion_sub(Quaternion a, Quaternion b);
#endif
Quaternion operator+(Quaternion a, Quaternion b);
Quaternion operator-(Quaternion a, Quaternion b);

// origin of screen is left botton
Vec3 screen_to_world(IntVec2 screen_pos,
                     IntVec2 screen_size,
                     Mat4 proj,
                     Mat4 view);
Vec3 screen_to_world_ray(IntVec2 screen_pos,
                         IntVec2 screen_size,
                         Vec3 camera_pos,
                         Mat4 proj,
                         Mat4 view);
}

#endif // DSC_MATHEMATICS_H
