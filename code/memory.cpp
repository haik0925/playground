﻿#include "memory.h"
#include "common.h"
#include "debug.h"

namespace dsc
{
// Use a random number as a tag
constexpr uint temp_memory_block_tag = 0xe7d3ff63;

global_var struct
{
    ArenaAllocator arena;
    bool inited;
} g_temp_memory_context;

internal_func ArenaAllocator* temp_memory__get_arena()
{
    assert_(g_temp_memory_context.inited);
    return &g_temp_memory_context.arena;
}

void temp_memory_init(void* memory, u64 size)
{
    ArenaAllocator arena = {};
    arena.memory = memory;
    arena.size = size;
    g_temp_memory_context.arena = arena;
    g_temp_memory_context.inited = true;
}

void* temp_memory_alloc(u64 size)
{
    if (size <= 0)
        return NULL;

    ArenaAllocator* arena = temp_memory__get_arena();
    u8* block = (u8*)arena->push(size + sizeof(uint) + sizeof(u64));
    *(uint*)block = temp_memory_block_tag;
    block += sizeof(uint);
    *(u64*)block = size;
    block += sizeof(u64);
    void* result = block;
    return result;
}

void* temp_memory_realloc(void* p, u64 size)
{
    assert_(size > 0);

    ArenaAllocator* arena = temp_memory__get_arena();
    void* result;
    if (p == NULL)
        result = temp_memory_alloc(size);
    else
    {
        void* block = (u8*)p - (sizeof(u64) + sizeof(uint));
        i64 dist = (u8*)block - (u8*)arena->memory;
        assert_(dist >= 0 && (u64)dist < arena->used);
        uint* tag = (uint*)block;
        assert_(*tag == temp_memory_block_tag);
        u64* old_size = (u64*)((u8*)block + sizeof(uint));
        if (size <= *old_size)
            result = p;
        else if ((u8*)p + *old_size == (u8*)arena->memory + arena->used)
        {
            arena->push(size - *old_size);
            *old_size = size;
            result = p;
        }
        else
            result = temp_memory_alloc(size);
    }

    return result;
}

void temp_memory_flush()
{
    ArenaAllocator* arena = temp_memory__get_arena();
    arena->used = 0;
}

void pool_allocator_init(PoolAllocator* pa, int obj_size, int count, void* mem)
{
    pa->obj_size = obj_size;
    pa->count = 0;
    pa->max_count = count;
    pa->memory_ptr = mem;
    pa->free_list_head = (FreeList*)pa->memory_ptr;

    FreeList* free_list = pa->free_list_head;
    for (int i = 0; i < pa->max_count - 1; ++i)
    {
        free_list->next = (FreeList*)((u8*)free_list + pa->obj_size);
        free_list = free_list->next;
    }
    free_list->next = NULL;
}

bool pool_allocator_bound_check(const PoolAllocator* pa, void* ptr)
{
    bool result = true;
    uintptr_t test_address = (uintptr_t)ptr;
    uintptr_t min_address = (uintptr_t)pa->memory_ptr;
    uintptr_t max_address =
        (uintptr_t)pa->memory_ptr + pa->max_count * pa->obj_size;

    if (test_address < min_address && test_address > max_address)
        result = false;
    else if ((test_address - min_address) % pa->obj_size != 0)
        result = false;

    return result;
}

void* pool_allocator_alloc(PoolAllocator* pa)
{
    assert_(pa->free_list_head && "No more left memory space to allocate!");

    ++pa->count;
    void* obj = pa->free_list_head;
    pa->free_list_head = pa->free_list_head->next;
    memset(obj, 0, pa->obj_size);
    return obj;
}

void pool_allocator_free(PoolAllocator* pa, void* ptr)
{
    assert_(pool_allocator_bound_check(pa, ptr) &&
            "the received ptr param is out of bound");

    --pa->count;
    ((FreeList*)ptr)->next = pa->free_list_head;
    pa->free_list_head = (FreeList*)ptr;
}

void* ArenaAllocator::push(u64 size)
{
    assert_((used + size) <= this->size);
    void* result = (u8*)memory + used;
    used += size;
    return result;
}

void* ArenaAllocator::get_free_memory_head() const
{
    return (u8*)memory + used;
}

u64 ArenaAllocator::get_free_memory_size() const
{
    assert_(used <= size); // prevent overflow
    return size - used;
}

ArenaAllocator
    ArenaAllocator::create_sub_arena(u64 reserved_sub_arena_memory_size)
{
    void* memory = push(reserved_sub_arena_memory_size);
    ArenaAllocator sub_arena = {};
    sub_arena.memory = memory;
    sub_arena.size = reserved_sub_arena_memory_size;
    return sub_arena;
}

PoolAllocator::PoolAllocator(int obj_size, int count, void* mem)
{
    pool_allocator_init(this, obj_size, count, mem);
}

bool PoolAllocator::bound_check(void* ptr) const
{
    return pool_allocator_bound_check(this, ptr);
}

void* PoolAllocator::alloc_()
{
    return pool_allocator_alloc(this);
}

void PoolAllocator::free(void* ptr)
{
    return pool_allocator_free(this, ptr);
}

}
