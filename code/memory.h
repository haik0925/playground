﻿#ifndef DSC_MEMORY_H
#define DSC_MEMORY_H

#include "primitive_types.h"
#include "debug.h"

namespace dsc
{
struct Allocator
{
    virtual ~Allocator() = default;
    virtual void* allocate(size_t size) = 0;
    virtual void deallocate(void* ptr) = 0;

    template<typename T>
    T* allocate_array(size_t count)
    {
        return (T*)allocate(sizeof(T) * count);
    }
};

struct GlobalHeapAllocator : Allocator
{
    void* allocate(size_t size) override
    {
        return malloc(size);
    }
    void deallocate(void* ptr) override
    {
        free(ptr);
    }

    static GlobalHeapAllocator& get()
    {
        static GlobalHeapAllocator instance;
        return instance;
    }
};

struct ArenaAllocator : Allocator
{
    void* memory;
    u64 size;
    u64 used;

    void* push(u64 size);
    template<typename T>
    T* push()
    {
        return (T*)push(sizeof(T));
    }
    template<typename T>
    T* push_array(int count)
    {
        return (T*)push(sizeof(T) * count);
    }
    void* get_free_memory_head() const;
    u64 get_free_memory_size() const;
    ArenaAllocator create_sub_arena(u64 reserved_sub_arena_memory_size);

    ~ArenaAllocator() override = default;
    void* allocate(size_t size) override
    {
        return push(size);
    }
    void deallocate(void* ptr) override
    {
    }
};

void temp_memory_init(void* memory, u64 size);
void* temp_memory_alloc(u64 size);
void* temp_memory_realloc(void* p, u64 size);
void temp_memory_flush();

struct FreeList
{
    FreeList* next;
};

struct PoolAllocator : Allocator
{
    int obj_size = 0;
    i64 count = 0;
    i64 max_count = 0;
    FreeList* free_list_head = nullptr;
    void* memory_ptr = nullptr;

    PoolAllocator() = default;
    PoolAllocator(int obj_size, int count, void* mem);
    bool bound_check(void* ptr) const;
    void* alloc_();
    template<typename T>
    T* alloc()
    {
        assert_((int)sizeof(T) == obj_size);
        return (T*)alloc_();
    }
    void free(void* ptr);

    ~PoolAllocator() override = default;
    void* allocate(size_t size) override
    {
        assert_(size == obj_size);
        return alloc_();
    }
    void deallocate(void* ptr) override
    {
        free(ptr);
    }
};

#if 0
void pool_allocator_init(PoolAllocator* pa, int obj_size, int count, void* mem);
bool pool_allocator_bound_check(PoolAllocator* pa, void* ptr);
void* pool_allocator_alloc(PoolAllocator* pa);
void pool_allocator_free(PoolAllocator* pa, void* ptr);
#endif
}

#endif // DSC_MEMORY_H
