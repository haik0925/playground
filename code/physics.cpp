﻿#include "physics.h"
#include "debug.h"
#include "common.h"
#include "shared.h"
#include "game_scene.h"
#include <math.h>

#if 0
void* b3Alloc(u32 size)
{
    return malloc(size);
}
void b3Free(void* block)
{
    free(block);
}

void b3Log(const char* string, ...)
{
    char buffer[1000];
    va_list args;
    va_start(args, string);
    vsprintf(buffer, string, args);
    va_end(args);
    log_(buffer);
}
#endif

void b3BeginProfileScope(const char* name)
{
}
void b3EndProfileScope()
{
}

namespace dsc
{
bool physics_collision_check_aabb_aabb(Vec3 a_pos,
                                       Vec3 a_extent,
                                       Vec3 b_pos,
                                       Vec3 b_extent)
{
    if (a_pos.x + a_extent.x < b_pos.x - b_extent.x ||
        a_pos.x - a_extent.x > b_pos.x + b_extent.x)
        return false;

    if (a_pos.y + a_extent.y < b_pos.y - b_extent.y ||
        a_pos.y - a_extent.y > b_pos.y + b_extent.y)
        return false;

    if (a_pos.z + a_extent.z < b_pos.z - b_extent.z ||
        a_pos.z - a_extent.z > b_pos.z + b_extent.z)
        return false;

    return true;
}

Vec3 physics_collision_resolution_aabb_aabb(Vec3 a_pos,
                                            Vec3 a_extent,
                                            Vec3 b_pos,
                                            Vec3 b_extent)
{
    Vec3 a_closest_vertex = a_pos;
    Vec3 b_closest_vertex = b_pos;

    // find closest points
    if (a_pos.x < b_pos.x)
    {
        a_closest_vertex.x += a_extent.x;
        b_closest_vertex.x -= b_extent.x;
    }
    else
    {
        a_closest_vertex.x -= a_extent.x;
        b_closest_vertex.x += b_extent.x;
    }

    if (a_pos.y < b_pos.y)
    {
        a_closest_vertex.y += a_extent.y;
        b_closest_vertex.y -= b_extent.y;
    }
    else
    {
        a_closest_vertex.y -= a_extent.y;
        b_closest_vertex.y += b_extent.y;
    }

    if (a_pos.z < b_pos.z)
    {
        a_closest_vertex.z += a_extent.z;
        b_closest_vertex.z -= b_extent.z;
    }
    else
    {
        a_closest_vertex.z -= a_extent.z;
        b_closest_vertex.z += b_extent.z;
    }

    // find required force per face
    Vec3 delta = b_closest_vertex - a_closest_vertex;
    float x = delta.x < 0 ? -delta.x : delta.x;
    float y = delta.y < 0 ? -delta.y : delta.y;
    float z = delta.z < 0 ? -delta.z : delta.z;

    if (x < y && x < z)
        delta.y = delta.z = 0;
    else if (y < x && y < z)
        delta.x = delta.z = 0;
    else
        delta.x = delta.y = 0;

    return delta;
}

bool physics_collision_check_ray_plane(Vec3 ray_pos,
                                       Vec3 ray_dir,
                                       float plane_dist,
                                       Vec3 plane_dir,
                                       float* collision_point_dist)
{
    *collision_point_dist =
        (plane_dist - plane_dir.dot(ray_pos)) / plane_dir.dot(ray_dir);
    if (*collision_point_dist < 0 || *collision_point_dist > float_max)
        return false;

    return true;
}

bool physics_collision_check_ray_aabb(Vec3 ray_pos,
                                      Vec3 ray_dir,
                                      Vec3 aabb_pos,
                                      Vec3 aabb_extent,
                                      float* collision_point_dist)
{
    Vec3 min, max;

    physics_collision_check_ray_plane(
        ray_pos, ray_dir, aabb_pos.x - aabb_extent.x, {1, 0, 0}, &min.x);
    physics_collision_check_ray_plane(
        ray_pos, ray_dir, aabb_pos.y - aabb_extent.y, {0, 1, 0}, &min.y);
    physics_collision_check_ray_plane(
        ray_pos, ray_dir, aabb_pos.z - aabb_extent.z, {0, 0, 1}, &min.z);
    physics_collision_check_ray_plane(
        ray_pos, ray_dir, aabb_pos.x + aabb_extent.x, {1, 0, 0}, &max.x);
    physics_collision_check_ray_plane(
        ray_pos, ray_dir, aabb_pos.y + aabb_extent.y, {0, 1, 0}, &max.y);
    physics_collision_check_ray_plane(
        ray_pos, ray_dir, aabb_pos.z + aabb_extent.z, {0, 0, 1}, &max.z);

    if (min.x > max.x)
        swap(min.x, max.x);
    if (min.y > max.y)
        swap(min.y, max.y);
    if (min.z > max.z)
        swap(min.z, max.z);

    // use x spot as a temporary min, max representation
    min.x = fmaxf(fmaxf(min.x, min.y), min.z);
    max.x = fminf(fminf(max.x, max.y), max.z);

    // no intersection, not colliding
    if (max.x < min.x)
        return false;
    if (min.x < 0)
        return false;

    *collision_point_dist = min.x;
    return true;
}

void physics_update(Vec3* player_pos,
                    Vec3* player_vel,
                    bool* can_player_jump,
                    AssetStorage* asset_storage,
                    AudioContext* audio,
                    Room* rooms,
                    Door* doors,
                    Enemy* enemy_buffer,
                    int enemy_capacity,
                    int* enemy_count,
                    Bullet* bullet_buffer,
                    int bullet_capacity,
                    int* bullet_count)
{
    const float resolution_force_multiplier = 12;

    // resolve if player collides.
    {
        const Vec3 player_collider_extent = {1, 4, 1};
        const float player_on_ground_epsilon = 0.1f;

        // rooms
        for (int i = 0; i < max_room_count; ++i)
        {
            RoomTemplate* room_template = rooms[i].room_template;

            // check only rooms that player is in
            if (!physics_collision_check_aabb_aabb(
                    rooms[i].pos + room_template->room_pos,
                    room_template->room_size * 0.5f, *player_pos,
                    player_collider_extent))
                continue;

            // check statics
            for (int j = 0; j < room_template->static_entity_count; ++j)
            {
                if (physics_collision_check_aabb_aabb(
                        rooms[i].pos + room_template->static_entities[j].pos,
                        room_template->static_entities[j].size * 0.5f,
                        *player_pos, player_collider_extent))
                {
                    Vec3 force = physics_collision_resolution_aabb_aabb(
                        rooms[i].pos + room_template->static_entities[j].pos,
                        room_template->static_entities[j].size * 0.5f,
                        *player_pos, player_collider_extent);

                    *player_pos -= force;
                    if (force.y < -player_on_ground_epsilon)
                        *can_player_jump = true;
                    *player_vel -= force * resolution_force_multiplier;
                }
            }

            // check boxes
            for (int j = 0; j < rooms[i].box_count; ++j)
            {
                if (physics_collision_check_aabb_aabb(
                        rooms[i].pos + rooms[i].boxes[j].entity.pos,
                        rooms[i].boxes[j].entity.size * 0.5f, *player_pos,
                        player_collider_extent))
                {
                    Vec3 force = physics_collision_resolution_aabb_aabb(
                        rooms[i].pos + rooms[i].boxes[j].entity.pos,
                        rooms[i].boxes[j].entity.size * 0.5f, *player_pos,
                        player_collider_extent);

                    *player_pos -= force;
                    if (force.y < -player_on_ground_epsilon)
                        *can_player_jump = true;
                    *player_vel -= force * resolution_force_multiplier;
                }
            }
        }

        // all doors
        for (int i = 0; i < max_door_count; ++i)
        {
            if (physics_collision_check_aabb_aabb(
                    doors[i].entity.pos, doors[i].entity.size * 0.5f,
                    *player_pos, player_collider_extent))
            {
                Vec3 force = physics_collision_resolution_aabb_aabb(
                    doors[i].entity.pos, doors[i].entity.size * 0.5f,
                    *player_pos, player_collider_extent);

                *player_pos -= force;
                if (force.y < -player_on_ground_epsilon)
                    *can_player_jump = true;
                *player_vel -= force * resolution_force_multiplier;
            }
        }
    }

    // resolve if enemy collides.
    for (int enemy_index = 0; enemy_index < enemy_capacity; ++enemy_index)
    {
        Enemy* enemy = &enemy_buffer[enemy_index];

        if (enemy->is_in_use)
        {
            for (int room_index = 0; room_index < max_room_count; ++room_index)
            {
                Room* room = &rooms[room_index];
                RoomTemplate* room_template = room->room_template;
                Vec3 enemy_extent = enemy->entity->size * 0.5f;

                // check only rooms that enemy is in
                if (!physics_collision_check_aabb_aabb(
                        room->pos + room_template->room_pos,
                        room_template->room_size * 0.5f, enemy->entity->pos,
                        enemy_extent))
                    continue;

                // static entities
                for (int entity_index = 0;
                     entity_index < room_template->static_entity_count;
                     ++entity_index)
                {
                    Entity* entity =
                        &room_template->static_entities[entity_index];

                    if (physics_collision_check_aabb_aabb(
                            room->pos + entity->pos, entity->size * 0.5f,
                            enemy->entity->pos, enemy_extent))
                    {
                        Vec3 force = physics_collision_resolution_aabb_aabb(
                            room->pos + entity->pos, entity->size * 0.5f,
                            enemy->entity->pos, enemy_extent);

                        enemy->entity->pos -= force;
                        enemy->vel -= force * resolution_force_multiplier;
                    }
                }

                // boxes
                for (int box_index = 0; box_index < room->box_count;
                     ++box_index)
                {
                    if (physics_collision_check_aabb_aabb(
                            room->pos + room->boxes[box_index].entity.pos,
                            room->boxes[box_index].entity.size * 0.5f,
                            enemy->entity->pos, enemy_extent))
                    {
                        Vec3 force = physics_collision_resolution_aabb_aabb(
                            room->pos + room->boxes[box_index].entity.pos,
                            room->boxes[box_index].entity.size * 0.5f,
                            enemy->entity->pos, enemy_extent);

                        enemy->entity->pos -= force;
                        enemy->vel -= force * resolution_force_multiplier;
                    }
                }
            }

            // doors
            for (int i = 0; i < max_door_count; ++i)
            {
                if (physics_collision_check_aabb_aabb(
                        doors[i].entity.pos, doors[i].entity.size * 0.5f,
                        enemy->entity->pos, enemy->entity->size * 0.5f))
                {
                    Vec3 force = physics_collision_resolution_aabb_aabb(
                        doors[i].entity.pos, doors[i].entity.size * 0.5f,
                        enemy->entity->pos, enemy->entity->size * 0.5f);

                    enemy->entity->pos -= force;
                    enemy->vel = force * resolution_force_multiplier;
                }
            }
        }
    }

    // remove if bullet collides.
    for (int bullet_index = 0; bullet_index < bullet_capacity; ++bullet_index)
    {
        Bullet* bullet = &bullet_buffer[bullet_index];

        if (bullet->is_in_use)
        {
            Vec3 bullet_extent = bullet->entity->size * 0.5f;

            // room
            for (int room_index = 0; room_index < max_room_count; ++room_index)
            {
                Room* room = &rooms[room_index];
                RoomTemplate* room_template = room->room_template;

                // check only rooms that bullet is in
                if (!physics_collision_check_aabb_aabb(
                        room->pos + room_template->room_pos,
                        room_template->room_size * 0.5f, bullet->entity->pos,
                        bullet_extent))
                    continue;

                // boxes
                for (int j = 0; j < room->box_count; ++j)
                {
                    Entity* box = &room->boxes[j].entity;

                    if (physics_collision_check_aabb_aabb(
                            room->pos + box->pos, box->size * 0.5f,
                            bullet->entity->pos, bullet_extent))
                    {
                        --(*bullet_count);
                        bullet->is_in_use = false;

                        // box hit
                        if (--room->boxes[j].hp > 0)
                        {
                            audio->play_sound(
                                &asset_storage->_sfxs[SfxType::BoxHit]);
                            break;
                        }

                        // remove if hp is zero
                        audio->play_sound(
                            &asset_storage->_sfxs[SfxType::BoxBreak]);
                        room->boxes[j] = room->boxes[--room->box_count];
                        break;
                    }
                }

                // static entities
                for (int j = 0; j < room_template->static_entity_count; ++j)
                {
                    Entity* entity = &room_template->static_entities[j];

                    if (physics_collision_check_aabb_aabb(
                            room->pos + entity->pos, entity->size * 0.5f,
                            bullet->entity->pos, bullet_extent))
                    {
                        --(*bullet_count);
                        bullet->is_in_use = false;
                        break;
                    }
                }
            }

            // doors
            for (int i = 0; i < max_door_count; ++i)
            {
                if (physics_collision_check_aabb_aabb(
                        doors[i].entity.pos, doors[i].entity.size * 0.5f,
                        bullet->entity->pos, bullet->entity->size * 0.5f))
                {
                    --(*bullet_count);
                    bullet->is_in_use = false;
                    break;
                }
            }

            // enemy
            for (int enemy_index = 0; enemy_index < enemy_capacity;
                 ++enemy_index)
            {
                Enemy* enemy = &enemy_buffer[enemy_index];

                if (enemy->is_in_use &&
                    physics_collision_check_aabb_aabb(
                        enemy->entity->pos, enemy->entity->size * 0.5f,
                        bullet->entity->pos, bullet_extent))
                {
                    --(*bullet_count);
                    bullet->is_in_use = false;

                    // enemy life --;
                    --enemy->life;
                    if (enemy->life <= 0)
                    {
                        --(*enemy_count);
                        enemy->is_in_use = false;
                        continue;
                    }

                    break;
                }
            }
        }
    }
}

PhysicsDebugDrawModel::PhysicsDebugDrawModel(const AssetStorage* assets)
    : shader(&assets->_shaders[ShaderType::DebugPhysics])
    , segment_vertices(max_vertex_count)
    , segments_vo(nullptr,
                  max_vertex_count * sizeof(DebugVertex),
                  sizeof(DebugVertex),
                  s_attribs,
                  ssize32(s_attribs))
{
}

void PhysicsDebugDrawModel::push_segment(Vec3 p1, Vec3 p2, Vec4 color)
{
    segment_vertices.push_back({p1, color});
    segment_vertices.push_back({p2, color});
}

void PhysicsDebugDrawModel::render()
{
    segments_vo.update_buffer(segment_vertices.data(),
                              segment_vertices.ssize32() * sizeof(DebugVertex));

    shader->bind();
    segments_vo.bind();
    glDrawArrays(GL_LINES, 0, segment_vertices.ssize32());
    segments_vo.unbind();
    shader->unbind();

    segment_vertices.clear();
}

void PhysicsDebugDrawController::DrawPoint(const b3Vec3& p,
                                           float32 size,
                                           const b3Color& color)
{
    assert_(!"Need to implement");
}

void PhysicsDebugDrawController::DrawSegment(const b3Vec3& p1,
                                             const b3Vec3& p2,
                                             const b3Color& color)
{
    model->push_segment({p1.x, p1.y, p1.z}, {p2.x, p2.y, p2.z},
                        {color.r, color.g, color.b, 1});
}

void PhysicsDebugDrawController::DrawTriangle(const b3Vec3& p1,
                                              const b3Vec3& p2,
                                              const b3Vec3& p3,
                                              const b3Color& color)
{
    assert_(!"Need to implement");
}

void PhysicsDebugDrawController::DrawSolidTriangle(const b3Vec3& normal,
                                                   const b3Vec3& p1,
                                                   const b3Vec3& p2,
                                                   const b3Vec3& p3,
                                                   const b3Color& color)
{
    assert_(!"Need to implement");
}

void PhysicsDebugDrawController::DrawPolygon(const b3Vec3* vertices,
                                             u32 count,
                                             const b3Color& color)
{
    assert_(!"Need to implement");
}

void PhysicsDebugDrawController::DrawSolidPolygon(const b3Vec3& normal,
                                                  const b3Vec3* vertices,
                                                  u32 count,
                                                  const b3Color& color)
{
    assert_(!"Need to implement");
}

void PhysicsDebugDrawController::DrawCircle(const b3Vec3& normal,
                                            const b3Vec3& center,
                                            float32 radius,
                                            const b3Color& color)
{
    assert_(!"Need to implement");
}

void PhysicsDebugDrawController::DrawSolidCircle(const b3Vec3& normal,
                                                 const b3Vec3& center,
                                                 float32 radius,
                                                 const b3Color& color)
{
    assert_(!"Need to implement");
}

void PhysicsDebugDrawController::DrawSphere(const b3Vec3& center,
                                            float32 radius,
                                            const b3Color& color)
{
    assert_(!"Need to implement");
}

void PhysicsDebugDrawController::DrawSolidSphere(const b3Vec3& center,
                                                 float32 radius,
                                                 const b3Color& color)
{
    assert_(!"Need to implement");
}

void PhysicsDebugDrawController::DrawCapsule(const b3Vec3& p1,
                                             const b3Vec3& p2,
                                             float32 radius,
                                             const b3Color& color)
{
    assert_(!"Need to implement");
}

void PhysicsDebugDrawController::DrawSolidCapsule(const b3Vec3& p1,
                                                  const b3Vec3& p2,
                                                  float32 radius,
                                                  const b3Color& color)
{
    assert_(!"Need to implement");
}

void PhysicsDebugDrawController::DrawAABB(const b3AABB3& aabb,
                                          const b3Color& color)
{
    assert_(!"Need to implement");
}

void PhysicsDebugDrawController::DrawTransform(const b3Transform& xf)
{
    assert_(!"Need to implement");
}

}
