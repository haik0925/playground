﻿#ifndef DSC_PHYSICS_H
#define DSC_PHYSICS_H

#include "mathematics.h"
#include "renderer.h"
#include "containers.h"
#include "bounce/bounce.h"

constexpr float float_max = 3.402823466e+38F;

namespace dsc
{
bool physics_collision_check_aabb_aabb(Vec3 a_pos,
                                       Vec3 a_extent,
                                       Vec3 b_pos,
                                       Vec3 b_extent);
Vec3 physics_collision_resolution_aabb_aabb(Vec3 a_pos,
                                            Vec3 a_extent,
                                            Vec3 b_pos,
                                            Vec3 b_extent);

bool physics_collision_check_ray_plane(Vec3 ray_pos,
                                       Vec3 ray_dir,
                                       float plane_dist,
                                       Vec3 plane_dir,
                                       float* collision_point_dist);
bool physics_collision_check_ray_aabb(Vec3 ray_pos,
                                      Vec3 ray_dir,
                                      Vec3 aabb_pos,
                                      Vec3 aabb_extent,
                                      float* collision_point_dist);
struct Vec3;
struct Room;
struct Enemy;
struct Bullet;
struct AssetStorage;
struct AudioContext;
struct Door;

void physics_update(Vec3* player_pos,
                    Vec3* player_vel,
                    bool* can_player_jump,
                    AssetStorage* asset_storage,
                    AudioContext* audio,
                    Room* rooms,
                    Door* doors,
                    Enemy* enemy_buffer,
                    int enemy_capacity,
                    int* enemy_count,
                    Bullet* bullet_buffer,
                    int bullet_capacity,
                    int* bullet_count);

struct PhysicsDebugDrawModel
{
    struct DebugVertex
    {
        Vec3 pos;
        Vec4 color;
    };

    const Shader* shader = nullptr;
    // TODO(illkwon): implement static_vector and apply.
    static_vector<DebugVertex> segment_vertices;
    VertexObject segments_vo;

    static constexpr int max_vertex_count = 1000;

    static inline VertexLayoutAttrib s_attribs[] = {
        {VertexUsage::Position, VertexComponentType::Float, 3,
         offsetof(DebugVertex, pos)},
        {VertexUsage::Color, VertexComponentType::Float, 4,
         offsetof(DebugVertex, color)},
    };

    explicit PhysicsDebugDrawModel(const AssetStorage* assets);
    void push_segment(Vec3 p1, Vec3 p2, Vec4 color);
    void render();
};

struct PhysicsDebugDrawController : public b3Draw
{
    PhysicsDebugDrawModel* model;

    void
        DrawPoint(const b3Vec3& p, float32 size, const b3Color& color) override;
    void DrawSegment(const b3Vec3& p1,
                     const b3Vec3& p2,
                     const b3Color& color) override;
    void DrawTriangle(const b3Vec3& p1,
                      const b3Vec3& p2,
                      const b3Vec3& p3,
                      const b3Color& color) override;
    void DrawSolidTriangle(const b3Vec3& normal,
                           const b3Vec3& p1,
                           const b3Vec3& p2,
                           const b3Vec3& p3,
                           const b3Color& color) override;
    void DrawPolygon(const b3Vec3* vertices,
                     u32 count,
                     const b3Color& color) override;
    void DrawSolidPolygon(const b3Vec3& normal,
                          const b3Vec3* vertices,
                          u32 count,
                          const b3Color& color) override;
    void DrawCircle(const b3Vec3& normal,
                    const b3Vec3& center,
                    float32 radius,
                    const b3Color& color) override;
    void DrawSolidCircle(const b3Vec3& normal,
                         const b3Vec3& center,
                         float32 radius,
                         const b3Color& color) override;
    void DrawSphere(const b3Vec3& center,
                    float32 radius,
                    const b3Color& color) override;
    void DrawSolidSphere(const b3Vec3& center,
                         float32 radius,
                         const b3Color& color) override;
    void DrawCapsule(const b3Vec3& p1,
                     const b3Vec3& p2,
                     float32 radius,
                     const b3Color& color) override;
    void DrawSolidCapsule(const b3Vec3& p1,
                          const b3Vec3& p2,
                          float32 radius,
                          const b3Color& color) override;
    void DrawAABB(const b3AABB3& aabb, const b3Color& color) override;
    void DrawTransform(const b3Transform& xf) override;
};

}
#endif // DSC_PHYSICS_H
