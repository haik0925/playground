#include "renderer.h"
#include "memory.h"

namespace dsc
{
Shader Shader::create(const char* vs_src, const char* fs_src)
{
    Shader result;
    result.vs = glCreateShaderProgramv(GL_VERTEX_SHADER, 1, &vs_src);
    char buf[500] = {0};
    int tmp = 0;
    glGetProgramInfoLog(result.vs, sizeof(buf), &tmp, buf);
    if (*buf)
        log_(buf);
    result.fs = glCreateShaderProgramv(GL_FRAGMENT_SHADER, 1, &fs_src);
    glGetProgramInfoLog(result.fs, sizeof(buf), &tmp, buf);
    if (*buf)
        log_(buf);
    glCreateProgramPipelines(1, &result.program);
    glUseProgramStages(result.program, GL_VERTEX_SHADER_BIT, result.vs);
    glUseProgramStages(result.program, GL_FRAGMENT_SHADER_BIT, result.fs);

    return result;
}

Shader Shader::create_from_file(const char* vs_filename,
                                const char* fs_filename)
{
    // TODO: Replace file io code to proper asset loading code
    u64 buf_size = 2_mb;
    void* buf = temp_memory_alloc(buf_size);
    memset(buf, 0, buf_size);
    char* vs_src = (char*)buf;
    char* fs_src = (char*)((u8*)buf + buf_size / 2);
    debug_read_all_binary_file(vs_filename, vs_src, buf_size / 2);
    debug_read_all_binary_file(fs_filename, fs_src, buf_size / 2);
    Shader result = Shader::create(vs_src, fs_src);
    return result;
}

void Shader::bind() const
{
    glBindProgramPipeline(program);
}

void Shader::unbind() const
{
    glBindProgramPipeline(0);
}

Texture texture_load(u8* pixels,
                     int width,
                     int height,
                     GLint wrap_u,
                     GLint wrap_v,
                     GLint min_filter,
                     GLint mag_filter,
                     GLenum internal_format,
                     GLenum pixel_format,
                     GLenum pixel_type)
{
    Texture texture;
    glCreateTextures(GL_TEXTURE_2D, 1, &texture);
    glTextureParameteri(texture, GL_TEXTURE_WRAP_S, wrap_u);
    glTextureParameteri(texture, GL_TEXTURE_WRAP_T, wrap_v);
    glTextureParameteri(texture, GL_TEXTURE_MIN_FILTER, min_filter);
    glTextureParameteri(texture, GL_TEXTURE_MAG_FILTER, mag_filter);
    glTextureStorage2D(texture, 1, internal_format, width, height);
    glTextureSubImage2D(texture, 0, 0, 0, width, height, pixel_format,
                        pixel_type, pixels);

    return texture;
}

Mesh Mesh::create_from_obj_file(const char* filename,
                                ArenaAllocator* mesh_arena)
{
    u64 file_size = debug_get_file_size(filename);
    void* temp_file_data = temp_memory_alloc(file_size);
    u64 read_size =
        debug_read_all_binary_file(filename, temp_file_data, file_size);
    assert_(file_size == read_size);
    TempObjInfo temp_obj_info =
        debug_load_temp_obj_from_raw_data(temp_file_data, file_size);
    return Mesh::create_from_temp_obj_info(&temp_obj_info, mesh_arena);
}

Mesh Mesh::create_from_temp_obj_info(TempObjInfo* temp_obj_info,
                                     ArenaAllocator* mesh_arena)
{
    Mesh result;

    assert_(temp_obj_info->attrib.num_faces /
                    temp_obj_info->attrib.num_face_num_verts ==
                3 &&
            temp_obj_info->attrib.num_faces %
                    temp_obj_info->attrib.num_face_num_verts ==
                0 &&
            "ALL POLYGONS MUST BE TRIANGLE");

    result.vertex_count = temp_obj_info->attrib.num_faces;
    result.vertices = mesh_arena->push_array<Vertex>(result.vertex_count);
    float* obj_vertices = temp_obj_info->attrib.vertices;
    int vertex_index = 0;
    for (size_t face_index = 0; face_index < temp_obj_info->attrib.num_faces;
         face_index += 3)
    {
        tinyobj_vertex_index_t* face = &temp_obj_info->attrib.faces[face_index];
        Vertex v[3] = {0};
        for (int i = 0; i < 3; ++i)
        {
            int obj_vertex_index = face[i].v_idx * 3;

            v[i].pos = Vec3{
                obj_vertices[obj_vertex_index],
                obj_vertices[obj_vertex_index + 1],
                obj_vertices[obj_vertex_index + 2],
            };
        }
        Vec3 normal =
            (v[1].pos - v[0].pos).cross(v[2].pos - v[0].pos).normalize();

        for (int i = 0; i < 3; ++i)
            v[i].normal = normal;

        // TODO(illkwon): Remove the code assigning temp value to uv after
        // fixing the gray line issue;
        v[0].uv = Vec2{0, 0};
        v[1].uv = Vec2{1, 1};
        v[2].uv = Vec2{1, 1};

        memcpy(&result.vertices[vertex_index], v, sizeof(v));
        vertex_index += 3;
    }
    assert_(vertex_index == result.vertex_count);

    return result;
}

Mesh Mesh::create_cube(ArenaAllocator* mesh_arena)
{
    Mesh result;

    Vertex vertices[] = {
        // Back
        {Vec3{0.5f, -0.5f, -0.5f}, Vec2{0.0f, 0.0f}, Vec3{0.0f, 0.0f, -1.0f}},
        {Vec3{-0.5f, -0.5f, -0.5f}, Vec2{1.0f, 0.0f}, Vec3{0.0f, 0.0f, -1.0f}},
        {Vec3{-0.5f, 0.5f, -0.5f}, Vec2{1.0f, 1.0f}, Vec3{0.0f, 0.0f, -1.0f}},
        {Vec3{-0.5f, 0.5f, -0.5f}, Vec2{1.0f, 1.0f}, Vec3{0.0f, 0.0f, -1.0f}},
        {Vec3{0.5f, 0.5f, -0.5f}, Vec2{0.0f, 1.0f}, Vec3{0.0f, 0.0f, -1.0f}},
        {Vec3{0.5f, -0.5f, -0.5f}, Vec2{0.0f, 0.0f}, Vec3{0.0f, 0.0f, -1.0f}},

        // Front
        {Vec3{-0.5f, -0.5f, 0.5f}, Vec2{0.0f, 0.0f}, Vec3{0.0f, 0.0f, 1.0f}},
        {Vec3{0.5f, -0.5f, 0.5f}, Vec2{1.0f, 0.0f}, Vec3{0.0f, 0.0f, 1.0f}},
        {Vec3{0.5f, 0.5f, 0.5f}, Vec2{1.0f, 1.0f}, Vec3{0.0f, 0.0f, 1.0f}},
        {Vec3{0.5f, 0.5f, 0.5f}, Vec2{1.0f, 1.0f}, Vec3{0.0f, 0.0f, 1.0f}},
        {Vec3{-0.5f, 0.5f, 0.5f}, Vec2{0.0f, 1.0f}, Vec3{0.0f, 0.0f, 1.0f}},
        {Vec3{-0.5f, -0.5f, 0.5f}, Vec2{0.0f, 0.0f}, Vec3{0.0f, 0.0f, 1.0f}},

        // Left
        {Vec3{-0.5f, 0.5f, -0.5f}, Vec2{0.0f, 1.0f}, Vec3{-1.0f, 0.0f, 0.0f}},
        {Vec3{-0.5f, -0.5f, -0.5f}, Vec2{0.0f, 0.0f}, Vec3{-1.0f, 0.0f, 0.0f}},
        {Vec3{-0.5f, -0.5f, 0.5f}, Vec2{1.0f, 0.0f}, Vec3{-1.0f, 0.0f, 0.0f}},
        {Vec3{-0.5f, -0.5f, 0.5f}, Vec2{1.0f, 0.0f}, Vec3{-1.0f, 0.0f, 0.0f}},
        {Vec3{-0.5f, 0.5f, 0.5f}, Vec2{1.0f, 1.0f}, Vec3{-1.0f, 0.0f, 0.0f}},
        {Vec3{-0.5f, 0.5f, -0.5f}, Vec2{0.0f, 1.0f}, Vec3{-1.0f, 0.0f, 0.0f}},

        // Right
        {Vec3{0.5f, -0.5f, -0.5f}, Vec2{1.0f, 0.0f}, Vec3{1.0f, 0.0f, 0.0f}},
        {Vec3{0.5f, 0.5f, -0.5f}, Vec2{1.0f, 1.0f}, Vec3{1.0f, 0.0f, 0.0f}},
        {Vec3{0.5f, 0.5f, 0.5f}, Vec2{0.0f, 1.0f}, Vec3{1.0f, 0.0f, 0.0f}},
        {Vec3{0.5f, 0.5f, 0.5f}, Vec2{0.0f, 1.0f}, Vec3{1.0f, 0.0f, 0.0f}},
        {Vec3{0.5f, -0.5f, 0.5f}, Vec2{0.0f, 0.0f}, Vec3{1.0f, 0.0f, 0.0f}},
        {Vec3{0.5f, -0.5f, -0.5f}, Vec2{1.0f, 0.0f}, Vec3{1.0f, 0.0f, 0.0f}},

        // Bottom
        {Vec3{-0.5f, -0.5f, -0.5f}, Vec2{0.0f, 0.0f}, Vec3{0.0f, -1.0f, 0.0f}},
        {Vec3{0.5f, -0.5f, -0.5f}, Vec2{1.0f, 0.0f}, Vec3{0.0f, -1.0f, 0.0f}},
        {Vec3{0.5f, -0.5f, 0.5f}, Vec2{1.0f, 1.0f}, Vec3{0.0f, -1.0f, 0.0f}},
        {Vec3{0.5f, -0.5f, 0.5f}, Vec2{1.0f, 1.0f}, Vec3{0.0f, -1.0f, 0.0f}},
        {Vec3{-0.5f, -0.5f, 0.5f}, Vec2{0.0f, 1.0f}, Vec3{0.0f, -1.0f, 0.0f}},
        {Vec3{-0.5f, -0.5f, -0.5f}, Vec2{0.0f, 0.0f}, Vec3{0.0f, -1.0f, 0.0f}},

        // Top
        {Vec3{0.5f, 0.5f, -0.5f}, Vec2{1.0f, 1.0f}, Vec3{0.0f, 1.0f, 0.0f}},
        {Vec3{-0.5f, 0.5f, -0.5f}, Vec2{0.0f, 1.0f}, Vec3{0.0f, 1.0f, 0.0f}},
        {Vec3{-0.5f, 0.5f, 0.5f}, Vec2{0.0f, 0.0f}, Vec3{0.0f, 1.0f, 0.0f}},
        {Vec3{-0.5f, 0.5f, 0.5f}, Vec2{0.0f, 0.0f}, Vec3{0.0f, 1.0f, 0.0f}},
        {Vec3{0.5f, 0.5f, 0.5f}, Vec2{1.0f, 0.0f}, Vec3{0.0f, 1.0f, 0.0f}},
        {Vec3{0.5f, 0.5f, -0.5f}, Vec2{1.0f, 1.0f}, Vec3{0.0f, 1.0f, 0.0f}},
    };
    result.vertices = mesh_arena->push_array<Vertex>(ssize32(vertices));
    memcpy(result.vertices, vertices, sizeof(vertices));
    result.vertex_count = ssize32(vertices);

    return result;
}

Mesh Mesh::create_plane(ArenaAllocator* mesh_arena)
{
    Mesh result;

    Vertex vertices[] = {
        {{0.5f, 0.5f, -0.5f}, {1.0f, 1.0f}, {0.0f, 1.0f, 0.0f}},
        {{-0.5f, 0.5f, -0.5f}, {0.0f, 1.0f}, {0.0f, 1.0f, 0.0f}},
        {{-0.5f, 0.5f, 0.5f}, {0.0f, 0.0f}, {0.0f, 1.0f, 0.0f}},
        {{-0.5f, 0.5f, 0.5f}, {0.0f, 0.0f}, {0.0f, 1.0f, 0.0f}},
        {{0.5f, 0.5f, 0.5f}, {1.0f, 0.0f}, {0.0f, 1.0f, 0.0f}},
        {{0.5f, 0.5f, -0.5f}, {1.0f, 1.0f}, {0.0f, 1.0f, 0.0f}},
    };
    result.vertices = mesh_arena->push_array<Vertex>(ssize32(vertices));
    memcpy(result.vertices, vertices, sizeof(vertices));
    result.vertex_count = ssize32(vertices);

    return result;
}

Mesh Mesh::create_ui_quad(ArenaAllocator* mesh_arena)
{
    Mesh result;

    Vertex vertices[] = {
        {{0.0f, 0.0f, 0.0f}, {0.0f, 1.0f}, {0.0f, 1.0f, 0.0f}},
        {{1.0f, 0.0f, 0.0f}, {0.0f, 1.0f}, {0.0f, 1.0f, 0.0f}},
        {{1.0f, 1.0f, 0.0f}, {0.0f, 1.0f}, {0.0f, 1.0f, 0.0f}},
        {{1.0f, 1.0f, 0.0f}, {0.0f, 1.0f}, {0.0f, 1.0f, 0.0f}},
        {{0.0f, 1.0f, 0.0f}, {0.0f, 1.0f}, {0.0f, 1.0f, 0.0f}},
        {{0.0f, 0.0f, 0.0f}, {0.0f, 1.0f}, {0.0f, 1.0f, 0.0f}},
    };
    result.vertices = mesh_arena->push_array<Vertex>(ssize32(vertices));
    memcpy(result.vertices, vertices, sizeof(vertices));
    result.vertex_count = ssize32(vertices);

    return result;
}

Mesh Mesh::create_npc_test_billboard(ArenaAllocator* mesh_arena)
{
    Mesh result;

    int texture_width = 256;
    int texture_height = 944;
    int block_width = 16;
    int block_height = 16;
    float u0 = 88.f / (float)texture_width;
    float v0 = (float)(944 - 216) / (float)texture_height;
    float u1 = (float)(88 + block_width) / (float)texture_width;
    float v1 = (float)(944 - (216 + block_height)) / (float)texture_height;
    Vertex vertices[] = {
        {{-0.5f, -0.5f, 0.5f}, {u0, v0}, {0.0f, 0.0f, 1.0f}},
        {{0.5f, -0.5f, 0.5f}, {u1, v0}, {0.0f, 0.0f, 1.0f}},
        {{0.5f, 0.5f, 0.5f}, {u1, v1}, {0.0f, 0.0f, 1.0f}},
        {{0.5f, 0.5f, 0.5f}, {u1, v1}, {0.0f, 0.0f, 1.0f}},
        {{-0.5f, 0.5f, 0.5f}, {u0, v1}, {0.0f, 0.0f, 1.0f}},
        {{-0.5f, -0.5f, 0.5f}, {u0, v0}, {0.0f, 0.0f, 1.0f}},
    };
    result.vertices = mesh_arena->push_array<Vertex>(ssize32(vertices));
    memcpy(result.vertices, vertices, sizeof(vertices));
    result.vertex_count = ssize32(vertices);

    return result;
}

VertexObject::VertexObject(void* vertex_buffer,
                           int vertex_buffer_size,
                           int vertex_size,
                           VertexLayoutAttrib* vertex_attribs,
                           int vertex_attrib_count)
{
    glCreateBuffers(1, &vbo);
    glNamedBufferStorage(vbo, vertex_buffer_size, vertex_buffer,
                         GL_DYNAMIC_STORAGE_BIT);

    glCreateVertexArrays(1, &vao);
    glVertexArrayVertexBuffer(vao, 0, vbo, 0, vertex_size);

    for (int i = 0; i < vertex_attrib_count; ++i)
    {
        VertexLayoutAttrib attrib = vertex_attribs[i];
        GLenum type;
        switch (attrib.type)
        {
        case VertexComponentType::Float: type = GL_FLOAT; break;
        case VertexComponentType::Int: type = GL_INT; break;
        default: assert_(false);
        }

        glEnableVertexArrayAttrib(vao, i);
        glVertexArrayAttribFormat(vao, i, attrib.count, type, GL_FALSE,
                                  attrib.offset);
        glVertexArrayAttribBinding(vao, i, 0);
    }
}

void VertexObject::update_buffer(void* data, int size) const
{
    glNamedBufferSubData(vbo, 0, size, data);
}

void VertexObject::bind() const
{
    glBindVertexArray(vao);
}

void VertexObject::unbind() const
{
    glBindVertexArray(0);
}

Model::Model(Mesh* mesh, Shader* shader)
    : mesh(mesh)
    , shader(shader)
    , vertex_object(mesh->vertices,
                    sizeof(*mesh->vertices) * mesh->vertex_count,
                    sizeof(Vertex),
                    s_layouts,
                    3)
{
}

void Model::bind() const
{
    shader->bind();
    vertex_object.bind();
}

void Model::unbind() const
{
    vertex_object.unbind();
    shader->unbind();
}

ShaderConstant::ShaderConstant(u64 constant_size)
{
    glCreateBuffers(1, &buffer);
    size = constant_size;
    glNamedBufferData(buffer, size, NULL, GL_STATIC_DRAW);
    // glNamedBufferStorage(constant->buffer, constant->size, NULL,
    // GL_DYNAMIC_STORAGE_BIT);
}

void ShaderConstant::set_data(void* data) const
{
    glNamedBufferSubData(buffer, 0, size, data);

    // NOTE(illkwon): This method doesn't work on some nvidia driver
    // glNamedBufferData(constant->buffer, constant->size, data,
    // GL_DYNAMIC_COPY);

    // NOTE(illkwon): This method is too slow
    /*void* mapped_buffer_memory = glMapNamedBuffer(constant->buffer,
    GL_WRITE_ONLY); memcpy(mapped_buffer_memory, data, constant->size);
    glUnmapNamedBuffer(constant->buffer);*/
}

void ShaderConstant::bind(int binding_index) const
{
    glBindBufferRange(GL_UNIFORM_BUFFER, binding_index, buffer, 0, size);
}

Texture get_default_texture()
{
    global_var struct
    {
        Texture texture;
        bool inited;
    } g_default_texture;

    if (!g_default_texture.inited)
    {
        u8 pixel[4] = {
            0xFF,
            0xFF,
            0xFF,
            0xFF,
        };
        g_default_texture.texture = texture_load(
            pixel, 1, 1, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE, GL_NEAREST,
            GL_NEAREST, GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE);

        g_default_texture.inited = true;
    }

    return g_default_texture.texture;
}
RendererBindState
    begin_render_with_initial_binds(ShaderConstant* view_proj_constant,
                                    ShaderConstant* light_constant,
                                    ShaderConstant* model_constant)
{
    view_proj_constant->bind(0);
    light_constant->bind(1);
    model_constant->bind(2);

    for (int i = 0; i < 16; ++i)
        glBindTextureUnit(i, get_default_texture());

    RendererBindState result = {};
    result.view_proj_constant = view_proj_constant;
    result.light_constant = light_constant;
    result.model_constant = model_constant;
    for (auto& this_texture : result.last_bound_textures)
        this_texture = get_default_texture();

    return result;
}

void RenderDataList::push(Mat4 matrix, Vec3 color)
{
    assert_(count < capacity);
    ModelMatrixAndColor mac = {};
    mac.matrix = matrix;
    assert_(mat4_inverse(matrix, &mac.matrix_inv));
    mac.color = color;
    data[count++] = mac;
}

RenderDataList RenderDataList::create_from_temp_memory(int capacity)
{
    RenderDataList result = {};
    if (capacity <= 0)
        return result;

    result.data = (ModelMatrixAndColor*)temp_memory_alloc(
        capacity * sizeof(ModelMatrixAndColor));
    result.capacity = capacity;

    return result;
}

void draw_multiple_objects_with_single_model(RendererBindState* bind_state,
                                             Model* model,
                                             RenderDataList render_data_list,
                                             Texture* textures,
                                             int texture_count)
{
    if (render_data_list.count <= 0)
        return;

    if (bind_state->last_bound_model != model)
    {
        model->bind();
        bind_state->last_bound_model = model;
    }

    int texture_index = 0;
    for (; texture_index < texture_count; ++texture_index)
    {
        if (bind_state->last_bound_textures[texture_index] !=
            textures[texture_index])
        {
            glBindTextureUnit(texture_index, textures[texture_index]);
            bind_state->last_bound_textures[texture_index] =
                textures[texture_index];
        }
    }
    for (; texture_index < ssize(bind_state->last_bound_textures);
         ++texture_index)
    {
        if (bind_state->last_bound_textures[texture_index] !=
            get_default_texture())
        {
            glBindTextureUnit(texture_index, get_default_texture());
            bind_state->last_bound_textures[texture_index] =
                get_default_texture();
        }
    }

    for (int i = 0; i < render_data_list.count; ++i)
    {
        Mat4 m = render_data_list.data[i].matrix;
        Mat4 m_inv = render_data_list.data[i].matrix_inv;
        Vec3 color = render_data_list.data[i].color;
        assert_(mat4_inverse(m, &m_inv));

        ModelConstantData constant_data = {0};
        constant_data.model = m;
        constant_data.model_inv = m_inv;
        constant_data.model_color.v = color;
        bind_state->model_constant->set_data(&constant_data);

        glDrawArrays(GL_TRIANGLES, 0, model->mesh->vertex_count);
    }
}

}
