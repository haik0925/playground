﻿#ifndef DSC_RENDERER_H
#define DSC_RENDERER_H
#include "common.h"
#include "debug.h"
#include "shared.h"
#include "glad.h"

namespace dsc
{
struct ArenaAllocator;

using Texture = uint;

Texture get_default_texture();
Texture texture_load(u8* pixels,
                     int width,
                     int height,
                     GLint wrap_u,
                     GLint wrap_v,
                     GLint min_filter,
                     GLint mag_filter,
                     GLenum internal_format,
                     GLenum pixel_format,
                     GLenum pixel_type);

struct Shader
{
    uint vs = 0;
    uint fs = 0;
    uint program = 0;

    static Shader create(const char* vs_src, const char* fs_src);
    static Shader create_from_file(const char* vs_filename,
                                   const char* fs_filename);
    void bind() const;
    void unbind() const;
};

struct Vertex
{
    Vec3 pos = {};
    Vec2 uv = {};
    Vec3 normal = {};
};

enum class VertexUsage
{
    Undefined = 0,
    Position,
    Normal,
    TexCoord,
    Color,
};

enum class VertexComponentType
{
    Undefined = 0,
    Int,
    Float,
};

struct VertexLayoutAttrib
{
    VertexUsage usage = VertexUsage::Undefined;
    VertexComponentType type = VertexComponentType::Undefined;
    int count = 0;
    int offset = 0;
};

struct Mesh
{
    Vertex* vertices = nullptr;
    int vertex_count = 0;

    static Mesh create_from_obj_file(const char* filename,
                                     ArenaAllocator* mesh_arena);
    static Mesh create_from_temp_obj_info(TempObjInfo* temp_obj_info,
                                          ArenaAllocator* mesh_arena);
    static Mesh create_cube(ArenaAllocator* mesh_arena);
    static Mesh create_plane(ArenaAllocator* mesh_arena);
    static Mesh create_ui_quad(ArenaAllocator* mesh_arena);
    static Mesh create_npc_test_billboard(ArenaAllocator* mesh_arena);
};

struct VertexObject
{
    GLuint vao = 0;
    GLuint vbo = 0;

    VertexObject() = default;

    VertexObject(void* vertex_buffer,
                 int vertex_buffer_size,
                 int vertex_size,
                 VertexLayoutAttrib* vertex_attribs,
                 int vertex_attrib_count);

    void update_buffer(void* data, int size) const;
    void bind() const;
    void unbind() const;
};

struct Model
{
    Mesh* mesh = nullptr;
    Shader* shader = nullptr;

    VertexObject vertex_object;

    inline static VertexLayoutAttrib s_layouts[] = {
        {
            VertexUsage::Position,
            VertexComponentType::Float,
            3,
            offsetof(Vertex, pos),
        },
        {
            VertexUsage::TexCoord,
            VertexComponentType::Float,
            2,
            offsetof(Vertex, uv),
        },
        {
            VertexUsage::Normal,
            VertexComponentType::Float,
            3,
            offsetof(Vertex, normal),
        },
    };

    Model() = default;
    Model(Mesh* mesh, Shader* shader);

    void bind() const;
    void unbind() const;
};

struct ShaderConstant
{
    GLuint buffer = 0;
    u64 size = 0;

    explicit ShaderConstant(u64 constant_size);

    void set_data(void* data) const;
    void bind(int binding_index) const;
};

// TODO(illkwon): Auto generate constant structs by parsing glsl
enum class ConstantType
{
    ViewProj = 0,
    Light,
    Model,
    Count,
};

// vec3 type in std140 layout has alignment of 4*4 bytes.
// thus its c struct representation has to have same
// alignment to the vec3 type in glsl.
union UBOVec3
{
    Vec3 v;
    u32 alignment[4]; // Should never be used. It exists just for alignment.
};

struct ViewProjConstantData
{
    UBOVec3 view_pos;
    Mat4 view;
    Mat4 proj;
};

struct LightConstantData
{
    struct
    {
        UBOVec3 direction;
        UBOVec3 ambient;
        UBOVec3 diffuse;
        UBOVec3 specular;
    } dir_light;

    struct
    {
        UBOVec3 position;
        UBOVec3 ambient;
        UBOVec3 diffuse;
        UBOVec3 specular;
        float constant;
        float linear;
        float quadratic;
        u32 padding;
    } point_lights[10];
    int point_light_count;
};

struct ModelConstantData
{
    Mat4 model;
    Mat4 model_inv;
    UBOVec3 model_color;
};

struct RendererBindState
{
    ShaderConstant* view_proj_constant;
    ShaderConstant* light_constant;
    ShaderConstant* model_constant;

    Model* last_bound_model;
    Texture last_bound_textures[16];
};

RendererBindState
    begin_render_with_initial_binds(ShaderConstant* view_proj_constant,
                                    ShaderConstant* light_constant,
                                    ShaderConstant* model_constant);

struct ModelMatrixAndColor
{
    Mat4 matrix;
    Mat4 matrix_inv;
    Vec3 color;
};

struct RenderDataList
{
    ModelMatrixAndColor* data;
    int capacity;
    int count;

    void push(Mat4 matrix, Vec3 color);

    static RenderDataList create_from_temp_memory(int capacity);
};

void draw_multiple_objects_with_single_model(RendererBindState* bind_state,
                                             Model* model,
                                             RenderDataList render_data_list,
                                             Texture* textures,
                                             int texture_count);
}

#endif // DSC_RENDERER_H
