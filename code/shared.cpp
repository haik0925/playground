﻿#include "shared.h"
#include "app.h"
#include <math.h>

namespace dsc
{
void button_state_process_event(ButtonState* button_state,
                                const AppKeyEvent* key_event)
{
    button_state->is_down = key_event->is_down;
    // button_state->was_down = key_event->was_down;
}

bool button_state_is_pressed(ButtonState button_state)
{
    bool result =
        button_state.is_down && (button_state.is_down != button_state.was_down);
    return result;
}

bool button_state_is_released(ButtonState button_state)
{
    bool result = !button_state.is_down &&
                  (button_state.is_down != button_state.was_down);
    return result;
}

void ButtonState::process_event(const AppKeyEvent* key_event)
{
    button_state_process_event(this, key_event);
}

bool ButtonState::is_pressed() const
{
    return button_state_is_pressed(*this);
}

bool ButtonState::is_released() const
{
    return button_state_is_released(*this);
}

Vec3 Camera::get_look(float yaw_offset_in_degree,
                      float pitch_offset_in_degree) const
{
    float yaw_radian = to_radians(yaw + yaw_offset_in_degree);
    float pitch_radian = to_radians(pitch + pitch_offset_in_degree);
    float sin_yaw = sinf(yaw_radian);
    float cos_yaw = cosf(yaw_radian);
    float sin_pitch = sinf(pitch_radian);
    float cos_pitch = cosf(pitch_radian);

    Vec3 result{sin_yaw * cos_pitch, sin_pitch, cos_yaw * cos_pitch};

    return result;
}

Mat4 Camera::get_view_mat(Vec3 pos_offset,
                          float yaw_offset_in_degree,
                          float pitch_offset_in_degree) const
{
    const Vec3 up{0, 1, 0};
    Vec3 look = get_look(yaw_offset_in_degree, pitch_offset_in_degree);
    const Mat4 result = mat4_lookat(pos + pos_offset, pos + look, up);
    return result;
}

void Camera::look_first_person(Input* input,
                               float rotation_speed_in_degree,
                               float dt)
{
    if (input->mouse_delta.x != 0)
        yaw -= (float)input->mouse_delta.x * dt * rotation_speed_in_degree;
    if (input->mouse_delta.y != 0)
        pitch += (float)input->mouse_delta.y * dt * rotation_speed_in_degree;

    float speed_by_button_input = 30.f;
    float delta_movement_by_button_input =
        speed_by_button_input * dt * rotation_speed_in_degree;
    if (input->button_action_right.is_down != input->button_action_left.is_down)
    {
        if (input->button_action_right.is_down)
            yaw -= delta_movement_by_button_input;
        else
            yaw += delta_movement_by_button_input;
    }
    if (input->button_action_up.is_down != input->button_action_down.is_down)
    {
        if (input->button_action_up.is_down)
            pitch += delta_movement_by_button_input;
        else
            pitch -= delta_movement_by_button_input;
    }

    pitch = fmaxf(fminf(pitch, 80), -80);
}

void Camera::float_toward_look(Input* input, float move_speed, float dt)
{
    Vec3 forward = get_look();
    Vec3 right = forward.cross({0, 1, 0}).normalize();

    vel = {};

    if (input->button_move_left.is_down)
        vel -= right;
    if (input->button_move_right.is_down)
        vel += right;
    if (input->button_move_back.is_down)
        vel -= forward;
    if (input->button_move_forward.is_down)
        vel += forward;

    vel = vel.normalize() * move_speed;
    pos += vel * dt;
}
}
