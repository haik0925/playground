﻿#ifndef DSC_SHARED_H
#define DSC_SHARED_H
#include "mathematics.h"

namespace dsc
{
struct AppKeyEvent;

struct ButtonState
{
    bool is_down;
    bool was_down;

    void process_event(const AppKeyEvent* key_event);
    bool is_pressed() const;
    bool is_released() const;
};

struct Input
{
    union
    {
        struct
        {
            ButtonState button_move_forward;
            ButtonState button_move_back;
            ButtonState button_move_left;
            ButtonState button_move_right;
            ButtonState button_move_jump;
            ButtonState button_action_up;
            ButtonState button_action_down;
            ButtonState button_action_left;
            ButtonState button_action_right;
            ButtonState button_left_click;
            ButtonState button_right_click;
            ButtonState button_reload;
            ButtonState button_interact;
            ButtonState button_select_weapon_1;
            ButtonState button_toggle_editor_edit_mode;
            ButtonState button_debug_1;
            ButtonState button_debug_2;
            ButtonState button_debug_3;
            ButtonState button_debug_4;
            ButtonState button_control;
            ButtonState button_editor_undo;
            ButtonState button_editor_redo;
            ButtonState button_editor_save;
            ButtonState button_editor_delete;
        };

        ButtonState buttons[19];
    };

    IntVec2 mouse_pos;
    IntVec2 mouse_delta;

    // Settings
    bool show_cursor;
    bool lock_mouse_to_center;
};

struct Camera
{
    Vec3 pos = {};
    Vec3 vel = {};
    float yaw = 0;
    float pitch = 0;

    Vec3 get_look(float yaw_offset_in_degree = 0,
                  float pitch_offset_in_degree = 0) const;

    Mat4 get_view_mat(Vec3 pos_offset = {},
                      float yaw_offset_in_degree = 0,
                      float pitch_offset_in_degree = 0) const;
    void look_first_person(Input* input,
                           float rotation_speed_in_degree,
                           float dt);
    void float_toward_look(Input* input, float move_speed, float dt);
};

}
#endif // DSC_SHARED_H
