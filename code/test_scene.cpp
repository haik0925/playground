﻿#include "test_scene.h"
#include "renderer.h"
#include "asset.h"
#include "gui.h"

namespace dsc
{
TestScene::TestScene(AssetStorage* assets) : physics_debug_draw_model(assets)
{
    auto physics_debug_draw_controller = new PhysicsDebugDrawController();
    physics_debug_draw_controller->model = &physics_debug_draw_model;
    b3Draw_draw = physics_debug_draw_controller;
    b3Draw_draw->SetFlags(b3Draw::e_shapesFlag);

    physics_world.SetGravity({0, -9.81f, 0});
    {
        b3BodyDef ground_def;
        ground_def.position.Set(0, -10, 0);
        ground.setup(ground_def, physics_world, {10, 2, 10});
    }

    Vec3 cube_positions[10];
    for (int i = 0; i < dsc::ssize(cube_positions); ++i)
        cube_positions[i] = {0, (float)i * 10, 0};

    for (auto& this_cube_pos : cube_positions)
    {
        TestBoxBody cube;
        b3BodyDef cube_def;
        cube_def.type = e_dynamicBody;
        cube_def.position.Set(this_cube_pos.x, this_cube_pos.y,
                              this_cube_pos.z);
        cube.setup(cube_def, physics_world, {1, 1, 1}, 1.f);
        cubes.push_back(cube);
    }
}

TestScene::~TestScene()
{
    if (b3Draw_draw)
    {
        delete b3Draw_draw;
        b3Draw_draw = nullptr;
    }
    for (auto& this_cube : cubes)
    {
        if (this_cube.hull)
            delete this_cube.hull;
    }
    cubes.clear();
}

void TestScene::update_and_render(Input* input,
                                  AssetStorage* asset_storage,
                                  GuiContext* gui,
                                  AudioContext* audio,
                                  ShaderConstant* view_proj_constant,
                                  ShaderConstant* light_constant,
                                  ShaderConstant* model_constant,
                                  WindowSize window_size,
                                  float dt)
{
    camera.look_first_person(input, 3, dt);
    camera.float_toward_look(input, 20, dt);

    physics_world.Step(dt, 8, 2);

    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glFrontFace(GL_CCW);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glDisable(GL_STENCIL_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glViewport(0, 0, window_size.width, window_size.height);
    glClearColor(0.3f, 0.3f, 0.3f, 1.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    Mat4 view = camera.get_view_mat();
    const Mat4 proj =
        mat4_persp(fov, (float)window_size.width / (float)window_size.height,
                   near_plane, far_plane);
    {
        ViewProjConstantData data = {};
        data.view_pos.v = camera.pos;
        data.view = view;
        data.proj = proj;
        view_proj_constant->set_data(&data);
    }

    {
        LightConstantData data = {};
        data.dir_light.direction = {-0.2f, -1.0f, -0.3f};
        data.dir_light.ambient = {0.2f, 0.2f, 0.2f};
        data.dir_light.diffuse = {0.5f, 0.5f, 0.5f};
        data.dir_light.specular = {1.f, 1.f, 1.f};
        data.point_lights[0].position.v = camera.pos;
        data.point_lights[0].ambient.v = {0.5f, 0.5f, 0.5f};
        data.point_lights[0].diffuse.v = {0.8f, 0.8f, 0.8f};
        data.point_lights[0].specular.v = {1.f, 1.f, 1.f};
        data.point_lights[0].constant = 1;
        data.point_lights[0].linear = 0.022f;
        data.point_lights[0].quadratic = 0.0019f;
        data.point_light_count = 1;
        light_constant->set_data(&data);
    }

    RendererBindState renderer_bind_state = begin_render_with_initial_binds(
        view_proj_constant, light_constant, model_constant);

    /*RenderDataList render_data_list =
        RenderDataList::create_from_temp_memory(cubes.ssize32() + 1);
    for (auto& this_cube : cubes)
        render_data_list.push(this_cube.get_transform(), {1, 0.5f, 0.5f});
    render_data_list.push(ground.get_transform(), {1, 1, 1});

    draw_multiple_objects_with_single_model(
        &renderer_bind_state, &asset_storage->_models[ModelType::CubeTest],
        render_data_list, &asset_storage->_textures[TextureType::Door], 1);*/

    physics_world.Draw();
    physics_debug_draw_model.render();

    {
        float y = 10;
        float font_scale = 1.5f;
        float h = gui->get_font_height() * font_scale;
        gui->push_label(10, y, font_scale, 1,
                        "ground.get_position(): [%f %f %f]",
                        ground.get_position().x, ground.get_position().y,
                        ground.get_position().z);
        y += h;
        int i = 0;
        for (auto& this_cube : cubes)
        {
            gui->push_label(
                10, y, font_scale, 1, "cubes[%d].get_position(): [%f %f %f]", i,
                this_cube.get_position().x, this_cube.get_position().y,
                this_cube.get_position().z);
            y += h;
            ++i;
        }
    }
}

}
