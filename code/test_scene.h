﻿#ifndef DSC_TEST_SCENE_H
#define DSC_TEST_SCENE_H
#include "shared.h"
#include "asset.h"
#include "gui.h"
#include "physics.h"
#include "base_scene.h"
#include "bounce/bounce.h"

namespace dsc
{
struct TestBoxBody
{
    b3Body* body = nullptr;
    b3BoxHull* hull = nullptr;
    Vec3 size = {};

    void setup(const b3BodyDef& def,
               b3World& world,
               Vec3 shape_size,
               float shape_density = 0.f)
    {
        body = world.CreateBody(def);
        hull = new b3BoxHull();
        size = shape_size;
        hull->SetTransform({b3Diagonal(size.x, size.y, size.z), b3Vec3_zero});
        b3HullShape hull_shape;
        hull_shape.m_hull = hull;
        b3ShapeDef shape_def;
        shape_def.shape = &hull_shape;
        body->CreateShape(shape_def);
    }

    auto get_position() const
    {
        return body->GetPosition();
    }

    Mat4 get_transform() const
    {
        Mat4 m = mat4_translate(get_position().x, get_position().y,
                                get_position().z) *
                 mat4_scale(size.x * 2, size.y * 2, size.z * 2);
        return m;
    }
};

struct TestScene : BaseScene
{
    PhysicsDebugDrawModel physics_debug_draw_model;

    b3World physics_world;

    Camera camera;
    float fov = 60.f;
    float near_plane = 0.1f;
    float far_plane = 1000.f;

    TestBoxBody ground;
    dsc::vector<TestBoxBody> cubes;

    explicit TestScene(AssetStorage* assets);
    ~TestScene() override;
    void update_and_render(Input* input,
                           AssetStorage* asset_storage,
                           GuiContext* gui,
                           AudioContext* audio,
                           ShaderConstant* view_proj_constant,
                           ShaderConstant* light_constant,
                           ShaderConstant* model_constant,
                           WindowSize window_size,
                           float dt) override;
};

}

#endif // DSC_TEST_SCENE_H
