﻿#include "memory.h"
#define STBI_MALLOC(sz) dsc::temp_memory_alloc(sz)
#define STBI_REALLOC(p, sz) dsc::temp_memory_realloc(p, sz)
#define STBI_FREE(p) (p)
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h"
#define TINYOBJ_MALLOC(size) dsc::temp_memory_alloc(size)
#define TINYOBJ_FREE(p) (p)
#define TINYOBJ_LOADER_C_IMPLEMENTATION
#include "tinyobj_loader_c.h"
#include "stb_vorbis.h"
#define CUTE_SOUND_IMPLEMENTATION
#include "cute_sound.h"
