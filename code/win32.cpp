﻿#include "win32.h"
#include "common.h"
#include "debug.h"
#include "app.h"
#include "glad_wgl.h"

namespace dsc
{
// TODO(illkwon): Remove global state?
global_var HCURSOR g_cursor;
global_var bool g_show_cursor = true;
global_var bool g_is_window_focused = true;

internal_func LRESULT CALLBACK win32_main_window_callback(HWND window,
                                                          UINT message,
                                                          WPARAM wp,
                                                          LPARAM lp);

static_assert(sizeof(HWND) == sizeof(Handle64), "Incompatible handle size");
static_assert(sizeof(HDC) == sizeof(Handle64), "Incompatible handle size");

constexpr auto win32_class_name = L"win32_class_name";

internal_func void APIENTRY win32__gl_debug_callback(GLenum source,
                                                     GLenum type,
                                                     GLuint id,
                                                     GLenum severity,
                                                     GLsizei length,
                                                     const GLchar* message,
                                                     const void* userdata)
{
    const char* header = "GL";
    const char* source_str = NULL;
    switch (source)
    {
    case GL_DEBUG_SOURCE_API: source_str = "API"; break;
    case GL_DEBUG_SOURCE_WINDOW_SYSTEM: source_str = "WINDOW_SYSTEM"; break;
    case GL_DEBUG_SOURCE_SHADER_COMPILER: source_str = "SHADER_COMPILER"; break;
    case GL_DEBUG_SOURCE_THIRD_PARTY: source_str = "THIRD_PARTY"; break;
    case GL_DEBUG_SOURCE_APPLICATION: source_str = "APPLICATION"; break;
    case GL_DEBUG_SOURCE_OTHER: source_str = "OTHER"; break;
    default: assert_(false); break;
    }

    const char* type_str = NULL;
    switch (type)
    {
    case GL_DEBUG_TYPE_ERROR: type_str = "ERROR"; break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
        type_str = "DEPRECATED_BEHAVIOR";
        break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
        type_str = "UNDEFINED_BEHAVIOR";
        break;
    case GL_DEBUG_TYPE_PORTABILITY: type_str = "PORTABILITY"; break;
    case GL_DEBUG_TYPE_PERFORMANCE: type_str = "PERFORMANCE"; break;
    case GL_DEBUG_TYPE_MARKER: type_str = "MARKER"; break;
    case GL_DEBUG_TYPE_PUSH_GROUP: type_str = "PUSH_GROUP"; break;
    case GL_DEBUG_TYPE_POP_GROUP: type_str = "POP_GROUP"; break;
    case GL_DEBUG_TYPE_OTHER: type_str = "OTHER"; break;
    default: assert_(false); break;
    }

    const char* serverity_str = NULL;
    switch (severity)
    {
    case GL_DEBUG_SEVERITY_HIGH: serverity_str = "HIGH"; break;
    case GL_DEBUG_SEVERITY_MEDIUM: serverity_str = "MEDIUM"; break;
    case GL_DEBUG_SEVERITY_LOW: serverity_str = "LOW"; break;
    case GL_DEBUG_SEVERITY_NOTIFICATION: serverity_str = "NOTIFICATION"; break;
    default: assert_(false); break;
    }

    log_append("[ ");
    log_append(header);
    log_append(" ");
    log_append(source_str);
    log_append(" ");
    log_append(type_str);
    log_append(" ");
    log_append(serverity_str);
    log_(" ] ");
    log_(message);
}

void win32_init_wgl(Win32Context* win32)
{
    HWND fake_window = CreateWindowEx(0, win32_class_name, L"fake_window_name",
                                      WS_OVERLAPPEDWINDOW, //|WS_VISIBLE,
                                      CW_USEDEFAULT, CW_USEDEFAULT, 1, 1, 0, 0,
                                      (HINSTANCE)win32->instance, 0);
    assert_(fake_window);

    HDC fake_dc = GetDC(fake_window);

    PIXELFORMATDESCRIPTOR fake_pfd = {};
    fake_pfd.nSize = sizeof(fake_pfd);
    fake_pfd.nVersion = 1;
    fake_pfd.dwFlags =
        PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    fake_pfd.iPixelType = PFD_TYPE_RGBA;
    fake_pfd.cColorBits = 32;
    fake_pfd.cDepthBits = 24;
    fake_pfd.cStencilBits = 8;
    fake_pfd.iLayerType = PFD_MAIN_PLANE;

    int fake_pixel_format = ChoosePixelFormat(fake_dc, &fake_pfd);
    assert_(fake_pixel_format);

    SetPixelFormat(fake_dc, fake_pixel_format, &fake_pfd);
    HGLRC fake_rc = wglCreateContext(fake_dc);
    assert_(fake_rc);
    assert_(wglMakeCurrent(fake_dc, fake_rc));
    assert_(gladLoadGL());

    assert_(gladLoadWGL((HDC)win32->dc));

    int pixel_attribs[] = {WGL_DRAW_TO_WINDOW_ARB,
                           GL_TRUE,
                           WGL_SUPPORT_OPENGL_ARB,
                           GL_TRUE,
                           WGL_DOUBLE_BUFFER_ARB,
                           GL_TRUE,
                           WGL_PIXEL_TYPE_ARB,
                           WGL_TYPE_RGBA_ARB,
                           WGL_ACCELERATION_ARB,
                           WGL_FULL_ACCELERATION_ARB,
                           WGL_COLOR_BITS_ARB,
                           32,
                           WGL_ALPHA_BITS_ARB,
                           8,
                           WGL_DEPTH_BITS_ARB,
                           24,
                           WGL_STENCIL_BITS_ARB,
                           8,
                           WGL_SAMPLE_BUFFERS_ARB,
                           GL_TRUE,
                           WGL_SAMPLES_ARB,
                           4,
                           0};

    int pixel_format;
    UINT num_formats;
    bool status = wglChoosePixelFormatARB((HDC)win32->dc, pixel_attribs, NULL,
                                          1, &pixel_format, &num_formats);
    assert_(status && num_formats != 0);

    PIXELFORMATDESCRIPTOR pfd;
    DescribePixelFormat((HDC)win32->dc, pixel_format, sizeof(pfd), &pfd);
    SetPixelFormat((HDC)win32->dc, pixel_format, &pfd);
    int context_attribs[] = {WGL_CONTEXT_MAJOR_VERSION_ARB,
                             4,
                             WGL_CONTEXT_MINOR_VERSION_ARB,
                             5,
                             WGL_CONTEXT_PROFILE_MASK_ARB,
                             WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
#ifdef DEBUG
                             WGL_CONTEXT_FLAGS_ARB,
                             WGL_CONTEXT_DEBUG_BIT_ARB,
#endif
                             0};
    HGLRC rc = wglCreateContextAttribsARB((HDC)win32->dc, 0, context_attribs);
    assert_(rc);

    wglMakeCurrent(0, 0);
    wglDeleteContext(fake_rc);
    ReleaseDC(fake_window, fake_dc);
    DestroyWindow(fake_window);
    assert_(wglMakeCurrent((HDC)win32->dc, rc));

#ifdef GLAD_DEBUG
    // glad_set_pre_callback(&openglPreCallback);
    // glad_set_post_callback(&openglPostCallback);
#define GLAD_DETACH_DEBUG_CALLBACK(gl_func)                                    \
    glad_debug_##gl_func = glad_##gl_func
    GLAD_DETACH_DEBUG_CALLBACK(glBegin);
    GLAD_DETACH_DEBUG_CALLBACK(glEnd);
    GLAD_DETACH_DEBUG_CALLBACK(glVertex3f);
#undef GLAD_DETACH_DEBUG_CALLBACK
#endif // GLAD_DEBUG

#if DEBUG
    GLint gl_context_flags;
    glGetIntegerv(GL_CONTEXT_FLAGS, &gl_context_flags);
    if (gl_context_flags & GL_CONTEXT_FLAG_DEBUG_BIT)
    {
        glEnable(GL_DEBUG_OUTPUT);
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
        glDebugMessageCallback(&win32__gl_debug_callback, NULL);
        glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL,
                              GL_TRUE);
        glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE,
                              GL_DEBUG_SEVERITY_NOTIFICATION, 0, NULL,
                              GL_FALSE);
    }
#endif

    log_("OpenGL version: %s", glGetString(GL_VERSION));
    log_("GLSL version: %s", glGetString(GL_SHADING_LANGUAGE_VERSION));

    // Clear event created by creating and deleting fake window
    while (true)
    {
        AppEvent dummy = {};
        if (!win32_poll_event(&dummy))
            break;
    }
}

Win32Context win32_init(int window_width, int window_height)
{
    HINSTANCE instance = GetModuleHandle(0);

    g_cursor = LoadCursor(NULL, IDC_ARROW);

    WNDCLASSEX wc = {};
    wc.cbSize = sizeof(wc);
    wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wc.lpfnWndProc = &win32_main_window_callback;
    wc.hInstance = instance;
    wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wc.hCursor = g_cursor;
    wc.lpszClassName = win32_class_name;

    assert_(RegisterClassEx(&wc));

    DWORD window_style = WS_OVERLAPPEDWINDOW | WS_VISIBLE;
    RECT window_rect = {0, 0, window_width, window_height};
    assert_(AdjustWindowRectEx(&window_rect, window_style, FALSE, 0));
    int actual_window_width = window_rect.right - window_rect.left;
    int actual_window_height = window_rect.bottom - window_rect.top;
    HWND window =
        CreateWindowEx(0, wc.lpszClassName, L"Doomscape", window_style,
                       CW_USEDEFAULT, CW_USEDEFAULT, actual_window_width,
                       actual_window_height, 0, 0, instance, 0);
    assert_(window);

    HDC dc = GetDC(window);

    /*
    switch (rctype)
    {
    case Win32RendererContextType_DirectX:
        win32__init_dx(window_width, window_height, window);
        break;
    case Win32RendererContextType_OpenGL:
        win32_init_wgl(instance, window, dc);
        break;
    default:
        assert_(false);
    }*/

    Win32Context result = {};
    result.window = (Handle64)window;
    result.dc = (Handle64)dc;
    return result;
}

void win32_show_cursor(bool show)
{
    if (g_show_cursor != show)
    {
        g_show_cursor = show;
        SetCursor(g_show_cursor ? g_cursor : NULL);
    }
}

internal_func LRESULT CALLBACK win32_main_window_callback(HWND window,
                                                          UINT message,
                                                          WPARAM wp,
                                                          LPARAM lp)
{
    LRESULT result = 0;

    switch (message)
    {
    case WM_SETCURSOR:
        if (g_show_cursor)
            SetCursor(g_cursor);
        else
            SetCursor(NULL);
        // result = DefWindowProc(window, message, wp, lp);
        break;

    case WM_EXITSIZEMOVE: break;
    case WM_SIZE: break;
    case WM_CLOSE:
    case WM_DESTROY: { PostQuitMessage(0);
    }
    break;

    case WM_SYSKEYDOWN:
    case WM_SYSKEYUP:
    case WM_KEYDOWN:
    case WM_KEYUP: assert_(false); break;

    // TODO(illkwon): Custom event queue (to make it not depend on global
    // state)?
    // alt-tab incident goes here!
    case WM_SETFOCUS: g_is_window_focused = true; break;
    case WM_KILLFOCUS: g_is_window_focused = false; break;

    default: result = DefWindowProc(window, message, wp, lp); break;
    }

    return result;
}

bool win32_poll_event(AppEvent* out_event)
{
    MSG msg;
    bool is_event_polled = PeekMessage(&msg, 0, 0, 0, PM_REMOVE);

    if (is_event_polled)
    {
        WPARAM wp = msg.wParam;
        LPARAM lp = msg.lParam;

        switch (msg.message)
        {
        case WM_QUIT: *out_event = AppEvent{AppEventType::Quit}; break;

        case WM_SYSKEYDOWN:
        case WM_SYSKEYUP:
        case WM_KEYDOWN:
        case WM_KEYUP:
        {
            AppKeyCode keycode = AppKeyCode::Undefined;
            switch (wp)
            {
            case VK_ESCAPE: keycode = AppKeyCode::ESC; break;
            case VK_SPACE: keycode = AppKeyCode::Space; break;
            case '0': keycode = AppKeyCode::Num0; break;
            case '1': keycode = AppKeyCode::Num1; break;
            case '2': keycode = AppKeyCode::Num2; break;
            case '3': keycode = AppKeyCode::Num3; break;
            case '4': keycode = AppKeyCode::Num4; break;
            case '5': keycode = AppKeyCode::Num5; break;
            case '6': keycode = AppKeyCode::Num6; break;
            case '7': keycode = AppKeyCode::Num7; break;
            case '8': keycode = AppKeyCode::Num8; break;
            case '9': keycode = AppKeyCode::Num9; break;
            case 'A': keycode = AppKeyCode::A; break;
            case 'D': keycode = AppKeyCode::D; break;
            case 'E': keycode = AppKeyCode::E; break;
            case 'R': keycode = AppKeyCode::R; break;
            case 'S': keycode = AppKeyCode::S; break;
            case 'W': keycode = AppKeyCode::W; break;
            case 'Y': keycode = AppKeyCode::Y; break;
            case 'Z': keycode = AppKeyCode::Z; break;
            case VK_UP: keycode = AppKeyCode::ArrowUp; break;
            case VK_DOWN: keycode = AppKeyCode::ArrowDown; break;
            case VK_LEFT: keycode = AppKeyCode::ArrowLeft; break;
            case VK_RIGHT: keycode = AppKeyCode::ArrowRight; break;
            case VK_F1: keycode = AppKeyCode::F1; break;
            case VK_F2: keycode = AppKeyCode::F2; break;
            case VK_F3: keycode = AppKeyCode::F3; break;
            case VK_F4: keycode = AppKeyCode::F4; break;
            case VK_F5: keycode = AppKeyCode::F5; break;
            case VK_F6: keycode = AppKeyCode::F6; break;
            case VK_F7: keycode = AppKeyCode::F7; break;
            case VK_F8: keycode = AppKeyCode::F8; break;
            case VK_F9: keycode = AppKeyCode::F9; break;
            case VK_F10: keycode = AppKeyCode::F10; break;
            case VK_F11: keycode = AppKeyCode::F11; break;
            case VK_F12: keycode = AppKeyCode::F12; break;
            case VK_SHIFT: keycode = AppKeyCode::Shift; break;
            case VK_CONTROL: keycode = AppKeyCode::Control; break;
            case VK_DELETE: keycode = AppKeyCode::Delete; break;

            default: break;
            }
            if (keycode != AppKeyCode::Undefined)
            {
                *out_event = {};
                out_event->key = AppKeyEvent{
                    AppEventType::Keyupdown,
                    keycode,
                    //.was_down = ((lp & (1 << 30)) != 0),
                    ((lp & (1 << 31)) == 0),
                    ((lp & (1 << 29)) != 0),
                };
            }
        }
        break;

        case WM_LBUTTONDOWN:
        case WM_RBUTTONDOWN:
        {
            AppKeyCode keycode = AppKeyCode::Undefined;
            if (wp & MK_RBUTTON)
                keycode = AppKeyCode::RMouse;
            else if (wp & MK_LBUTTON)
                keycode = AppKeyCode::LMouse;
            if (keycode != AppKeyCode::Undefined)
            {
                *out_event = {};
                out_event->key = AppKeyEvent{
                    AppEventType::Keyupdown, keycode, true,
                    false, // this too
                };
            }
        }
        break;

        case WM_LBUTTONUP:
        {
            AppKeyCode keycode = AppKeyCode::LMouse;
            *out_event = {};
            out_event->key = {
                AppEventType::Keyupdown, keycode, false,
                false, // this too
            };
        }
        break;
        case WM_RBUTTONUP:
        {
            AppKeyCode keycode = AppKeyCode::RMouse;
            *out_event = {};
            out_event->key = {
                AppEventType::Keyupdown, keycode, false,
                false, // this too
            };
        }
        break;

        default: TranslateMessage(&msg); DispatchMessage(&msg);
        }
    }

    return is_event_polled;
}

WindowSize win32_window_size(const Win32Context* ctx)
{
    RECT client_rect;
    GetClientRect((HWND)(ctx->window), &client_rect);
    WindowSize result = {client_rect.right - client_rect.left,
                         client_rect.bottom - client_rect.top};

    return result;
}

float win32_elapsed_time(Win32Clock start, Win32Clock end)
{
    global_var LARGE_INTEGER freq;
    global_var bool is_first_call = true;
    if (is_first_call)
    {
        QueryPerformanceFrequency(&freq);
        is_first_call = false;
    }

    float result = (float)(end - start) / (float)freq.QuadPart;
    return result;
}

Win32Clock win32_current_clock()
{
    LARGE_INTEGER result;
    QueryPerformanceCounter(&result);
    return result.QuadPart;
}

IntVec2 win32_cursor_pos(const Win32Context* ctx)
{
    WindowSize ws = win32_window_size(ctx);

    POINT mp;
    GetCursorPos(&mp);
    ScreenToClient((HWND)(ctx->window), &mp);
    return {mp.x, ws.height - mp.y};
}

void win32_set_cursor_pos(const Win32Context* ctx, int x, int y)
{
    WindowSize ws = win32_window_size(ctx);
    POINT mp = {x, ws.height - y};
    ClientToScreen((HWND)(ctx->window), &mp);
    SetCursorPos(mp.x, mp.y);
}

bool win32_is_window_focused()
{
    return g_is_window_focused;
}
}
