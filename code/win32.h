﻿#ifndef DSC_WIN32_H
#define DSC_WIN32_H

#include "primitive_types.h"
#include "mathematics.h"

namespace dsc
{
union AppEvent;

using Win32Clock = u64;

struct Win32Context
{
    Handle64 instance;
    Handle64 window;
    Handle64 dc;
};

bool win32_poll_event(AppEvent* out_event);
void win32_init_wgl(Win32Context* win32);
Win32Context win32_init(int window_width, int window_height);
void win32_show_cursor(bool show);
WindowSize win32_window_size(const Win32Context* ctx);
float win32_elapsed_time(Win32Clock start, Win32Clock end);
Win32Clock win32_current_clock();
IntVec2 win32_cursor_pos(const Win32Context* ctx);
void win32_set_cursor_pos(const Win32Context* ctx, int x, int y);
bool win32_is_window_focused();

}

#endif // DSC_WIN32_H
