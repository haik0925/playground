#!/usr/bin/env python3
import subprocess

def main():
    cloc_result = subprocess.Popen(["cloc", "code", "-v=2",
                               "--exclude-list-file=cloc_exclude.txt"],
                              stdout=subprocess.PIPE).communicate()[0].decode('utf-8')
    print(cloc_result)
    loc = cloc_result.split()[-2]
    output = "Total Lines of Code:" + loc
    print(output)
    with open("loc.txt", "w") as f:
        f.write(output)

if __name__ == "__main__":
    main()

