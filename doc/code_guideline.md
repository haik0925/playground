# 코드 가이드라인

## 스타일
- 들여쓰기는 항상 들여쓰기 당 4개의 space를 사용한다. tab은 사용하지 않는다.
- 모든 텍스트 인코딩은 BOM없는 UTF-8을 사용한다.
- 함수, 구조체(struct), 열거자(enum) 정의 시 중괄호는 항상 newline에 작성한다.
  ```c
  typedef struct
  {
  } Foo;
  typedef enum
  {
  } Bar;
  int main()
  {
  }
  ```

## 네이밍 컨벤션

### 파일
- 파일 이름은 snake_case로 작성한다.
- 파일 이름은 네임스페이스의 역할을 대체한다.
- 가능하면 struct에 해당하는 개별 파일을 만들지 않는다.
    - 불필요한 파일 파편화를 줄이기 위함.
    - 전체적인 빌드 타임을 향상시키기 위함.
- 모든 파일은 code디렉토리에 위치해 있어야 한다. 서브 디렉토리는 만들지 않는다.
- 모든 헤더 파일에 헤더 가드를 붙인다. 헤더 가드의 이름은 ALL_UPPER_CASE로 작성한다.
  ```c
  // File Name: sample_code.h
  #ifndef SAMPLE_CODE_H
  #define SAMPLE_CODE_H
  /*Code goes here*/
  #endif//SAMPLE_CODE_H
  ```
  
### 함수
- 모든 함수의 이름은 snake_case로 작성한다.
- 다양한 문맥에서 사용될수 있는 범용 유틸성 함수
  ```c
  void function_name();
  ```
- 다양한 종류의 데이터를 처리하는 함수   
  namespace의 역할인 파일이름을 prefix로 사용한다.
  ```c
  void file_name_function_name();
  ```
- struct의 데이터 처리가 주를 이루는 함수
  ```c
  void struct_name_function_name();
  ```
- Internal 함수
  - C언어는 private함수를 언어 차원에서 지원하진 않는다.   
    대신 static키워드를 활용해 함수의 사용처를 file scope내부로 한정 지을수 있다.
    이러한 함수들을 internal함수라 칭한다.
    - static 키워드 대신 internal 매크로를 사용하자.
  - 이 컨벤션은 stb 라이브러리의 internal함수 네이밍 컨벤션을 가져온 것이다.
  ```c
  internal void struct_name__function_name();// One more underscore than normal function
  internal void file_name__function_name();// One more underscore than normal function
  ```
- 자주 쓰이는 컨벤션
  ```c
  typedef struct
  {
  /*Data fields*/
  } Foo;

  Foo foo_load(const char* filename); // Load from other data (e.g. files)
  void foo_init(Foo* foo); // Handles init
  void foo_deinit(); // Handles data deinit
  Foo* foo_create(); // Handles both memory alloc and data init
  void foo_destroy(); // Handles both memory free and data deinit
  ```

### 구조체(struct), 열거자(enum)
- 모든 struct/enum의 이름은 PascalCase로 작성한다.
- 모든 struct/enum의 선언은 typedef를 사용한다.
- struct/enum 변수 선언 시 struct/enum의 이름 대신 typedef된 이름를 사용한다. struct/enum의 이름은 typedef 이름과 동일하게 지정하거나 익명으로 남긴다. 보통 forward declaration이 필요한 경우에 struct/enum의 이름을 지정한다.
  ```c
  typedef struct Foo//struct name is optional
  {
  /*Data fields*/
  } Foo;

  int main()
  {
      struct Foo a; // WRONG!!
      Foo a; // CORRECT

      return 0;
  }
  ```
- enum value의 이름은 EnumName_Value와 같이 지정한다.
  ```c
  typedef enum Bar//enum name is optional
  {
      Bar_A,
      Bar_B,
      Bar_C,
      Bar_D,
      Bar_ThisIsValue,
      Bar_ValueNameShouldUsePascalCase,
  } Bar
  ```
  

### 변수
- 모든 변수의 이름은 snake_case로 작성한다.
- static 변수 선언은 global 매크로로 대체한다.
- Variable Length Array/View 를 의미하는 포인터와 그 사이즈는 아래와 같이
  명명한다.
  ```c
  Object* objects;
  int object_count;
  ```

### 매크로
- 모든 매크로의 이름은 snake_case로 작성한다.
  - 함수 네이밍 컨벤션을 동일하게 가져가기 위함.

## 코드 스타일
- 헤더파일은 최대한 적은 수의 헤더파일만을 include한다. 구현 코드에 필요한 코드를 헤더 파일에 include 하지 않는다. 소스 파일에 include하자.
- malloc을 쓰지 않는다. 모든 힙 할당은 프로그램 시작시점에 VirtualAlloc으로 할당한 메모리를 활용하는 것으로 대체한다.
  - Temporary한 메모리 할당은 temp_memory_alloc을 사용하자.
- 간단한 struct 초기화는 C99의 기능인 designated initializer를 사용한다.
  ```c
  Foo foo = {.a = 1, .b = 2, .c = 3};
  ```
- 또한 C99의 기능인 compound literal도 적극적으로 사용한다. 주로 포인터를 받는 함수에 임시 객체를 넘겨줄때 유용하다.
  ```c
  void foo_do_something(Foo* foo);

  int main()
  {
      foo_do_something(&(Foo){.a = 1, .b = 2, .c = 3});
  }
  ```
- 여러 줄 분량의 단일 또는 복수 statment를 포함하는 if/else/for/while scope는
  중괄호로 명시한다.
  ```c
  // Multi statements
  for (int i = 0; i < 1; ++i)
  {
      sum = a + b;
      result += sum;
  }
  // Multi-line single statement
  for (int i = 0; i < 1; ++i)
  {
      if (i % 2 == 0)
          count += 1;
  }
  ```
- 1줄 분량의 단일 statement를 포함하는 if/else/for/while scope는 중괄호로
  명시하지 않는다.
  ```c
  if (i % 2 == 0)
      count += 1;
  ```
- if/else절 묶음 중 최소 한 케이스에서 scope를 중괄호로 명시해야 할 경우, 나머지
  케이스도 scope를 중괄호로 명시한다.
  ```c
  if (i % 2 == 0)
  {
      even_count += 1;
  }
  else
  {
      printf("The number is not even!\n");
      odd_count -= 1;
  }
  ```
