local solution = {
    name = "doomscape",
    config = {
        debug = "debug",
        release = "release",
    },
}
local doomscape = {
    project_name = "doomscape",
    postbuild_command = "python3 $(SolutionDir)scripts\\preprocess_glsl.py $(SolutionDir)code\\glsl $(SolutionDir)data\\glsl",
    src_dir = "code/",
    script_dir = "scripts/",
    shader_dir = "code/glsl/",
    working_dir = "data/",
    output_dir = "build/",
    lib_dir = "lib/",
}

local bounce = {
    project_name = "bounce",
    src_dir = "code/thirdparties/bounce/src/",
    include_dir = "code/thirdparties/bounce/include/",
    external_dir = "code/thirdparties/bounce/external/",
    output_dir = doomscape.lib_dir,
}

function os.winSdkVersion()
    local reg_arch = iif( os.is64bit(), "\\Wow6432Node\\", "\\" )
    local sdk_version = os.getWindowsRegistry( "HKLM:SOFTWARE" .. reg_arch .."Microsoft\\Microsoft SDKs\\Windows\\v10.0\\ProductVersion" )
    print(sdk_version)
    if sdk_version ~= nil then return sdk_version .. ".0" end
end

workspace(solution.name)
    startproject(doomscape.project_name)
    architecture "x64"
    configurations {solution.config.debug, solution.config.release}
    warnings "Extra"
    language "C++"
    -- compileas "C++"
    -- staticruntime "On" -- available on master build
    buildoptions "/std:c++latest"
    system "windows"
    systemversion(os.winSdkVersion())
    architecture "x64"
    defines {"_CRT_SECURE_NO_WARNINGS"}
    -- Disable some msvc warnings
    disablewarnings {
    "4101", -- Unreferenced local variable
    "4204", -- Non standard non-constant aggregate initializer
    "4505",
    "4456",
    "4201",
    "4100",
    "4189",
    "4458",
    "4819",
    "4127",
    "4701",
    "4291",
    }

project(doomscape.project_name)
    kind "WindowedApp"
    flags {"MultiProcessorCompile", "NoMinimalRebuild",
           "NoBufferSecurityCheck", "FatalCompileWarnings"}
    files {
        doomscape.src_dir .. "*.*",
        doomscape.src_dir .. "thirdparties/*.*",
        doomscape.shader_dir .. "/include/*.*",
        doomscape.shader_dir .. "*.*",
        doomscape.script_dir .. "*.*",
        ".clang-format",
    }
    removefiles {
        doomscape.src_dir .. "game_unitybuild.*"
    }
    debugdir(doomscape.working_dir)
    includedirs {
        doomscape.src_dir .. "thirdparties",
        bounce.include_dir,
    }--, "$(UM_IncludePath)"}
    libdirs "$(WindowsSDK_LibraryPath_x64)"
    links {"opengl32", "winmm"}

    postbuildcommands {doomscape.postbuild_command}

    filter("configurations:" .. solution.config.debug)
        libdirs(doomscape.lib_dir .. solution.config.debug)
        links(bounce.project_name .. solution.config.debug)
        targetdir(doomscape.output_dir .. solution.config.debug)
        defines {"DEBUG"}
        symbols "On"

    filter("configurations:" .. solution.config.release)
        libdirs(doomscape.lib_dir .. solution.config.release)
        links(bounce.project_name .. solution.config.release)
        targetdir(doomscape.output_dir .. solution.config.release)
        optimize "On"

project(bounce.project_name)
    kind "StaticLib"
    flags {"MultiProcessorCompile", "NoMinimalRebuild",
           "NoBufferSecurityCheck"}
    files {
        bounce.src_dir .. "bounce/**.*",
        bounce.include_dir .. "bounce/**.*",
        bounce.include_dir .. "bounce/**.*",
        bounce.external_dir .. "triangle/*.h",
        bounce.external_dir .. "triangle/*.c",
    }

    includedirs {
        bounce.external_dir,
        bounce.include_dir,
    }--, "$(UM_IncludePath)"}

    filter("configurations:" .. solution.config.debug)
        targetname(bounce.project_name .. solution.config.debug)
        targetdir(bounce.output_dir .. solution.config.debug)
        defines {"DEBUG"}
        symbols "On"

    filter("configurations:" .. solution.config.release)
        targetname(bounce.project_name .. solution.config.release)
        targetdir(bounce.output_dir .. solution.config.release)
        optimize "On"
