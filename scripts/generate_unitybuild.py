#!/usr/bin/env python3
import argparse
import os

def gen_unitybuid_for_extension(extension: str, root_dir: str, excludes: list):
    output_filename = "game_unitybuild." + extension

    excludes.append(output_filename)

    output_path = os.path.join(root_dir, output_filename)
    with open(output_path, "w") as output_file:
        for filename in os.listdir(root_dir):
            if filename.endswith(extension) and filename not in excludes:
                output_file.write("#include \"" + filename + "\"\n")

def main():
    parser = argparse.ArgumentParser();
    parser.add_argument("root", help="Root Directory")
    parser.add_argument("exclude_list", help="Files to exclude", nargs="*")
    args = parser.parse_args();

    root_dir = args.root;
    excludes = args.exclude_list

    gen_unitybuid_for_extension("cpp", root_dir, excludes)
    gen_unitybuid_for_extension("c", root_dir, excludes)

if __name__ == "__main__":
    main()
