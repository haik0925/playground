#!/usr/bin/env python3
import argparse
import re
import os

def insert_include_files_to_target(target_str, include_str_table):
    found = re.search('(?:\/\/ *)(?:INCLUDE_FILE +")(?P<include>\w+.\w+)(?:")', target_str)
    if found:
        include_filename = found.group("include")
        include_str = include_str_table[include_filename]
        str_index_to_insert = found.end() + 1
        result = target_str[:str_index_to_insert] + include_str + target_str[str_index_to_insert:]
        # print("Final result: \n" + result)
    else:
        # print("No matches!")
        result = target_str
    return result

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("src", help="Source Directory")
    parser.add_argument("dst", help="Destination Directory")
    args = parser.parse_args()

    source_dir = args.src
    dest_dir = args.dst
    print("Source Dir: " + source_dir)
    print("Dest Dir: " + dest_dir)

    os.makedirs(dest_dir, exist_ok=True)

    include_root_dir = os.path.join(source_dir, "include")
    include_str_table = {}
    for include_filename in os.listdir(include_root_dir):
        with open(os.path.join(include_root_dir, include_filename), "r") as f:
            include_str_table[include_filename] = f.read()

    # print(include_str_table)

    for target_filename in os.listdir(source_dir):
        if os.path.isdir(os.path.join(source_dir, target_filename)) == False:
            print("Preprocessing " + target_filename + "...")
            with open(os.path.join(source_dir, target_filename), "r") as target_file:
                result = insert_include_files_to_target(target_file.read(), include_str_table)
                with open(os.path.join(dest_dir, target_filename), "w") as result_file:
                    result_file.write(result)

    print("Successfully preprocessed glsl codes!")

if __name__ == "__main__":
    main()
